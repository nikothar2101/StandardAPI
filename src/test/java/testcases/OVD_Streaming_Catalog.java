package testcases;

import java.io.IOException;
import java.util.HashMap;

import org.json.JSONObject;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import assertions.Validations;
import commonFunctions.ApiKey;
import commonFunctions.DatabaseConnection;
import commonFunctions.ExcelUtility;
import commonFunctions.KeyParameters;
import commonFunctions.ResponseValidation;
import io.restassured.module.jsv.JsonSchemaValidator;
import io.restassured.response.Response;
import steps.GetRequestMethod;

@Listeners(reports.TestListener.class)
public class OVD_Streaming_Catalog {

	public static HashMap<String, String> map = new HashMap<String, String>();
	 String filePath = System.getProperty("user.dir")+"\\Data";
	 String DynamicURL;
	 String Sheetname ="OVDStreaming";
	 		

		  @BeforeMethod 
		  public void beforeMethod(ITestResult result) throws IOException
		  { 
		  map.put("TestCase", result.getMethod().getMethodName()); 
		  ExcelUtility excelread=new ExcelUtility();
		  excelread.readExcel(filePath,"StandardAPI.xlsx",Sheetname,map.get("TestCase")); 
		  DatabaseConnection Connector= new DatabaseConnection();
		  if(result.getMethod().getMethodName()=="Metadata_Validation") {
		  Connector.DBConnection("OVD_DBName");
		  }
		  else {
			  Connector.DBConnection("VOD_DBNAME");  
		  }
		  }

		  @AfterMethod 
		  public void afterMethod(ITestResult result) throws IOException
		  {
			 DatabaseConnection Connector= new DatabaseConnection();
			 Connector.closeDBConnection();
		  }
		  @Test(enabled=true)
			public void Metadata_Validation() throws IOException {
			  ResponseValidation validation =new ResponseValidation();
			  validation.MetadataValidation();
		  }
		  @Test(enabled=true)
			public void MaxOffSet_Limit_Validation() throws IOException {
			  ResponseValidation validation =new ResponseValidation();
			  validation.MaxOffSet_Limit();
				
		  }
		  
		  @Test(enabled=true)
			public void ResponseValidation100() throws IOException {
			  ResponseValidation validation =new ResponseValidation();
			  validation.OVDResponseValidation();
				
		  }
		  @Test(enabled=false)
			public void noAPIKey_Validation() throws IOException {
				ApiKey APIKeyTAG =new ApiKey();
				APIKeyTAG.getEndpointValidation("OVDStreaming");
				APIKeyTAG.noAPIKeyTag(ExcelUtility.map.get("Tag"),"OVDStreaming");
				Validations valid =new Validations();
				valid.getResponseValue(map.get("TestCase"),Sheetname);
				
			}
		 
		 @Test(enabled=true)
			public void noAPIKeyValue_Validation() throws IOException {
				ApiKey APIKeyTAG =new ApiKey();
				APIKeyTAG.getEndpointValidation("OVDStreaming");
				APIKeyTAG.noAPIKeyValue(ExcelUtility.map.get("Tag"),"OVDStreaming"); 
				Validations valid =new Validations();
				valid.getResponseValue(map.get("TestCase"),Sheetname);
			}
		 
		 @Test(enabled=true)
			public void invalidAPIKeyValue_Validation() throws IOException {
				ApiKey APIKeyTAG =new ApiKey();
				APIKeyTAG.getEndpointValidation("OVDStreaming");
				APIKeyTAG.invalidAPIKeyValue(ExcelUtility.map.get("Tag"),Sheetname);
				Validations valid =new Validations();
				valid.getResponseValue(map.get("TestCase"),Sheetname);	
			}
		 @Test(enabled=true)
			public void validAPIKeyValueOfDifferentProduct_Validation() throws IOException {
				ApiKey APIKeyTAG =new ApiKey();
				APIKeyTAG.getEndpointValidation("OVDStreaming");
				APIKeyTAG.invalidAPIKeyValue(ExcelUtility.map.get("Tag"),Sheetname);
				Validations valid =new Validations();
				valid.getResponseValue(map.get("TestCase"),Sheetname);	
			}
		 @Test(enabled=true)
			public void noContextValue_Validation() throws IOException {
				ApiKey APIKeyTAG =new ApiKey();
				KeyParameters Kparameter =new KeyParameters();
				APIKeyTAG.getEndpointValidation("OVDStreaming");
				Kparameter.ErrorTagValueNoPresent(ExcelUtility.map.get("Tag"),Sheetname);
				Validations valid =new Validations();
				valid.getResponseValue(map.get("TestCase"),Sheetname);	
			}
		 @Test(enabled=true)
			public void noContextTag_Validation() throws IOException {
				ApiKey APIKeyTAG =new ApiKey();
				KeyParameters Kparameter =new KeyParameters();
				APIKeyTAG.getEndpointValidation("OVDStreaming");
				Kparameter.ErrorTagNotPresent(ExcelUtility.map.get("Tag"),Sheetname);
				Validations valid =new Validations();
				valid.getResponseValue(map.get("TestCase"),Sheetname);	
			}
		 @Test(enabled=true)
			public void noApplicationNameValue_Validation() throws IOException {
				ApiKey APIKeyTAG =new ApiKey();
				KeyParameters Kparameter =new KeyParameters();
				APIKeyTAG.getEndpointValidation("OVDStreaming");
				Kparameter.ErrorTagValueNoPresent(ExcelUtility.map.get("Tag"),Sheetname);
				Validations valid =new Validations();
				valid.getResponseValue(map.get("TestCase"),"Sheetname");		
			}
		 
		 @Test(enabled=true)
			public void noChangeOffSet_ValueValidation() throws IOException {
				ApiKey APIKeyTAG =new ApiKey();
				KeyParameters Kparameter =new KeyParameters();
				APIKeyTAG.getEndpointValidation("OVDStreaming");
				Kparameter.ErrorTagValueNoPresent(ExcelUtility.map.get("Tag"),Sheetname);
				Validations valid =new Validations();
				valid.getResponseValue(map.get("TestCase"),Sheetname);	
			}
			@Test(enabled=true)
			public void invalidValueChangeOffSet_Validation() throws IOException {
				ApiKey APIKeyTAG =new ApiKey();
				KeyParameters Kparameter =new KeyParameters();
				APIKeyTAG.getEndpointValidation("OVDStreaming");
				Kparameter.ErrorInValidValue(ExcelUtility.map.get("Tag"),Sheetname);
				Validations valid =new Validations();
				valid.getResponseValue(map.get("TestCase"),Sheetname);	
			}
			@Test(enabled=true)
			public void noChangeOffSet_Validation() throws IOException {
				ApiKey APIKeyTAG =new ApiKey();
				KeyParameters Kparameter =new KeyParameters();
				APIKeyTAG.getEndpointValidation("OVDStreaming");
				Kparameter.ErrorTagNotPresent(ExcelUtility.map.get("Tag"),Sheetname);
				Validations valid =new Validations();
				valid.getResponseValue(map.get("TestCase"),Sheetname);	
			}
			@Test(enabled=true)
			public void noLimitTag_Validation() throws IOException {
				ApiKey APIKeyTAG =new ApiKey();
				GetRequestMethod GetReq =new GetRequestMethod();
				KeyParameters Kparameter =new KeyParameters();
				APIKeyTAG.getEndpointValidation("OVDStreaming");
				Kparameter.LogoType_Tag(ExcelUtility.map.get("Tag"));
				Validations valid =new Validations();
				Response response=GetReq.dynamicgetRequest(KeyParameters.map.get("DynamicURL")); 
				int ActualStatus= response.getStatusCode();
				String ActualCode=Integer.toString(ActualStatus);
				valid.statusCodeValidation(ActualCode,ExcelUtility.map.get("StatusCode"));
				valid.schemaValidation(response.asString(), JsonSchemaValidator.matchesJsonSchemaInClasspath("Schema/OVD_Streaming1.json"));		
				
			}
			@Test(enabled=true)
			public void noLimitValue_Validation() throws IOException {
				ApiKey APIKeyTAG =new ApiKey();
				GetRequestMethod GetReq =new GetRequestMethod();
				KeyParameters Kparameter =new KeyParameters();
				APIKeyTAG.getEndpointValidation("OVDStreaming");
				Kparameter.LogoType_Value(ExcelUtility.map.get("Tag"));
				Validations valid =new Validations();
				Response response=GetReq.dynamicgetRequest(KeyParameters.map.get("DynamicURL")); 
				int ActualStatus= response.getStatusCode();
				String ActualCode=Integer.toString(ActualStatus);
				valid.statusCodeValidation(ActualCode,ExcelUtility.map.get("StatusCode"));
				valid.schemaValidation(response.asString(), JsonSchemaValidator.matchesJsonSchemaInClasspath("Schema/OVD_Streaming1.json"));		
			}
			@Test(enabled=true)
			public void invalidValueLimit_Validation() throws IOException {
				ApiKey APIKeyTAG =new ApiKey();
				KeyParameters Kparameter =new KeyParameters();
				APIKeyTAG.getEndpointValidation("OVDStreaming");
				Kparameter.LogoType_InvalidValue(ExcelUtility.map.get("Tag"),Sheetname);
				Validations valid =new Validations();
				valid.getResponseValue(map.get("TestCase"),Sheetname);	
			}
			 @Test(enabled=true)
				public void TC001_HEADENDIDS_Validation() throws IOException {
				  ResponseValidation validation =new ResponseValidation();
				  validation.HeadEndID_DataValidation();
			  }
			 @Test(enabled=true)
				public void TC002_HEADENDIDS_Validation() throws IOException {
				  ResponseValidation validation =new ResponseValidation();
				  validation.HeadEndID_DataValidation();
			  }
			 @Test(enabled=true)
				public void TC003_HEADENDIDS_Validation() throws IOException {
				  ResponseValidation validation =new ResponseValidation();
				  validation.HeadEndID_DataValidation();
			  }
			 @Test(enabled=true)
				public void TC004_MultipleHeadEndIDs_Validation() throws IOException {
					ApiKey APIKeyTAG =new ApiKey();
					GetRequestMethod GetReq =new GetRequestMethod();
					KeyParameters Kparameter =new KeyParameters();
					APIKeyTAG.getEndpointValidation("HeadEndID_OVDStreaming");
					Kparameter.InValidValuePresent(ExcelUtility.map.get("Tag"));
					Validations valid =new Validations();
					Response response=GetReq.dynamicgetRequest(KeyParameters.map.get("DynamicURL")); 
					int ActualStatus= response.getStatusCode();
					String ActualCode=Integer.toString(ActualStatus);
					valid.statusCodeValidation(ActualCode,ExcelUtility.map.get("StatusCode"));
					valid.schemaValidation(response.asString(), JsonSchemaValidator.matchesJsonSchemaInClasspath("Schema/OVD_Streaming1.json"));		
				}
			
}

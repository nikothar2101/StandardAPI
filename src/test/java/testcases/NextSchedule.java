package testcases;

import java.io.IOException;
import java.util.HashMap;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import assertions.Validations;
import commonFunctions.ApiKey;
import commonFunctions.DatabaseConnection;
import commonFunctions.ExcelUtility;
import commonFunctions.KeyParameters;
import io.restassured.module.jsv.JsonSchemaValidator;
import io.restassured.response.Response;
import steps.GetRequestMethod;

@Listeners(reports.TestListener.class)

public class NextSchedule {
	
	public static HashMap<String, String> map = new HashMap<String, String>();
	 String filePath = System.getProperty("user.dir")+"\\Data";
	 String DynamicURL;
	 String Sheetname ="NextSchedule";
	 		

		  @BeforeMethod 
		  public void beforeMethod(ITestResult result) throws IOException
		  { 
		  map.put("TestCase", result.getMethod().getMethodName()); 
		  ExcelUtility excelread=new ExcelUtility();
		  excelread.readExcel(filePath,"StandardAPI.xlsx",Sheetname,map.get("TestCase")); 
			
			  DatabaseConnection Connector= new DatabaseConnection();
			 
			  if(result.getMethod().getMethodName()=="ProgrammeID_Validation") {
				  Connector.DBConnection("VOD_DBNAME");
				  }
				  else {
					  Connector.DBConnection("EPGCLOUD");  
				  }
			  
			 
		  }

		  @AfterMethod 
		  public void afterMethod(ITestResult result) throws IOException
		  {
			DatabaseConnection Connector= new DatabaseConnection();
			 Connector.closeDBConnection();
		  }
		  
	
	
	@Test(enabled=true)
	public void NextSchedule_Validation() throws IOException {
	
	GetRequestMethod GetReq =new GetRequestMethod(); 
	ApiKey APIKeyTAG =new ApiKey();
	Validations valid =new Validations();
	String ENDPOINT=APIKeyTAG.getEndpointValidation1("NextSchedule");
	Response response=GetReq.dynamicgetRequest(ENDPOINT); 
	int ActualStatus= response.getStatusCode();
	String ActualCode=Integer.toString(ActualStatus);
	valid.statusCodeValidation(ActualCode,ExcelUtility.map.get("StatusCode"));
	valid.schemaValidation(response.asString(), JsonSchemaValidator.matchesJsonSchemaInClasspath("Schema/NextSchedule.json"));		
	}
	
	

}

package testcases;

import java.io.IOException;
import java.util.HashMap;

import org.testng.ITestResult;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;


import assertions.Validations;
import commonFunctions.ApiKey;
import commonFunctions.ExcelUtility;
import commonFunctions.KeyParameters;
import commonFunctions.ResponseValidation;
import io.restassured.module.jsv.JsonSchemaValidator;
import io.restassured.response.Response;
import steps.GetRequestMethod;

@Listeners(reports.TestListener.class)
public class AdvanceSearch {
	public static HashMap<String, String> map = new HashMap<String, String>();
	 String filePath = System.getProperty("user.dir")+"\\Data";
	 String DynamicURL;
	 String Sheetname="AdvanceSearch";

		  @BeforeMethod 
		  public void afterMethod(ITestResult result) throws IOException
		  { 
			  map.put("TestCase", result.getMethod().getMethodName()); 
			  ExcelUtility excelread=new ExcelUtility();
		  excelread.readExcel(filePath,"StandardAPI.xlsx","AdvanceSearch",map.get("TestCase")); }
		 
	@Test(enabled=true)
	  public void advanceSearchAPI() throws IOException { 
	  GetRequestMethod GetReq=new GetRequestMethod(); 
	  Validations valid =new Validations();
	  GetReq.getEndpoint("AdvanceSearch"); 
	  //To fetch response attribute Response
	  Response response=GetReq.getRequest();
	  int ActualStatus= response.getStatusCode();
	  String ActualCode=Integer.toString(ActualStatus);
	  valid.statusCodeValidation(ActualCode,ExcelUtility.map.get("StatusCode"));
	  valid.schemaValidation(response.asString(),JsonSchemaValidator.matchesJsonSchemaInClasspath("Schema/AdvanceSearchSchema.json"));
	  }
	 
	
	/*
	 * @Test(enabled=true) public void advanceSearchAPI() throws IOException {
	 * GetRequestMethod GetReq =new GetRequestMethod(); 
	 * ResponseValidation responseCompare= new ResponseValidation();
	 *  Validations valid =new Validations(); 
	 * GetReq.getEndpoint("ScheduleGrid_UAT");
	 *  Response response=GetReq.getRequest();
	 *   GetReq.getEndpoint("ScheduleGrid_LIVE"); 
	 *   //To fetch response attribute 
	 * Response response1=GetReq.getRequest();
	 * responseCompare.compareJSON(response.asString(),response1.asString()); }
	 */
	@Test(enabled=true)
	public void noAPIKey_Validation() throws IOException {
		ApiKey APIKeyTAG =new ApiKey();
		APIKeyTAG.getEndpointValidation("AdvanceSearch");
		APIKeyTAG.noAPIKeyTag(ExcelUtility.map.get("Tag"),"AdvanceSearch");
		Validations valid =new Validations();
		valid.getResponseValue(map.get("TestCase"),"AdvanceSearch");
		
	}
	@Test(enabled=true)
	public void noAPIKeyValue_Validation() throws IOException {
		ApiKey APIKeyTAG =new ApiKey();
		APIKeyTAG.getEndpointValidation("AdvanceSearch");
		APIKeyTAG.noAPIKeyValue(ExcelUtility.map.get("Tag"),"AdvanceSearch"); 
		Validations valid =new Validations();
		valid.getResponseValue(map.get("TestCase"),"AdvanceSearch");
	}
	@Test(enabled=true)
	public void validAPIKeyValueOfDifferentProduct_Validation() throws IOException {
		ApiKey APIKeyTAG =new ApiKey();
		APIKeyTAG.getEndpointValidation("AdvanceSearch");
		APIKeyTAG.validAPIKeyValueOfDifferentProduct(ExcelUtility.map.get("Tag"),"AdvanceSearch");
		Validations valid =new Validations();
		valid.getResponseValue(map.get("TestCase"),"AdvanceSearch");	
	}
	@Test(enabled=false)
	public void invalidAPIKeyValue_Validation() throws IOException {
		ApiKey APIKeyTAG =new ApiKey();
		APIKeyTAG.getEndpointValidation("AdvanceSearch");
		APIKeyTAG.invalidAPIKeyValue(ExcelUtility.map.get("Tag"),"AdvanceSearch");
		Validations valid =new Validations();
		valid.getResponseValue(map.get("TestCase"),"AdvanceSearch");	
	}
	@Test(enabled=true)
	public void noResponseFormatKey_Validation() throws IOException {
		ApiKey APIKeyTAG =new ApiKey();
		KeyParameters ResponseLanguage =new KeyParameters();
		APIKeyTAG.getEndpointValidation("AdvanceSearch");
		ResponseLanguage.noResponseFormatKeyTag();
		Validations valid =new Validations();
		valid.getResponseValue(map.get("TestCase"),"AdvanceSearch");
		}
	@Test(enabled=true)
	public void noResponseFormatValue_Validation() throws IOException {
		ApiKey APIKeyTAG =new ApiKey();
		KeyParameters ResponseLanguage =new KeyParameters();
		Validations valid =new Validations();
		APIKeyTAG.getEndpointValidation("AdvanceSearch");
		ResponseLanguage.XMLErrorTagValueNoPresent(ExcelUtility.map.get("Tag"));
		valid.getResponseValue("noResponseFormatValue_Validation","AdvanceSearch");
		}
	@Test(enabled=true)
	public void noResponseFormatInvalidValue_Validation() throws IOException {
		ApiKey APIKeyTAG =new ApiKey();
		KeyParameters ResponseLanguage =new KeyParameters();
		Validations valid =new Validations();
		APIKeyTAG.getEndpointValidation("AdvanceSearch");
		ResponseLanguage.XMLErrorInValidValue(ExcelUtility.map.get("Tag"));
		valid.getResponseValue(map.get("TestCase"),"AdvanceSearch");
		}
	@Test(enabled=true)
	public void noResponseLanguageKey_Validation() throws IOException {
		ApiKey APIKeyTAG =new ApiKey();
		KeyParameters ResponseLanguage =new KeyParameters();
		APIKeyTAG.getEndpointValidation("AdvanceSearch");
		ResponseLanguage.ErrorTagNotPresent(ExcelUtility.map.get("Tag"),"AdvancedSearch");
		Validations valid =new Validations();
		valid.getResponseValue(map.get("TestCase"),"AdvanceSearch");
	}
	@Test(enabled=true)
	public void noResponseLanguageValue_Validation() throws IOException {
		GetRequestMethod GetReq =new GetRequestMethod(); 
		ApiKey APIKeyTAG =new ApiKey();
		KeyParameters PageNo =new KeyParameters();
		Validations valid =new Validations();
		APIKeyTAG.getEndpointValidation("AdvanceSearch");
		PageNo.TagValueNoPresent(ExcelUtility.map.get("Tag"));
		Response response=GetReq.dynamicgetRequest(KeyParameters.map.get("DynamicURL")); 
		int ActualStatus= response.getStatusCode();
		String ActualCode=Integer.toString(ActualStatus);
		valid.statusCodeValidation(ActualCode,ExcelUtility.map.get("StatusCode"));
		valid.schemaValidation(response.asString(), JsonSchemaValidator.matchesJsonSchemaInClasspath("Schema/AdvanceSearchSchema.json"));		
	}
	@Test(enabled=true)
	public void noResponseLanguageInvalidValue_Validation() throws IOException {
		ApiKey APIKeyTAG =new ApiKey();
		KeyParameters ResponseLanguage =new KeyParameters();
		APIKeyTAG.getEndpointValidation("AdvanceSearch");
		ResponseLanguage.ErrorInValidValue(ExcelUtility.map.get("Tag"),"AdvanceSearch");
		Validations valid =new Validations();
		valid.getResponseValue(map.get("TestCase"),"AdvanceSearch");
	}
	
	@Test(enabled=false)
	public void noPageNumberTag_Validation() throws IOException {
		GetRequestMethod GetReq =new GetRequestMethod(); 
		ApiKey APIKeyTAG =new ApiKey();
		KeyParameters PageNo =new KeyParameters();
		Validations valid =new Validations();
		APIKeyTAG.getEndpointValidation("AdvanceSearch");
		PageNo.TagNotPresent(ExcelUtility.map.get("Tag"));
		Response response=GetReq.dynamicgetRequest(KeyParameters.map.get("DynamicURL")); 
		int ActualStatus= response.getStatusCode();
		String ActualCode=Integer.toString(ActualStatus);
		valid.statusCodeValidation(ActualCode,ExcelUtility.map.get("StatusCode"));
		valid.schemaValidation(response.asString(), JsonSchemaValidator.matchesJsonSchemaInClasspath("Schema/AdvanceSearchSchema.json"));		
	}
	@Test(enabled=false)
	public void pageNumbertag_InvalidValue_Validation() throws IOException {
		ApiKey APIKeyTAG =new ApiKey();
		KeyParameters ResponseLanguage =new KeyParameters();
		APIKeyTAG.getEndpointValidation("AdvanceSearch");
		ResponseLanguage.ErrorInValidValue(ExcelUtility.map.get("Tag"),"AdvanceSearch");
		Validations valid =new Validations();
		valid.getResponseValue(map.get("TestCase"),"AdvanceSearch");	
	}
	@Test(enabled=false)
	public void noSearchValueTag_Validation() throws IOException {
		ApiKey APIKeyTAG =new ApiKey();
		KeyParameters ResponseLanguage =new KeyParameters();
		APIKeyTAG.getEndpointValidation("AdvanceSearch");
		ResponseLanguage.ErrorTagNotPresent(ExcelUtility.map.get("Tag"),"AdvancedSearch");
		Validations valid =new Validations();
		valid.getResponseValue(map.get("TestCase"),"AdvanceSearch");	
	}
	@Test(enabled=false)
	public void noSearchValuePresent_Validation() throws IOException {
		ApiKey APIKeyTAG =new ApiKey();
		KeyParameters ResponseLanguage =new KeyParameters();
		APIKeyTAG.getEndpointValidation("AdvanceSearch");
		ResponseLanguage.TagValueNoPresent(ExcelUtility.map.get("Tag"));
		Validations valid =new Validations();
		valid.getResponseValue(map.get("TestCase"),"AdvanceSearch");	
	}
	
	@Test(enabled=false)
	public void noSearchCriteriaTag_Validation() throws IOException {
		ApiKey APIKeyTAG =new ApiKey();
		KeyParameters ResponseLanguage =new KeyParameters();
		APIKeyTAG.getEndpointValidation("AdvanceSearch");
		ResponseLanguage.ErrorTagNotPresent(ExcelUtility.map.get("Tag"),"AdvancedSearch");
		Validations valid =new Validations();
		valid.getResponseValue(map.get("TestCase"),"AdvanceSearch");	
	}
	@Test(enabled=false)
	public void noSearchCriteriaValuePresent_Validation() throws IOException {
		GetRequestMethod GetReq =new GetRequestMethod(); 
		ApiKey APIKeyTAG =new ApiKey();
		KeyParameters PageNo =new KeyParameters();
		Validations valid =new Validations();
		APIKeyTAG.getEndpointValidation("AdvanceSearch");
		PageNo.TagValueNoPresent(ExcelUtility.map.get("Tag"));
		Response response=GetReq.dynamicgetRequest(KeyParameters.map.get("DynamicURL")); 
		int ActualStatus= response.getStatusCode();
		String ActualCode=Integer.toString(ActualStatus);
		valid.statusCodeValidation(ActualCode,ExcelUtility.map.get("StatusCode"));
		valid.schemaValidation(response.asString(), JsonSchemaValidator.matchesJsonSchemaInClasspath("Schema/AdvanceSearchSchema.json"));		
	}
	@Test(enabled=false)
	public void noFilteredGenresTag_Validation() throws IOException {
		GetRequestMethod GetReq =new GetRequestMethod(); 
		ApiKey APIKeyTAG =new ApiKey();
		KeyParameters PageNo =new KeyParameters();
		Validations valid =new Validations();
		APIKeyTAG.getEndpointValidation("AdvanceSearch");
		PageNo.TagNotPresent(ExcelUtility.map.get("Tag"));
		Response response=GetReq.dynamicgetRequest(KeyParameters.map.get("DynamicURL")); 
		String ExpectedCode =ExcelUtility.map.get("StatusCode");
		int ActualStatus= response.getStatusCode();
		String ActualCode=Integer.toString(ActualStatus);
		valid.statusCodeValidation(ActualCode,ExpectedCode);
		valid.schemaValidation(response.asString(), JsonSchemaValidator.matchesJsonSchemaInClasspath("Schema/AdvanceSearchSchema.json"));		
	}
	@Test(enabled=false)
	public void noFilteredGenresValue_Validation() throws IOException {
		GetRequestMethod GetReq =new GetRequestMethod(); 
		ApiKey APIKeyTAG =new ApiKey();
		KeyParameters PageNo =new KeyParameters();
		Validations valid =new Validations();
		APIKeyTAG.getEndpointValidation("AdvanceSearch");
		PageNo.TagValueNoPresent(ExcelUtility.map.get("Tag"));
		Response response=GetReq.dynamicgetRequest(KeyParameters.map.get("DynamicURL")); 
		String ExpectedCode =ExcelUtility.map.get("StatusCode");
		int ActualStatus= response.getStatusCode();
		String ActualCode=Integer.toString(ActualStatus);
		valid.statusCodeValidation(ActualCode,ExpectedCode);
		valid.schemaValidation(response.asString(), JsonSchemaValidator.matchesJsonSchemaInClasspath("Schema/AdvanceSearchSchema.json"));		
	}
	@Test(enabled=true)
	public void noFilteredGenresInvalidValue_Validation() throws IOException {
		ApiKey APIKeyTAG =new ApiKey();
		KeyParameters ResponseLanguage =new KeyParameters();
		APIKeyTAG.getEndpointValidation("AdvanceSearch");
		ResponseLanguage.Common_InvalidreturnNoValue(ExcelUtility.map.get("Tag"));
		Validations valid =new Validations();
		valid.getResponseValue(map.get("TestCase"),"AdvanceSearch");
		valid.response_Validation(ExcelUtility.map.get("ExpectedResponsestatus"),ApiKey.map.get("ActualResponseStatus")); 
		valid.message_Validation(ExcelUtility.map.get("ExpectedMessage"),ApiKey.map.get("ActualMessage"));
		
		}
	@Test(enabled=false)
	public void noFilteredSubGenresTag_Validation() throws IOException {
		GetRequestMethod GetReq =new GetRequestMethod(); 
		ApiKey APIKeyTAG =new ApiKey();
		KeyParameters PageNo =new KeyParameters();
		Validations valid =new Validations();
		APIKeyTAG.getEndpointValidation("AdvanceSearch");
		PageNo.TagNotPresent(ExcelUtility.map.get("Tag"));
		Response response=GetReq.dynamicgetRequest(KeyParameters.map.get("DynamicURL")); 
		int ActualStatus= response.getStatusCode();
		String ActualCode=Integer.toString(ActualStatus);
		valid.statusCodeValidation(ActualCode,ExcelUtility.map.get("StatusCode"));
		valid.schemaValidation(response.asString(), JsonSchemaValidator.matchesJsonSchemaInClasspath("Schema/AdvanceSearchSchema.json"));		
	}
	@Test(enabled=false)
	public void noFilteredSubGenresValue_Validation() throws IOException {
		GetRequestMethod GetReq =new GetRequestMethod(); 
		ApiKey APIKeyTAG =new ApiKey();
		KeyParameters PageNo =new KeyParameters();
		Validations valid =new Validations();
		APIKeyTAG.getEndpointValidation("AdvanceSearch");
		PageNo.TagValueNoPresent(ExcelUtility.map.get("Tag"));
		Response response=GetReq.dynamicgetRequest(KeyParameters.map.get("DynamicURL")); 
		int ActualStatus= response.getStatusCode();
		String ActualCode=Integer.toString(ActualStatus);
		valid.statusCodeValidation(ActualCode,ExcelUtility.map.get("StatusCode"));
		valid.schemaValidation(response.asString(), JsonSchemaValidator.matchesJsonSchemaInClasspath("Schema/AdvanceSearchSchema.json"));		
	}
	@Test(enabled=false)
	public void noFilteredSubGenresInvalidValue_Validation() throws IOException {
		ApiKey APIKeyTAG =new ApiKey();
		KeyParameters ResponseLanguage =new KeyParameters();
		APIKeyTAG.getEndpointValidation("AdvanceSearch");
		ResponseLanguage.Common_InvalidreturnNoValue(ExcelUtility.map.get("Tag"));
		Validations valid =new Validations();
		valid.getResponseValue(map.get("TestCase"),"AdvanceSearch");
		valid.response_Validation(ExcelUtility.map.get("ExpectedResponsestatus"),ApiKey.map.get("ActualResponseStatus")); 
		valid.message_Validation(ExcelUtility.map.get("ExpectedMessage"),ApiKey.map.get("ActualMessage"));
		
		}
	@Test(enabled=true)
	public void noLanguageTag_Validation() throws IOException {
		GetRequestMethod GetReq =new GetRequestMethod(); 
		ApiKey APIKeyTAG =new ApiKey();
		KeyParameters PageNo =new KeyParameters();
		Validations valid =new Validations();
		APIKeyTAG.getEndpointValidation("AdvanceSearch");
		PageNo.noLanguage_Tag();
		Response response=GetReq.dynamicgetRequest(KeyParameters.map.get("DynamicURL")); 
		int ActualStatus= response.getStatusCode();
		String ActualCode=Integer.toString(ActualStatus);
		valid.statusCodeValidation(ActualCode,ExcelUtility.map.get("StatusCode"));
		valid.schemaValidation(response.asString(), JsonSchemaValidator.matchesJsonSchemaInClasspath("Schema/AdvanceSearchSchema.json"));		
	}
	@Test(enabled=false)
	public void noLanguageValue_Validation() throws IOException {
		GetRequestMethod GetReq =new GetRequestMethod(); 
		ApiKey APIKeyTAG =new ApiKey();
		KeyParameters PageNo =new KeyParameters();
		Validations valid =new Validations();
		APIKeyTAG.getEndpointValidation("AdvanceSearch");
		PageNo.noLanguage_Value();
		Response response=GetReq.dynamicgetRequest(KeyParameters.map.get("DynamicURL")); 
		int ActualStatus= response.getStatusCode();
		String ActualCode=Integer.toString(ActualStatus);
		valid.statusCodeValidation(ActualCode,ExcelUtility.map.get("StatusCode"));
		valid.schemaValidation(response.asString(), JsonSchemaValidator.matchesJsonSchemaInClasspath("Schema/AdvanceSearchSchema.json"));		
	}
	@Test(enabled=false)
	public void noLanguageInvalidValue_Validation() throws IOException {
		ApiKey APIKeyTAG =new ApiKey();
		KeyParameters ResponseLanguage =new KeyParameters();
		APIKeyTAG.getEndpointValidation("AdvanceSearch");
		ResponseLanguage.Language_InvalideValue();
		Validations valid =new Validations();
		valid.getResponseValue(map.get("TestCase"),"AdvanceSearch");
		valid.response_Validation(ExcelUtility.map.get("ExpectedResponsestatus"),ApiKey.map.get("ActualResponseStatus")); 
		valid.message_Validation(ExcelUtility.map.get("ExpectedMessage"),ApiKey.map.get("ActualMessage"));
		}
	@Test(enabled=false)
	public void noLogoTypeTag_Validation() throws IOException {
		GetRequestMethod GetReq =new GetRequestMethod(); 
		ApiKey APIKeyTAG =new ApiKey();
		KeyParameters PageNo =new KeyParameters();
		Validations valid =new Validations();
		APIKeyTAG.getEndpointValidation("AdvanceSearch");
		PageNo.LogoType_Tag(ExcelUtility.map.get("Tag"));
		Response response=GetReq.dynamicgetRequest(KeyParameters.map.get("DynamicURL")); 
		int ActualStatus= response.getStatusCode();
		String ActualCode=Integer.toString(ActualStatus);
		valid.statusCodeValidation(ActualCode,ExcelUtility.map.get("StatusCode"));
		valid.schemaValidation(response.asString(), JsonSchemaValidator.matchesJsonSchemaInClasspath("Schema/AdvanceSearchSchema.json"));		
	
	}
	@Test(enabled=false)
	public void noLogoTypeValue_Validation() throws IOException {
		GetRequestMethod GetReq =new GetRequestMethod(); 
		ApiKey APIKeyTAG =new ApiKey();
		KeyParameters PageNo =new KeyParameters();
		Validations valid =new Validations();
		APIKeyTAG.getEndpointValidation("AdvanceSearch");
		PageNo.LogoType_Value(ExcelUtility.map.get("Tag"));
		Response response=GetReq.dynamicgetRequest(KeyParameters.map.get("DynamicURL")); 
		int ActualStatus= response.getStatusCode();
		String ActualCode=Integer.toString(ActualStatus);
		valid.statusCodeValidation(ActualCode,ExcelUtility.map.get("StatusCode"));
		valid.schemaValidation(response.asString(), JsonSchemaValidator.matchesJsonSchemaInClasspath("Schema/AdvanceSearchSchema.json"));		
	}
	@Test(enabled=false)
	public void noLogoTypeInvalidValue_Validation() throws IOException {
		GetRequestMethod GetReq =new GetRequestMethod(); 
		ApiKey APIKeyTAG =new ApiKey();
		KeyParameters PageNo =new KeyParameters();
		Validations valid =new Validations();
		APIKeyTAG.getEndpointValidation("AdvanceSearch");
		PageNo.LogoType_InvalidValue(ExcelUtility.map.get("Tag"),Sheetname);
		Response response=GetReq.dynamicgetRequest(KeyParameters.map.get("DynamicURL")); 
		int ActualStatus= response.getStatusCode();
		String ActualCode=Integer.toString(ActualStatus);
		valid.statusCodeValidation(ActualCode,ExcelUtility.map.get("StatusCode"));
		valid.schemaValidation(response.asString(), JsonSchemaValidator.matchesJsonSchemaInClasspath("Schema/AdvanceSearchSchema.json"));		
	}
	@Test(enabled=false)
	public void noIsReasoningTag_Validation() throws IOException {
		ApiKey APIKeyTAG =new ApiKey();
		KeyParameters Kparameter =new KeyParameters();
		APIKeyTAG.getEndpointValidation("AdvanceSearch");
		Kparameter.ErrorTagNotPresent(ExcelUtility.map.get("Tag"),"AdvancedSearch");
		Validations valid =new Validations();
		valid.getResponseValue(map.get("TestCase"),"AdvanceSearch");	
	}
	@Test(enabled=true)
	public void noIsReasoningValue_Validation() throws IOException {
		ApiKey APIKeyTAG =new ApiKey();
		KeyParameters Kparameter =new KeyParameters();
		APIKeyTAG.getEndpointValidation("AdvanceSearch");
		Kparameter.ErrorTagValueNoPresent(ExcelUtility.map.get("Tag"),"AdvanceSearch");
		Validations valid =new Validations();
		valid.getResponseValue(map.get("TestCase"),"AdvanceSearch");	
	}
	@Test(enabled=true)
	public void noIsReasoningInvalidValue_Validation() throws IOException {
		ApiKey APIKeyTAG =new ApiKey();
		KeyParameters Kparameter =new KeyParameters();
		APIKeyTAG.getEndpointValidation("AdvanceSearch");
		Kparameter.ErrorInValidValue(ExcelUtility.map.get("Tag"),"AdvanceSearch");
		Validations valid =new Validations();
		valid.getResponseValue(map.get("TestCase"),"AdvanceSearch");	
	}
	@Test(enabled=true)
	public void noContextTag_Validation() throws IOException {
		ApiKey APIKeyTAG =new ApiKey();
		KeyParameters Kparameter =new KeyParameters();
		APIKeyTAG.getEndpointValidation("AdvanceSearch");
		Kparameter.ErrorTagNotPresent(ExcelUtility.map.get("Tag"),"AdvancedSearch");
		Validations valid =new Validations();
		valid.getResponseValue(map.get("TestCase"),"AdvanceSearch");	
	}
	@Test(enabled=true)
	public void noContextValue_Validation() throws IOException {
		ApiKey APIKeyTAG =new ApiKey();
		KeyParameters Kparameter =new KeyParameters();
		APIKeyTAG.getEndpointValidation("AdvanceSearch");
		Kparameter.ErrorTagValueNoPresent(ExcelUtility.map.get("Tag"),"AdvanceSearch");
		Validations valid =new Validations();
		valid.getResponseValue(map.get("TestCase"),"AdvanceSearch");	
	}
	@Test(enabled=true)
	public void noSearchsortfieldTag_Validation() throws IOException {
		GetRequestMethod GetReq =new GetRequestMethod(); 
		ApiKey APIKeyTAG =new ApiKey();
		KeyParameters KParameters =new KeyParameters();
		Validations valid =new Validations();
		APIKeyTAG.getEndpointValidation("AdvanceSearch");
		KParameters.TagNotPresent(ExcelUtility.map.get("Tag"));
		Response response=GetReq.dynamicgetRequest(KeyParameters.map.get("DynamicURL")); 
		int ActualStatus= response.getStatusCode();
		String ActualCode=Integer.toString(ActualStatus);
		valid.statusCodeValidation(ActualCode,ExcelUtility.map.get("StatusCode"));
		valid.schemaValidation(response.asString(), JsonSchemaValidator.matchesJsonSchemaInClasspath("Schema/AdvanceSearchSchema.json"));		
	
	}
	@Test(enabled=true)
	public void noSearchsortfieldValue_Validation() throws IOException {
		GetRequestMethod GetReq =new GetRequestMethod(); 
		ApiKey APIKeyTAG =new ApiKey();
		KeyParameters KParameters =new KeyParameters();
		Validations valid =new Validations();
		APIKeyTAG.getEndpointValidation("AdvanceSearch");
		KParameters.TagValueNoPresent(ExcelUtility.map.get("Tag"));
		Response response=GetReq.dynamicgetRequest(KeyParameters.map.get("DynamicURL")); 
		int ActualStatus= response.getStatusCode();
		String ActualCode=Integer.toString(ActualStatus);
		valid.statusCodeValidation(ActualCode,ExcelUtility.map.get("StatusCode"));
		valid.schemaValidation(response.asString(), JsonSchemaValidator.matchesJsonSchemaInClasspath("Schema/AdvanceSearchSchema.json"));		
	}
	@Test(enabled=true)
	public void noSearchsortfieldInvalidValue_Validation() throws IOException {
		GetRequestMethod GetReq =new GetRequestMethod(); 
		ApiKey APIKeyTAG =new ApiKey();
		KeyParameters KParameters =new KeyParameters();
		Validations valid =new Validations();
		APIKeyTAG.getEndpointValidation("AdvanceSearch");
		KParameters.InValidValuePresent(ExcelUtility.map.get("Tag"));
		Response response=GetReq.dynamicgetRequest(KeyParameters.map.get("DynamicURL")); 
		int ActualStatus= response.getStatusCode();
		String ActualCode=Integer.toString(ActualStatus);
		valid.statusCodeValidation(ActualCode,ExcelUtility.map.get("StatusCode"));
		valid.schemaValidation(response.asString(), JsonSchemaValidator.matchesJsonSchemaInClasspath("Schema/AdvanceSearchSchema.json"));		
	}
	@Test(enabled=true)
	public void noheadendIdTag_Validation() throws IOException {
		GetRequestMethod GetReq =new GetRequestMethod(); 
		ApiKey APIKeyTAG =new ApiKey();
		KeyParameters KParameters =new KeyParameters();
		Validations valid =new Validations();
		APIKeyTAG.getEndpointValidation("AdvanceSearch");
		KParameters.ContextTagNotPresent(ExcelUtility.map.get("Tag"));
		Response response=GetReq.dynamicgetRequest(KeyParameters.map.get("DynamicURL")); 
		int ActualStatus= response.getStatusCode();
		String ActualCode=Integer.toString(ActualStatus);
		valid.statusCodeValidation(ActualCode,ExcelUtility.map.get("StatusCode"));
		valid.schemaValidation(response.asString(), JsonSchemaValidator.matchesJsonSchemaInClasspath("Schema/AdvanceSearchSchema.json"));		
	}
	@Test(enabled=true)
	public void noheadendIdValue_Validation() throws IOException {
		GetRequestMethod GetReq =new GetRequestMethod(); 
		ApiKey APIKeyTAG =new ApiKey();
		KeyParameters KParameters =new KeyParameters();
		Validations valid =new Validations();
		APIKeyTAG.getEndpointValidation("AdvanceSearch");
		KParameters.ContextValueNotPresent(ExcelUtility.map.get("Tag"));
		Response response=GetReq.dynamicgetRequest(KeyParameters.map.get("DynamicURL")); 
		int ActualStatus= response.getStatusCode();
		String ActualCode=Integer.toString(ActualStatus);
		valid.statusCodeValidation(ActualCode,ExcelUtility.map.get("StatusCode"));
		valid.schemaValidation(response.asString(), JsonSchemaValidator.matchesJsonSchemaInClasspath("Schema/AdvanceSearchSchema.json"));		
	}
	@Test(enabled=true)
	public void noheadendIdInvalidValue_Validation() throws IOException {
		ApiKey APIKeyTAG =new ApiKey();
		KeyParameters KParameters =new KeyParameters();
		Validations valid =new Validations();
		APIKeyTAG.getEndpointValidation("AdvanceSearch");
		KParameters.ErrorContextInvalidValuePresent(ExcelUtility.map.get("Tag"),"AdvanceSearch");
		valid.getResponseValue(map.get("TestCase"),"AdvanceSearch");	
	}
	
	@Test(enabled=true)
	public void noApplicationNameTag_Validation() throws IOException {
		GetRequestMethod GetReq =new GetRequestMethod(); 
		ApiKey APIKeyTAG =new ApiKey();
		KeyParameters KParameters =new KeyParameters();
		Validations valid =new Validations();
		APIKeyTAG.getEndpointValidation("AdvanceSearch");
		KParameters.ContextTagNotPresent(ExcelUtility.map.get("Tag"));
		Response response=GetReq.dynamicgetRequest(KeyParameters.map.get("DynamicURL")); 
		int ActualStatus= response.getStatusCode();
		String ActualCode=Integer.toString(ActualStatus);
		valid.statusCodeValidation(ActualCode,ExcelUtility.map.get("StatusCode"));
		valid.schemaValidation(response.asString(), JsonSchemaValidator.matchesJsonSchemaInClasspath("Schema/AdvanceSearchSchema.json"));		
	}
	@Test(enabled=true)
	public void noApplicationNameValue_Validation() throws IOException {
		GetRequestMethod GetReq =new GetRequestMethod(); 
		ApiKey APIKeyTAG =new ApiKey();
		KeyParameters KParameters =new KeyParameters();
		Validations valid =new Validations();
		APIKeyTAG.getEndpointValidation("AdvanceSearch");
		KParameters.ContextValueNotPresent(ExcelUtility.map.get("Tag"));
		Response response=GetReq.dynamicgetRequest(KeyParameters.map.get("DynamicURL")); 
		int ActualStatus= response.getStatusCode();
		String ActualCode=Integer.toString(ActualStatus);
		valid.statusCodeValidation(ActualCode,ExcelUtility.map.get("StatusCode"));
		valid.schemaValidation(response.asString(), JsonSchemaValidator.matchesJsonSchemaInClasspath("Schema/AdvanceSearchSchema.json"));		
	}
	@Test(enabled=true)
	public void noPageSizeTag_Validation() throws IOException {
		ApiKey APIKeyTAG =new ApiKey();
		KeyParameters KParameters =new KeyParameters();
		Validations valid =new Validations();
		APIKeyTAG.getEndpointValidation("AdvanceSearch");
		KParameters.ContextNoTagPresent_NoValue(ExcelUtility.map.get("Tag"),"AdvanceSearch");
		valid.getResponseValue(map.get("TestCase"),"AdvanceSearch");
		valid.response_Validation(ExcelUtility.map.get("ExpectedResponsestatus"),ApiKey.map.get("ActualResponseStatus")); 
		valid.message_Validation(ExcelUtility.map.get("ExpectedMessage"),ApiKey.map.get("ActualMessage"));
		}
	@Test(enabled=true)
	public void noPageSizeValue_Validation() throws IOException {
		ApiKey APIKeyTAG =new ApiKey();
		KeyParameters KParameters =new KeyParameters();
		Validations valid =new Validations();
		APIKeyTAG.getEndpointValidation("AdvanceSearch");
		KParameters.ContextNoValuePresent_NoValue(ExcelUtility.map.get("Tag"));
		valid.getResponseValue(map.get("TestCase"),"AdvanceSearch");
		valid.response_Validation(ExcelUtility.map.get("ExpectedResponsestatus"),ApiKey.map.get("ActualResponseStatus")); 
		valid.message_Validation(ExcelUtility.map.get("ExpectedMessage"),ApiKey.map.get("ActualMessage"));
		}
	
	@Test(enabled=true)
	public void noPageSizeInvalidValue_Validation() throws IOException {
		ApiKey APIKeyTAG =new ApiKey();
		KeyParameters ResponseLanguage =new KeyParameters();
		APIKeyTAG.getEndpointValidation("AdvanceSearch");
		ResponseLanguage.ContextInvalidValuePresent_NoValue(ExcelUtility.map.get("Tag"));
		Validations valid =new Validations();
		valid.getResponseValue(map.get("TestCase"),"AdvanceSearch");
		valid.response_Validation(ExcelUtility.map.get("ExpectedResponsestatus"),ApiKey.map.get("ActualResponseStatus")); 
		valid.message_Validation(ExcelUtility.map.get("ExpectedMessage"),ApiKey.map.get("ActualMessage"));
		}
	
	@Test(enabled=true)
	public void noOutdatetimeformatTag_Validation() throws IOException {
		GetRequestMethod GetReq =new GetRequestMethod(); 
		ApiKey APIKeyTAG =new ApiKey();
		KeyParameters KParameters =new KeyParameters();
		Validations valid =new Validations();
		APIKeyTAG.getEndpointValidation("AdvanceSearch");
		KParameters.ContextTagNotPresent(ExcelUtility.map.get("Tag"));
		Response response=GetReq.dynamicgetRequest(KeyParameters.map.get("DynamicURL")); 
		int ActualStatus= response.getStatusCode();
		String ActualCode=Integer.toString(ActualStatus);
		valid.statusCodeValidation(ActualCode,ExcelUtility.map.get("StatusCode"));
		valid.schemaValidation(response.asString(), JsonSchemaValidator.matchesJsonSchemaInClasspath("Schema/AdvanceSearchSchema.json"));		
	}
	@Test(enabled=true)
	public void noOutdatetimeformatValue_Validation() throws IOException {
		GetRequestMethod GetReq =new GetRequestMethod(); 
		ApiKey APIKeyTAG =new ApiKey();
		KeyParameters KParameters =new KeyParameters();
		Validations valid =new Validations();
		APIKeyTAG.getEndpointValidation("AdvanceSearch");
		KParameters.ContextValueNotPresent(ExcelUtility.map.get("Tag"));
		Response response=GetReq.dynamicgetRequest(KeyParameters.map.get("DynamicURL")); 
		int ActualStatus= response.getStatusCode();
		String ActualCode=Integer.toString(ActualStatus);
		valid.statusCodeValidation(ActualCode,ExcelUtility.map.get("StatusCode"));
		valid.schemaValidation(response.asString(), JsonSchemaValidator.matchesJsonSchemaInClasspath("Schema/AdvanceSearchSchema.json"));		
	}
	@Test(enabled=true)
	public void noOutdatetimeformatInValidValue_Validation() throws IOException {
		GetRequestMethod GetReq =new GetRequestMethod(); 
		ApiKey APIKeyTAG =new ApiKey();
		KeyParameters KParameters =new KeyParameters();
		Validations valid =new Validations();
		APIKeyTAG.getEndpointValidation("AdvanceSearch");
		KParameters.ContextInvalidValuePresent(ExcelUtility.map.get("Tag"));
		Response response=GetReq.dynamicgetRequest(KeyParameters.map.get("DynamicURL")); 
		int ActualStatus= response.getStatusCode();
		String ActualCode=Integer.toString(ActualStatus);
		valid.statusCodeValidation(ActualCode,ExcelUtility.map.get("StatusCode"));
		valid.schemaValidation(response.asString(), JsonSchemaValidator.matchesJsonSchemaInClasspath("Schema/AdvanceSearchSchema.json"));		
	}
	@Test(enabled=true)
	public void nochannelimageTag_Validation() throws IOException {
		GetRequestMethod GetReq =new GetRequestMethod(); 
		ApiKey APIKeyTAG =new ApiKey();
		KeyParameters KParameters =new KeyParameters();
		Validations valid =new Validations();
		APIKeyTAG.getEndpointValidation("AdvanceSearch");
		KParameters.ContextTagNotPresent(ExcelUtility.map.get("Tag"));
		Response response=GetReq.dynamicgetRequest(KeyParameters.map.get("DynamicURL")); 
		int ActualStatus= response.getStatusCode();
		String ActualCode=Integer.toString(ActualStatus);
		valid.statusCodeValidation(ActualCode,ExcelUtility.map.get("StatusCode"));
		valid.schemaValidation(response.asString(), JsonSchemaValidator.matchesJsonSchemaInClasspath("Schema/AdvanceSearchSchema.json"));		
	}
	@Test(enabled=true)
	public void noprogrammeimageTag_Validation() throws IOException {
		GetRequestMethod GetReq =new GetRequestMethod(); 
		ApiKey APIKeyTAG =new ApiKey();
		KeyParameters KParameters =new KeyParameters();
		Validations valid =new Validations();
		APIKeyTAG.getEndpointValidation("AdvanceSearch");
		KParameters.ContextTagNotPresent(ExcelUtility.map.get("Tag"));
		Response response=GetReq.dynamicgetRequest(KeyParameters.map.get("DynamicURL")); 
		int ActualStatus= response.getStatusCode();
		String ActualCode=Integer.toString(ActualStatus);
		valid.statusCodeValidation(ActualCode,ExcelUtility.map.get("StatusCode"));
		valid.schemaValidation(response.asString(), JsonSchemaValidator.matchesJsonSchemaInClasspath("Schema/AdvanceSearchSchema.json"));		
	}
	@Test(enabled=true)
	public void nochannelimageTagValue_Validation() throws IOException {
		ApiKey APIKeyTAG =new ApiKey();
		KeyParameters Kparameter =new KeyParameters();
		APIKeyTAG.getEndpointValidation("AdvanceSearch");
		Kparameter.ErrorContextTagNotPresent(ExcelUtility.map.get("Tag"),"AdvanceSearch");
		Validations valid =new Validations();
		valid.getResponseValue(map.get("TestCase"),"AdvanceSearch");	
	}
	@Test(enabled=true)
	public void noprogrammeimageTagValue_Validation() throws IOException {
		ApiKey APIKeyTAG =new ApiKey();
		KeyParameters Kparameter =new KeyParameters();
		APIKeyTAG.getEndpointValidation("AdvanceSearch");
		Kparameter.ErrorContextTagNotPresent(ExcelUtility.map.get("Tag"),"AdvanceSearch");
		Validations valid =new Validations();
		valid.getResponseValue(map.get("TestCase"),"AdvanceSearch");	
	}
	@Test(enabled=true)
	public void nochannelimageInValidValue_Validation() throws IOException {
		ApiKey APIKeyTAG =new ApiKey();
		KeyParameters Kparameter =new KeyParameters();
		APIKeyTAG.getEndpointValidation("AdvanceSearch");
		Kparameter.ErrorContextInvalidValuePresent(ExcelUtility.map.get("Tag"),"AdvanceSearch");
		Validations valid =new Validations();
		valid.getResponseValue(map.get("TestCase"),"AdvanceSearch");	
	}
	@Test(enabled=true)
	public void noprogrammeimageInValidValue_Validation() throws IOException {
		ApiKey APIKeyTAG =new ApiKey();
		KeyParameters Kparameter =new KeyParameters();
		APIKeyTAG.getEndpointValidation("AdvanceSearch");
		Kparameter.ErrorContextInvalidValuePresent(ExcelUtility.map.get("Tag"),"AdvanceSearch");
		Validations valid =new Validations();
		valid.getResponseValue(map.get("TestCase"),"AdvanceSearch");	
	}
	
	@Test(enabled=true)
	public void nochannelimageValue_Validation() throws IOException {
		ApiKey APIKeyTAG =new ApiKey();
		KeyParameters Kparameter =new KeyParameters();
		APIKeyTAG.getEndpointValidation("AdvanceSearch");
		Kparameter.ErrorContextValueNotPresent(ExcelUtility.map.get("Tag"),"AdvanceSearch");
		Validations valid =new Validations();
		valid.getResponseValue(map.get("TestCase"),"AdvanceSearch");	
	}
	@Test(enabled=true)
	public void noprogrammeimageValue_Validation() throws IOException {
		ApiKey APIKeyTAG =new ApiKey();
		KeyParameters Kparameter =new KeyParameters();
		APIKeyTAG.getEndpointValidation("AdvanceSearch");
		Kparameter.ErrorContextValueNotPresent(ExcelUtility.map.get("Tag"),"AdvanceSearch");
		Validations valid =new Validations();
		valid.getResponseValue(map.get("TestCase"),"AdvanceSearch");	
	}
	
}

package testcases;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;

import org.json.JSONObject;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;

import assertions.Validations;
import reports.ExtentTestManager;
import commonFunctions.ApiKey;
import commonFunctions.DatabaseConnection;
import commonFunctions.EPGQueries;
import commonFunctions.ExcelUtility;
import commonFunctions.KeyParameters;
import commonFunctions.ResponseValidation;
import io.restassured.module.jsv.JsonSchemaValidator;
import io.restassured.response.Response;
import steps.GetRequestMethod;

@Listeners(reports.TestListener.class)

public class VODCelebrity {
	
	public static HashMap<String, String> map = new HashMap<String, String>();
	 String filePath = System.getProperty("user.dir")+"\\Data";
	 String DynamicURL;
	 String Sheetname ="CelebrityAPI";
	 
	 @BeforeMethod 
	  public void beforeMethod(ITestResult result) throws IOException
	  {
		 DatabaseConnection Connector= new DatabaseConnection();
		 Connector.DBConnection("VOD_DBNAME");
		 map.put("TestCase", result.getMethod().getMethodName());
		 System.out.println(map.get("TestCase"));
	  ExcelUtility excelread=new ExcelUtility();
	  excelread.readExcel(filePath,"StandardAPI.xlsx",Sheetname,map.get("TestCase")); 
	  }
	 @AfterMethod 
	  public void afterMethod(ITestResult result) throws IOException
	  {
		 DatabaseConnection Connector= new DatabaseConnection();
		 Connector.closeDBConnection();
	  }
	 @Test(enabled=true)
		public void Celebrity_DBValidation() throws IOException {
		GetRequestMethod GetReq =new GetRequestMethod(); 
		EPGQueries Queries =new EPGQueries();
		ApiKey APIKeyTAG =new ApiKey();
		KeyParameters PageNo =new KeyParameters();
		Validations valid =new Validations();
		APIKeyTAG.getEndpointValidation("CelebrityAPI");
		PageNo.InValidValuePresent(ExcelUtility.map.get("Tag"));
		Response response=GetReq.dynamicgetRequest(KeyParameters.map.get("DynamicURL")); 
		int ActualStatus= response.getStatusCode();
		String ActualCode=Integer.toString(ActualStatus);
		valid.statusCodeValidation(ActualCode,ExcelUtility.map.get("StatusCode"));
		valid.schemaValidation(response.asString(), JsonSchemaValidator.matchesJsonSchemaInClasspath("Schema/CelebrityAPI.json"));		
		ResponseValidation jsonOperations = new ResponseValidation();
		JSONObject channels = jsonOperations.getChannels(response.asString(),"celebritydetails");
		DatabaseConnection Connector= new DatabaseConnection();
		Queries.Celebrityprogramlist();
		String Query=EPGQueries.map.get("Query");
		Query=Query.replace("%s",ExcelUtility.map.get("CastID") );
		Connector.getResult(Query);
		JSONObject db = Connector.getChannels();
		if (!jsonOperations.validateChannelDetailResult(channels, db))
		{
			System.out.println("Channel Data is as Expected");
			ExtentTestManager.getTest().log(Status.PASS, "PROGRAM DETAILS ARE VALIDATED");  
		} else 
		{ 
			System.out.println("Channel Data Mismatch");
			Assert.fail("Channel Data Mismatch");
			ExtentTestManager.getTest().log(Status.FAIL, "PROGRAM DETAILS ARE MISMATCHED"); 
		}

	 }
	 @Test(enabled=true)
		public void CastDetails_Validation() throws IOException {
		GetRequestMethod GetReq =new GetRequestMethod(); 
		EPGQueries Queries =new EPGQueries();
		ApiKey APIKeyTAG =new ApiKey();
		KeyParameters PageNo =new KeyParameters();
		Validations valid =new Validations();
		APIKeyTAG.getEndpointValidation("CelebrityAPI");
		PageNo.InValidValuePresent(ExcelUtility.map.get("Tag"));
		Response response=GetReq.dynamicgetRequest(KeyParameters.map.get("DynamicURL")); 
		int ActualStatus= response.getStatusCode();
		String ActualCode=Integer.toString(ActualStatus);
		valid.statusCodeValidation(ActualCode,ExcelUtility.map.get("StatusCode"));
		valid.schemaValidation(response.asString(), JsonSchemaValidator.matchesJsonSchemaInClasspath("Schema/CelebrityAPI.json"));		
		ResponseValidation jsonOperations = new ResponseValidation();
		JSONObject channels = jsonOperations.getChannels(response.asString(),"celebritydetails");
		DatabaseConnection Connector= new DatabaseConnection();
		Queries.CastDetails();
		String Query=EPGQueries.map.get("CastDetails");
		Query=Query.replace("%s",ExcelUtility.map.get("CastID") );
		System.out.println(Query);
		Connector.getResult(Query);
		if (!jsonOperations.validateCastDetails(channels))
		{
			ExtentTestManager.getTest().log(Status.PASS, "CAST DETAILS ARE VALIDATED");  
		} else 
		{ 
			Assert.fail("Channel Data Mismatch");
			ExtentTestManager.getTest().log(Status.FAIL, "CAST DETAILS ARE MISMATCHED"); 
		}
	 	}
	 @Test(enabled=true)
		public void CastImageDimension_Validation() throws IOException, SQLException {
		GetRequestMethod GetReq =new GetRequestMethod(); 
		ApiKey APIKeyTAG =new ApiKey();
		KeyParameters PageNo =new KeyParameters();
		Validations valid =new Validations();
		APIKeyTAG.getEndpointValidation("CelebrityAPI");
		PageNo.CastImageValidation();
		Response response=GetReq.dynamicgetRequest(KeyParameters.map.get("DynamicURL")); 
		int ActualStatus= response.getStatusCode();
		String ActualCode=Integer.toString(ActualStatus);
		valid.statusCodeValidation(ActualCode,ExcelUtility.map.get("StatusCode"));
		valid.schemaValidation(response.asString(), JsonSchemaValidator.matchesJsonSchemaInClasspath("Schema/CelebrityAPI.json"));		
		//API response
		ResponseValidation jsonOperations = new ResponseValidation();
		JSONObject channels = jsonOperations.getChannels(response.asString(),"celebritydetails");
		jsonOperations.validateCastImageDimension(channels);
	 	}

	 @Test(enabled=true)
		public void PageSize_Number_Validation() throws IOException {
		 	//API response
			ResponseValidation jsonOperations = new ResponseValidation();
			if (!jsonOperations.PageSize_NumberValidation())
			{
				System.out.println("Channel Data is as Expected");
				ExtentTestManager.getTest().log(Status.PASS, "PROGRAM DETAILS ARE VALIDATED");  
			} else 
			{ 
				System.out.println("Channel Data Mismatch");
				Assert.fail("Channel Data Mismatch");
				ExtentTestManager.getTest().log(Status.FAIL, "PROGRAM DETAILS ARE MISMATCHED"); 
	 }
	 }
	 @Test(enabled=true)
		public void noAPIKey_Validation() throws IOException {
			ApiKey APIKeyTAG =new ApiKey();
			APIKeyTAG.getEndpointValidation("CelebrityAPI");
			APIKeyTAG.noAPIKeyTag(ExcelUtility.map.get("Tag"),"CelebrityAPI");
			Validations valid =new Validations();
			valid.getResponseValue(map.get("TestCase"),Sheetname);
			
		}
	 
	 @Test(enabled=true)
		public void noAPIKeyValue_Validation() throws IOException {
			ApiKey APIKeyTAG =new ApiKey();
			APIKeyTAG.getEndpointValidation("CelebrityAPI");
			APIKeyTAG.noAPIKeyValue(ExcelUtility.map.get("Tag"),"CelebrityAPI"); 
			Validations valid =new Validations();
			valid.getResponseValue(map.get("TestCase"),Sheetname);
		}
	 
	 @Test(enabled=true)
		public void invalidAPIKeyValue_Validation() throws IOException {
			ApiKey APIKeyTAG =new ApiKey();
			APIKeyTAG.getEndpointValidation("CelebrityAPI");
			APIKeyTAG.invalidAPIKeyValue(ExcelUtility.map.get("Tag"),Sheetname);
			Validations valid =new Validations();
			valid.getResponseValue(map.get("TestCase"),Sheetname);	
		}
	 @Test(enabled=true)
		public void validAPIKeyValueOfDifferentProduct_Validation() throws IOException {
			ApiKey APIKeyTAG =new ApiKey();
			APIKeyTAG.getEndpointValidation("CelebrityAPI");
			APIKeyTAG.invalidAPIKeyValue(ExcelUtility.map.get("Tag"),Sheetname);
			Validations valid =new Validations();
			valid.getResponseValue(map.get("TestCase"),Sheetname);	
		}
	 @Test(enabled=true)
		public void noResponseFormatKey_Validation() throws IOException {
		 GetRequestMethod GetReq =new GetRequestMethod(); 
			ApiKey APIKeyTAG =new ApiKey();
			KeyParameters KParameters =new KeyParameters();
			Validations valid =new Validations();
			APIKeyTAG.getEndpointValidation("CelebrityAPI");
			KParameters.TagNotPresent(ExcelUtility.map.get("Tag"));
			Response response=GetReq.dynamicgetRequest(KeyParameters.map.get("DynamicURL")); 
			int ActualStatus= response.getStatusCode();
			String ActualCode=Integer.toString(ActualStatus);
			valid.statusCodeValidation(ActualCode,ExcelUtility.map.get("StatusCode"));
			valid.schemaValidation(response.asString(), JsonSchemaValidator.matchesJsonSchemaInClasspath("Schema/CelebrityAPI.json"));		
			}
	 @Test(enabled=true)
		public void noResponseFormatInvalidValue_Validation() throws IOException {
		 ApiKey APIKeyTAG =new ApiKey();
			APIKeyTAG.getEndpointValidation("CelebrityAPI");
			APIKeyTAG.invalidAPIKeyValue(ExcelUtility.map.get("Tag"),Sheetname);
			Validations valid =new Validations();
			valid.getResponseValue(map.get("TestCase"),Sheetname);	
			}
	 @Test(enabled=true)
		public void noResponseFormatValue_Validation() throws IOException {
		 GetRequestMethod GetReq =new GetRequestMethod(); 
			ApiKey APIKeyTAG =new ApiKey();
			KeyParameters KParameters =new KeyParameters();
			Validations valid =new Validations();
			APIKeyTAG.getEndpointValidation("CelebrityAPI");
			KParameters.TagValueNoPresent(ExcelUtility.map.get("Tag"));
			Response response=GetReq.dynamicgetRequest(KeyParameters.map.get("DynamicURL")); 
			int ActualStatus= response.getStatusCode();
			String ActualCode=Integer.toString(ActualStatus);
			valid.statusCodeValidation(ActualCode,ExcelUtility.map.get("StatusCode"));
			valid.schemaValidation(response.asString(), JsonSchemaValidator.matchesJsonSchemaInClasspath("Schema/CelebrityAPI.json"));		
			}
	 @Test(enabled=true)
		public void noResponseLanguageKey_Validation() throws IOException {
		 GetRequestMethod GetReq =new GetRequestMethod(); 
			ApiKey APIKeyTAG =new ApiKey();
			KeyParameters PageNo =new KeyParameters();
			Validations valid =new Validations();
			APIKeyTAG.getEndpointValidation("CelebrityAPI");
			PageNo.TagNotPresent(ExcelUtility.map.get("Tag"));
			Response response=GetReq.dynamicgetRequest(KeyParameters.map.get("DynamicURL")); 
			int ActualStatus= response.getStatusCode();
			String ActualCode=Integer.toString(ActualStatus);
			valid.statusCodeValidation(ActualCode,ExcelUtility.map.get("StatusCode"));
			valid.schemaValidation(response.asString(), JsonSchemaValidator.matchesJsonSchemaInClasspath("Schema/CelebrityAPI.json"));		
		
		}
		@Test(enabled=true)
		public void noResponseLanguageValue_Validation() throws IOException {
			GetRequestMethod GetReq =new GetRequestMethod(); 
			ApiKey APIKeyTAG =new ApiKey();
			KeyParameters PageNo =new KeyParameters();
			Validations valid =new Validations();
			APIKeyTAG.getEndpointValidation("CelebrityAPI");
			PageNo.TagValueNoPresent(ExcelUtility.map.get("Tag"));
			Response response=GetReq.dynamicgetRequest(KeyParameters.map.get("DynamicURL")); 
			int ActualStatus= response.getStatusCode();
			String ActualCode=Integer.toString(ActualStatus);
			valid.statusCodeValidation(ActualCode,ExcelUtility.map.get("StatusCode"));
			valid.schemaValidation(response.asString(), JsonSchemaValidator.matchesJsonSchemaInClasspath("Schema/CelebrityAPI.json"));		
		}
		 @Test(enabled=true)
			public void noResponseLanguageInvalidValue_Validation() throws IOException {
			 ApiKey APIKeyTAG =new ApiKey();
			 APIKeyTAG.getEndpointValidation("CelebrityAPI");
		     APIKeyTAG.invalidAPIKeyValue(ExcelUtility.map.get("Tag"),Sheetname);
			 Validations valid =new Validations();
			 valid.getResponseValue(map.get("TestCase"),Sheetname);	
				}
		@Test(enabled=true)
		public void noContextTag_Validation() throws IOException {
			ApiKey APIKeyTAG =new ApiKey();
			KeyParameters Kparameter =new KeyParameters();
			APIKeyTAG.getEndpointValidation("CelebrityAPI");
			Kparameter.ErrorTagNotPresent(ExcelUtility.map.get("Tag"),Sheetname);
			Validations valid =new Validations();
			valid.getResponseValue(map.get("TestCase"),Sheetname);	
		}
		@Test(enabled=true)
		public void noContextValue_Validation() throws IOException {
			ApiKey APIKeyTAG =new ApiKey();
			KeyParameters Kparameter =new KeyParameters();
			APIKeyTAG.getEndpointValidation("CelebrityAPI");
			Kparameter.ErrorTagValueNoPresent(ExcelUtility.map.get("Tag"),Sheetname);
			Validations valid =new Validations();
			valid.getResponseValue(map.get("TestCase"),Sheetname);	
		}
		@Test(enabled=true)
		public void noApplicationNameTag_Validation() throws IOException {
			GetRequestMethod GetReq =new GetRequestMethod(); 
			ApiKey APIKeyTAG =new ApiKey();
			KeyParameters KParameters =new KeyParameters();
			Validations valid =new Validations();
			APIKeyTAG.getEndpointValidation("CelebrityAPI");
			KParameters.ContextTagNotPresent(ExcelUtility.map.get("Tag"));
			Response response=GetReq.dynamicgetRequest(KeyParameters.map.get("DynamicURL")); 
			int ActualStatus= response.getStatusCode();
			String ActualCode=Integer.toString(ActualStatus);
			valid.statusCodeValidation(ActualCode,ExcelUtility.map.get("StatusCode"));
			valid.schemaValidation(response.asString(), JsonSchemaValidator.matchesJsonSchemaInClasspath("Schema/CelebrityAPI.json"));		
		}
		@Test(enabled=true)
		public void noApplicationNameValue_Validation() throws IOException {
			ApiKey APIKeyTAG =new ApiKey();
			KeyParameters Kparameter =new KeyParameters();
			APIKeyTAG.getEndpointValidation("CelebrityAPI");
			Kparameter.ErrorContextValueNotPresent(ExcelUtility.map.get("Tag"),"CelebrityAPI");
			Validations valid =new Validations();
			valid.getResponseValue(map.get("TestCase"),"CelebrityAPI");		
		}
		@Test(enabled=true)
		public void noheadendIdTag_Validation() throws IOException {
			ApiKey APIKeyTAG =new ApiKey();
			KeyParameters Kparameter =new KeyParameters();
			APIKeyTAG.getEndpointValidation("CelebrityAPI");
			Kparameter.ErrorTagNotPresent(ExcelUtility.map.get("Tag"),"CelebrityAPI");
			Validations valid =new Validations();
			valid.getResponseValue(map.get("TestCase"),"CelebrityAPI");	
		}
		@Test(enabled=true)
		public void noheadendIdInvalidValue_Validation() throws IOException {
			ApiKey APIKeyTAG =new ApiKey();
			KeyParameters KParameters =new KeyParameters();
			Validations valid =new Validations();
			APIKeyTAG.getEndpointValidation("CelebrityAPI");
			KParameters.ErrorContextInvalidValuePresent(ExcelUtility.map.get("Tag"),"CelebrityAPI");
			valid.getResponseValue(map.get("TestCase"),"CelebrityAPI");	
		}
		@Test(enabled=true)
		public void noheadendIdValue_Validation() throws IOException {
			ApiKey APIKeyTAG =new ApiKey();
			KeyParameters KParameters =new KeyParameters();
			Validations valid =new Validations();
			APIKeyTAG.getEndpointValidation("CelebrityAPI");
			KParameters.ErrorContextValueNotPresent(ExcelUtility.map.get("Tag"),"CelebrityAPI");
			valid.getResponseValue(map.get("TestCase"),"CelebrityAPI");	
		}
		@Test(enabled=true)
		public void noprogrammeimageValue_Validation() throws IOException {
			ApiKey APIKeyTAG =new ApiKey();
			KeyParameters Kparameter =new KeyParameters();
			APIKeyTAG.getEndpointValidation("CelebrityAPI");
			Kparameter.ErrorContextValueNotPresent(ExcelUtility.map.get("Tag"),"CelebrityAPI");
			Validations valid =new Validations();
			valid.getResponseValue(map.get("TestCase"),"CelebrityAPI");	
		}
		@Test(enabled=true)
		public void noprogrammeimageInValidValue_Validation() throws IOException {
			ApiKey APIKeyTAG =new ApiKey();
			KeyParameters Kparameter =new KeyParameters();
			APIKeyTAG.getEndpointValidation("CelebrityAPI");
			Kparameter.ErrorContextInvalidValuePresent(ExcelUtility.map.get("Tag"),"CelebrityAPI");
			Validations valid =new Validations();
			valid.getResponseValue(map.get("TestCase"),"CelebrityAPI");	
		}
		@Test(enabled=true)
		public void noprogrammeimageTagValue_Validation() throws IOException {
			ApiKey APIKeyTAG =new ApiKey();
			KeyParameters Kparameter =new KeyParameters();
			APIKeyTAG.getEndpointValidation("CelebrityAPI");
			Kparameter.ErrorContextTagNotPresent(ExcelUtility.map.get("Tag"),"CelebrityAPI");
			Validations valid =new Validations();
			valid.getResponseValue(map.get("TestCase"),"CelebrityAPI");	
		}
		@Test(enabled=true)
		public void noprogrammeimageHeightValue_Validation() throws IOException {
			ApiKey APIKeyTAG =new ApiKey();
			KeyParameters Kparameter =new KeyParameters();
			APIKeyTAG.getEndpointValidation("CelebrityAPI");
			Kparameter.ErrorTagValueNoPresent(ExcelUtility.map.get("Tag"),"CelebrityAPI");
			Validations valid =new Validations();
			valid.getResponseValue(map.get("TestCase"),"CelebrityAPI");	
		}
		@Test(enabled=true)
		public void noprogrammeimageHeightInValidValue_Validation() throws IOException {
			ApiKey APIKeyTAG =new ApiKey();
			KeyParameters Kparameter =new KeyParameters();
			APIKeyTAG.getEndpointValidation("CelebrityAPI");
			Kparameter.ErrorInValidValue(ExcelUtility.map.get("Tag"),"CelebrityAPI");
			Validations valid =new Validations();
			valid.getResponseValue(map.get("TestCase"),"CelebrityAPI");	
		}
		@Test(enabled=true)
		public void noprogrammeimageHeightTagValue_Validation() throws IOException {
			ApiKey APIKeyTAG =new ApiKey();
			KeyParameters Kparameter =new KeyParameters();
			APIKeyTAG.getEndpointValidation("CelebrityAPI");
			Kparameter.ContextNoTagPresent_NoValue(ExcelUtility.map.get("Tag"),"CelebrityAPI");
			Validations valid =new Validations();
			valid.getResponseValue(map.get("TestCase"),"CelebrityAPI");	
		}
		
		@Test(enabled=true)
		public void noCastImageWidthTagValue_Validation() throws IOException {
			ApiKey APIKeyTAG =new ApiKey();
			KeyParameters Kparameter =new KeyParameters();
			APIKeyTAG.getEndpointValidation("CelebrityAPI");
			Kparameter.ErrorTagValueNoPresent(ExcelUtility.map.get("Tag"),"CelebrityAPI");
			Validations valid =new Validations();
			valid.getResponseValue(map.get("TestCase"),"CelebrityAPI");	
		}
		@Test(enabled=true)
		public void noCastImageWidthInvalidValue_Validation() throws IOException {
			ApiKey APIKeyTAG =new ApiKey();
			KeyParameters Kparameter =new KeyParameters();
			APIKeyTAG.getEndpointValidation("CelebrityAPI");
			Kparameter.ErrorInValidValue(ExcelUtility.map.get("Tag"),"CelebrityAPI");
			Validations valid =new Validations();
			valid.getResponseValue(map.get("TestCase"),Sheetname);	
		}
		@Test(enabled=true)
		public void noCastImageWidthTag_Validation() throws IOException {
			ApiKey APIKeyTAG =new ApiKey();
			KeyParameters Kparameter =new KeyParameters();
			APIKeyTAG.getEndpointValidation("CelebrityAPI");
			Kparameter.ErrorTagNotPresent(ExcelUtility.map.get("Tag"),"CelebrityAPI");
			Validations valid =new Validations();
			valid.getResponseValue(map.get("TestCase"),Sheetname);	
		}
		@Test(enabled=true)
		public void noCastImageHeightTagValue_Validation() throws IOException {
			ApiKey APIKeyTAG =new ApiKey();
			KeyParameters Kparameter =new KeyParameters();
			APIKeyTAG.getEndpointValidation("CelebrityAPI");
			Kparameter.ErrorTagValueNoPresent(ExcelUtility.map.get("Tag"),"CelebrityAPI");
			Validations valid =new Validations();
			valid.getResponseValue(map.get("TestCase"),"CelebrityAPI");	
		}
		@Test(enabled=true)
		public void noCastImageHeightInvalidValue_Validation() throws IOException {
			ApiKey APIKeyTAG =new ApiKey();
			KeyParameters Kparameter =new KeyParameters();
			APIKeyTAG.getEndpointValidation("CelebrityAPI");
			Kparameter.ErrorInValidValue(ExcelUtility.map.get("Tag"),"CelebrityAPI");
			Validations valid =new Validations();
			valid.getResponseValue(map.get("TestCase"),"CelebrityAPI");	
		}
		@Test(enabled=true)
		public void noCastImageheightTag_Validation() throws IOException {
			ApiKey APIKeyTAG =new ApiKey();
			KeyParameters Kparameter =new KeyParameters();
			APIKeyTAG.getEndpointValidation("CelebrityAPI");
			Kparameter.ErrorTagNotPresent(ExcelUtility.map.get("Tag"),"CelebrityAPI");
			Validations valid =new Validations();
			valid.getResponseValue(map.get("TestCase"),"CelebrityAPI");	
		}
		@Test(enabled=true)
		public void noCastIDTagValue_Validation() throws IOException {
			ApiKey APIKeyTAG =new ApiKey();
			KeyParameters Kparameter =new KeyParameters();
			APIKeyTAG.getEndpointValidation("CelebrityAPI");
			Kparameter.ErrorTagValueNoPresent(ExcelUtility.map.get("Tag"),"CelebrityAPI");
			Validations valid =new Validations();
			valid.getResponseValue(map.get("TestCase"),Sheetname);	
		}
		@Test(enabled=true)
		public void noCastIDInvalidValue_Validation() throws IOException {
			ApiKey APIKeyTAG =new ApiKey();
			KeyParameters Kparameter =new KeyParameters();
			APIKeyTAG.getEndpointValidation("CelebrityAPI");
			Kparameter.ErrorInValidValue(ExcelUtility.map.get("Tag"),"CelebrityAPI");
			Validations valid =new Validations();
			valid.getResponseValue(map.get("TestCase"),Sheetname);	
		}
		@Test(enabled=true)
		public void noCastIDTag_Validation() throws IOException {
			ApiKey APIKeyTAG =new ApiKey();
			KeyParameters Kparameter =new KeyParameters();
			APIKeyTAG.getEndpointValidation("CelebrityAPI");
			Kparameter.ErrorTagNotPresent(ExcelUtility.map.get("Tag"),"CelebrityAPI");
			Validations valid =new Validations();
			valid.getResponseValue(map.get("TestCase"),Sheetname);	
		}
		@Test(enabled=true)
		public void noPagenoTagValue_Validation() throws IOException {
			ApiKey APIKeyTAG =new ApiKey();
			KeyParameters Kparameter =new KeyParameters();
			APIKeyTAG.getEndpointValidation("CelebrityAPI");
			Kparameter.ErrorTagValueNoPresent(ExcelUtility.map.get("Tag"),"CelebrityAPI");
			Validations valid =new Validations();
			valid.getResponseValue(map.get("TestCase"),Sheetname);	
		}
		@Test(enabled=true)
		public void noPagenoInvalidValue_Validation() throws IOException {
			ApiKey APIKeyTAG =new ApiKey();
			KeyParameters Kparameter =new KeyParameters();
			APIKeyTAG.getEndpointValidation("CelebrityAPI");
			Kparameter.ErrorInValidValue(ExcelUtility.map.get("Tag"),"CelebrityAPI");
			Validations valid =new Validations();
			valid.getResponseValue(map.get("TestCase"),Sheetname);	
		}
		@Test(enabled=true)
		public void noPagenoTag_Validation() throws IOException {
			ApiKey APIKeyTAG =new ApiKey();
			KeyParameters Kparameter =new KeyParameters();
			APIKeyTAG.getEndpointValidation("CelebrityAPI");
			Kparameter.ErrorTagNotPresent(ExcelUtility.map.get("Tag"),"CelebrityAPI");
			Validations valid =new Validations();
			valid.getResponseValue(map.get("TestCase"),Sheetname);	
		}
		@Test(enabled=true)
		public void noPageSizeTagValue_Validation() throws IOException {
			ApiKey APIKeyTAG =new ApiKey();
			KeyParameters Kparameter =new KeyParameters();
			APIKeyTAG.getEndpointValidation("CelebrityAPI");
			Kparameter.LogoType_Value(ExcelUtility.map.get("Tag"));
			APIKeyTAG.toFetchResponseAttributesCelebrityAPI();
			Validations valid =new Validations();
			valid.getResponseValue(map.get("TestCase"),Sheetname);	
		}
		@Test(enabled=true)
		public void noPageSizeInvalidValue_Validation() throws IOException {
			ApiKey APIKeyTAG =new ApiKey();
			KeyParameters Kparameter =new KeyParameters();
			APIKeyTAG.getEndpointValidation("CelebrityAPI");
			Kparameter.LogoType_InvalidValue(ExcelUtility.map.get("Tag"),Sheetname);
			APIKeyTAG.toFetchResponseAttributesCelebrityAPI();
			Validations valid =new Validations();
			valid.getResponseValue(map.get("TestCase"),Sheetname);	
		}
		@Test(enabled=true)
		public void noPageSizeTag_Validation() throws IOException {
			ApiKey APIKeyTAG =new ApiKey();
			KeyParameters Kparameter =new KeyParameters();
			APIKeyTAG.getEndpointValidation("CelebrityAPI");
			Kparameter.LogoType_Tag(ExcelUtility.map.get("Tag"));
			APIKeyTAG.toFetchResponseAttributesCelebrityAPI();
			Validations valid =new Validations();
			valid.getResponseValue(map.get("TestCase"),Sheetname);	
		}
		@Test(enabled=true)
		public void noPageSizeInvalidNumberValue_Validation() throws IOException {
			ApiKey APIKeyTAG =new ApiKey();
			KeyParameters Kparameter =new KeyParameters();
			Validations valid =new Validations();
			APIKeyTAG.getEndpointValidation("CelebrityAPI");
			Kparameter.LogoType_InvalidValue(ExcelUtility.map.get("Tag"),Sheetname);
			valid.comparePageSize();
			APIKeyTAG.toFetchResponseAttributesCelebrityAPI();
			valid.getResponseValue(map.get("TestCase"),Sheetname);	
			
		}
		
	 }

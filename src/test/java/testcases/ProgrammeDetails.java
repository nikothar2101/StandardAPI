package testcases;

import java.io.IOException;
import java.util.HashMap;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import assertions.Validations;
import commonFunctions.ApiKey;
import commonFunctions.DatabaseConnection;
import commonFunctions.ExcelUtility;
import commonFunctions.KeyParameters;
import commonFunctions.ResponseValidation;
import io.restassured.module.jsv.JsonSchemaValidator;
import io.restassured.response.Response;
import steps.GetRequestMethod;

@Listeners(reports.TestListener.class)

public class ProgrammeDetails {

	public static HashMap<String, String> map = new HashMap<String, String>();
	 String filePath = System.getProperty("user.dir")+"\\Data";
	 String DynamicURL;
	 String Sheetname ="ProgrammeDetails";
	 		

		  @BeforeMethod 
		  public void beforeMethod(ITestResult result) throws IOException
		  { 
		  map.put("TestCase", result.getMethod().getMethodName()); 
		  ExcelUtility excelread=new ExcelUtility();
		  excelread.readExcel(filePath,"StandardAPI.xlsx",Sheetname,map.get("TestCase")); 
			
			  DatabaseConnection Connector= new DatabaseConnection();
			 
			  if(result.getMethod().getMethodName()=="ProgrammeID_Validation" || result.getMethod().getMethodName()=="Manadatory_DataValidation"
					  ||result.getMethod().getMethodName()=="Optional_EN__DataValidation") {
				  Connector.DBConnection("VOD_DBNAME");
				  }
				  else {
					  Connector.DBConnection("EPGCLOUD");  
				  }
			  
			 
		  }

		  @AfterMethod 
		  public void afterMethod(ITestResult result) throws IOException
		  {
			DatabaseConnection Connector= new DatabaseConnection();
			 Connector.closeDBConnection();
		  }
		  
		  @Test(enabled=true)
			public void noAPIKey_Validation() throws IOException {
				ApiKey APIKeyTAG =new ApiKey();
				APIKeyTAG.getEndpointValidation("ProgrammeDetails");
				APIKeyTAG.noAPIKeyTag(ExcelUtility.map.get("Tag"),Sheetname);
				Validations valid =new Validations();
				valid.getResponseValue(map.get("TestCase"),Sheetname);
				
			}
		 
		 @Test(enabled=false)
			public void noAPIKeyValue_Validation() throws IOException {
				ApiKey APIKeyTAG =new ApiKey();
				APIKeyTAG.getEndpointValidation("ProgrammeDetails");
				APIKeyTAG.noAPIKeyValue(ExcelUtility.map.get("Tag"),Sheetname); 
				Validations valid =new Validations();
				valid.getResponseValue(map.get("TestCase"),Sheetname);
			}
		 
		 @Test(enabled=false)
			public void invalidAPIKeyValue_Validation() throws IOException {
				ApiKey APIKeyTAG =new ApiKey();
				APIKeyTAG.getEndpointValidation("ProgrammeDetails");
				APIKeyTAG.invalidAPIKeyValue(ExcelUtility.map.get("Tag"),Sheetname);
				Validations valid =new Validations();
				valid.getResponseValue(map.get("TestCase"),Sheetname);	
			}
		 @Test(enabled=false)
			public void validAPIKeyValueOfDifferentProduct_Validation() throws IOException {
				ApiKey APIKeyTAG =new ApiKey();
				APIKeyTAG.getEndpointValidation("ProgrammeDetails");
				APIKeyTAG.invalidAPIKeyValue(ExcelUtility.map.get("Tag"),Sheetname);
				Validations valid =new Validations();
				valid.getResponseValue(map.get("TestCase"),Sheetname);	
			}
		 
		 @Test(enabled=true)
			public void noResponseFormatKey_Validation() throws IOException {
				
				GetRequestMethod GetReq =new GetRequestMethod(); 
				ApiKey APIKeyTAG =new ApiKey();
				Validations valid =new Validations();
				KeyParameters ResponseLanguage =new KeyParameters();
				
				APIKeyTAG.getEndpointValidation("ProgrammeDetails");
				ResponseLanguage.TagNotPresent(ExcelUtility.map.get("Tag"));
				Response response=GetReq.dynamicgetRequest(KeyParameters.map.get("DynamicURL")); 
				int ActualStatus= response.getStatusCode();
				String ActualCode=Integer.toString(ActualStatus);
				valid.statusCodeValidation(ActualCode,ExcelUtility.map.get("StatusCode"));
				valid.schemaValidation(response.asString(), JsonSchemaValidator.matchesJsonSchemaInClasspath("Schema/ProgrammeDetails.json"));		
				
				}
			@Test(enabled=true)
			public void noResponseFormatValue_Validation() throws IOException {
				GetRequestMethod GetReq =new GetRequestMethod(); 
				ApiKey APIKeyTAG =new ApiKey();
				Validations valid =new Validations();
				KeyParameters ResponseLanguage =new KeyParameters();
				
				APIKeyTAG.getEndpointValidation("ProgrammeDetails");
				ResponseLanguage.TagValueNoPresent(ExcelUtility.map.get("Tag"));
				Response response=GetReq.dynamicgetRequest(KeyParameters.map.get("DynamicURL")); 
				int ActualStatus= response.getStatusCode();
				String ActualCode=Integer.toString(ActualStatus);
				valid.statusCodeValidation(ActualCode,ExcelUtility.map.get("StatusCode"));
				valid.schemaValidation(response.asString(), JsonSchemaValidator.matchesJsonSchemaInClasspath("Schema/ProgrammeDetails.json"));		
				
				}
			@Test(enabled=true)
			public void noResponseFormatInvalidValue_Validation() throws IOException {
				ApiKey APIKeyTAG =new ApiKey();
				APIKeyTAG.getEndpointValidation("ProgrammeDetails");
				APIKeyTAG.invalidAPIKeyValue(ExcelUtility.map.get("Tag"),Sheetname);
				Validations valid =new Validations();
				valid.getResponseValue(map.get("TestCase"),Sheetname);	
				}
			 @Test(enabled=true)
				public void noContextTag_Validation() throws IOException {
					ApiKey APIKeyTAG =new ApiKey();
					KeyParameters Kparameter =new KeyParameters();
					APIKeyTAG.getEndpointValidation("ProgrammeDetails");
					Kparameter.ErrorTagNotPresent(ExcelUtility.map.get("Tag"),Sheetname);
					Validations valid =new Validations();
					valid.getResponseValue(map.get("TestCase"),Sheetname);	
				}
				@Test(enabled=true)
				public void noContextValue_Validation() throws IOException {
					ApiKey APIKeyTAG =new ApiKey();
					KeyParameters Kparameter =new KeyParameters();
					APIKeyTAG.getEndpointValidation("ProgrammeDetails");
					Kparameter.ErrorTagValueNoPresent(ExcelUtility.map.get("Tag"),Sheetname);
					Validations valid =new Validations();
					valid.getResponseValue(map.get("TestCase"),Sheetname);	
				}
				@Test(enabled=false)
				public void noApplicationNameTag_Validation() throws IOException {
					GetRequestMethod GetReq =new GetRequestMethod(); 
					ApiKey APIKeyTAG =new ApiKey();
					KeyParameters KParameters =new KeyParameters();
					Validations valid =new Validations();
					APIKeyTAG.getEndpointValidation("ProgrammeDetails");
					KParameters.ContextTagNotPresent(ExcelUtility.map.get("Tag"));
					Response response=GetReq.dynamicgetRequest(KeyParameters.map.get("DynamicURL")); 
					int ActualStatus= response.getStatusCode();
					String ActualCode=Integer.toString(ActualStatus);
					valid.statusCodeValidation(ActualCode,ExcelUtility.map.get("StatusCode"));
					valid.schemaValidation(response.asString(), JsonSchemaValidator.matchesJsonSchemaInClasspath("Schema/ProgrammeDetails.json"));		
				}
				@Test(enabled=false)
				public void noApplicationTagValue_Validation() throws IOException {
					ApiKey APIKeyTAG =new ApiKey();
					KeyParameters KParameters =new KeyParameters();
					Validations valid =new Validations();
					APIKeyTAG.getEndpointValidation("ProgrammeDetails");
					KParameters.ErrorContextValueNotPresent(ExcelUtility.map.get("Tag"),Sheetname);
					valid.getResponseValue(map.get("TestCase"),Sheetname);	
				}
				 @Test(enabled=false)
					public void noheadendIdTag_Validation() throws IOException {
						ApiKey APIKeyTAG =new ApiKey();
						KeyParameters Kparameter =new KeyParameters();
						APIKeyTAG.getEndpointValidation("ProgrammeDetails");
						Kparameter.ErrorContextTagNotPresent(ExcelUtility.map.get("Tag"),Sheetname);
						Validations valid =new Validations();
						valid.getResponseValue(map.get("TestCase"),Sheetname);	
					}
				 @Test(enabled=false)
					public void noheadendIdValue_Validation() throws IOException {
						ApiKey APIKeyTAG =new ApiKey();
						KeyParameters Kparameter =new KeyParameters();
						APIKeyTAG.getEndpointValidation("ProgrammeDetails");
						Kparameter.ErrorContextValueNotPresent(ExcelUtility.map.get("Tag"),Sheetname);
						Validations valid =new Validations();
						valid.getResponseValue(map.get("TestCase"),Sheetname);	
					}
				 @Test(enabled=false)
					public void noheadendIdInvalidValue_Validation() throws IOException {
						ApiKey APIKeyTAG =new ApiKey();
						KeyParameters Kparameter =new KeyParameters();
						APIKeyTAG.getEndpointValidation("ProgrammeDetails");
						Kparameter.ErrorContextInvalidValuePresent(ExcelUtility.map.get("Tag"),Sheetname);
						Validations valid =new Validations();
						valid.getResponseValue(map.get("TestCase"),Sheetname);	
					}
				 
				 @Test(enabled=true)
					public void noprogrammeimageValue_Validation() throws IOException {
						ApiKey APIKeyTAG =new ApiKey();
						KeyParameters Kparameter =new KeyParameters();
						APIKeyTAG.getEndpointValidation("ProgrammeDetails");
						Kparameter.ErrorContextValueNotPresent(ExcelUtility.map.get("Tag"),Sheetname);
						Validations valid =new Validations();
						valid.getResponseValue(map.get("TestCase"),Sheetname);	
					}
					@Test(enabled=true)
					public void noprogrammeimageInValidValue_Validation() throws IOException {
						ApiKey APIKeyTAG =new ApiKey();
						KeyParameters Kparameter =new KeyParameters();
						APIKeyTAG.getEndpointValidation("ProgrammeDetails");
						Kparameter.ErrorContextInvalidValuePresent(ExcelUtility.map.get("Tag"),Sheetname);
						Validations valid =new Validations();
						valid.getResponseValue(map.get("TestCase"),Sheetname);	
					}
					@Test(enabled=true)
					public void noprogrammeimageTagValue_Validation() throws IOException {
						ApiKey APIKeyTAG =new ApiKey();
						KeyParameters Kparameter =new KeyParameters();
						APIKeyTAG.getEndpointValidation("ProgrammeDetails");
						Kparameter.ErrorContextTagNotPresent(ExcelUtility.map.get("Tag"),Sheetname);
						Validations valid =new Validations();
						valid.getResponseValue(map.get("TestCase"),Sheetname);	
					}
					@Test(enabled=true)
					public void noprogrammeimageHeightValue_Validation() throws IOException {
						ApiKey APIKeyTAG =new ApiKey();
						KeyParameters Kparameter =new KeyParameters();
						APIKeyTAG.getEndpointValidation("ProgrammeDetails");
						Kparameter.ErrorTagValueNoPresent(ExcelUtility.map.get("Tag"),Sheetname);
						Validations valid =new Validations();
						valid.getResponseValue(map.get("TestCase"),Sheetname);	
					}
					@Test(enabled=true)
					public void noprogrammeimageHeightInValidValue_Validation() throws IOException {
						ApiKey APIKeyTAG =new ApiKey();
						KeyParameters Kparameter =new KeyParameters();
						APIKeyTAG.getEndpointValidation("ProgrammeDetails");
						Kparameter.ErrorInValidValue(ExcelUtility.map.get("Tag"),Sheetname);
						Validations valid =new Validations();
						valid.getResponseValue(map.get("TestCase"),Sheetname);	
					}
					@Test(enabled=true)
					public void noprogrammeimageHeightTagValue_Validation() throws IOException {
						ApiKey APIKeyTAG =new ApiKey();
						KeyParameters Kparameter =new KeyParameters();
						APIKeyTAG.getEndpointValidation("ProgrammeDetails");
						Kparameter.ContextNoTagPresent_NoValue(ExcelUtility.map.get("Tag"),Sheetname);
						Validations valid =new Validations();
						valid.getResponseValue(map.get("TestCase"),Sheetname);	
					}
					
					@Test(enabled=true)
					public void noCastImageWidthTagValue_Validation() throws IOException {
						ApiKey APIKeyTAG =new ApiKey();
						KeyParameters Kparameter =new KeyParameters();
						APIKeyTAG.getEndpointValidation("P6ProgrammeDetails");
						Kparameter.LogoType_Value(ExcelUtility.map.get("Tag"));
						APIKeyTAG.toFetchResponseAttributesCelebrityAPI();
						Validations valid =new Validations();
						valid.getResponseValue(map.get("TestCase"),Sheetname);	
					}
					@Test(enabled=true)
					public void noCastImageWidthInvalidValue_Validation() throws IOException {
				    	
						ApiKey APIKeyTAG =new ApiKey();
						KeyParameters Kparameter =new KeyParameters();
						APIKeyTAG.getEndpointValidation("P6ProgrammeDetails");
						Kparameter.LogoType_InvalidValue(ExcelUtility.map.get("Tag"),Sheetname);
						APIKeyTAG.toFetchResponseAttributesCelebrityAPI();
						Validations valid =new Validations();
						valid.getResponseValue(map.get("TestCase"),Sheetname);
						}
					@Test(enabled=true)
					public void noCastImageWidthTag_Validation() throws IOException {
						ApiKey APIKeyTAG =new ApiKey();
						KeyParameters Kparameter =new KeyParameters();
						APIKeyTAG.getEndpointValidation("P6ProgrammeDetails");
						Kparameter.LogoType_Tag(ExcelUtility.map.get("Tag"));
						APIKeyTAG.toFetchResponseAttributesCelebrityAPI();
						Validations valid =new Validations();
						valid.getResponseValue(map.get("TestCase"),Sheetname);	
					}
					@Test(enabled=true)
					public void noCastImageHeightTagValue_Validation() throws IOException {
						ApiKey APIKeyTAG =new ApiKey();
						KeyParameters Kparameter =new KeyParameters();
						APIKeyTAG.getEndpointValidation("P6ProgrammeDetails");
						Kparameter.ErrorTagValueNoPresent(ExcelUtility.map.get("Tag"),Sheetname);
						Validations valid =new Validations();
						valid.getResponseValue(map.get("TestCase"),Sheetname);	
					}
					@Test(enabled=true)
					public void noCastImageHeightInvalidValue_Validation() throws IOException {
						ApiKey APIKeyTAG =new ApiKey();
						APIKeyTAG.getEndpointValidation("P6ProgrammeDetails");
						APIKeyTAG.invalidAPIKeyValue(ExcelUtility.map.get("Tag"),Sheetname);
						Validations valid =new Validations();
						valid.getResponseValue(map.get("TestCase"),Sheetname);	
					
					}
					@Test(enabled=true)
					public void noCastImageheightTag_Validation() throws IOException {
						ApiKey APIKeyTAG =new ApiKey();
						KeyParameters Kparameter =new KeyParameters();
						APIKeyTAG.getEndpointValidation("P6ProgrammeDetails");
						Kparameter.ErrorTagNotPresent(ExcelUtility.map.get("Tag"),Sheetname);
						Validations valid =new Validations();
						valid.getResponseValue(map.get("TestCase"),Sheetname);	
					}
					@Test(enabled=true)
					public void noProgrammeIDTagValue() throws IOException {
						ApiKey APIKeyTAG =new ApiKey();
						KeyParameters Kparameter =new KeyParameters();
						APIKeyTAG.getEndpointValidation("P6ProgrammeDetails");
						Kparameter.ErrorTagValueNoPresent(ExcelUtility.map.get("Tag"),Sheetname);
						Validations valid =new Validations();
						valid.getResponseValue(map.get("TestCase"),Sheetname);	
					}
					@Test(enabled=true)
					public void noProgrammeIDInvalidValue() throws IOException {
						ApiKey APIKeyTAG =new ApiKey();
						APIKeyTAG.getEndpointValidation("P6ProgrammeDetails");
						APIKeyTAG.invalidAPIKeyValue(ExcelUtility.map.get("Tag"),Sheetname);
						Validations valid =new Validations();
						valid.getResponseValue(map.get("TestCase"),Sheetname);	
					
					}
					@Test(enabled=true)
					public void noProgrammeIDTag() throws IOException {
						ApiKey APIKeyTAG =new ApiKey();
						KeyParameters Kparameter =new KeyParameters();
						APIKeyTAG.getEndpointValidation("P6ProgrammeDetails");
						Kparameter.ErrorTagNotPresent(ExcelUtility.map.get("Tag"),Sheetname);
						Validations valid =new Validations();
						valid.getResponseValue(map.get("TestCase"),Sheetname);	
					}
					
					 @Test(enabled=true)
						public void noResponseLanguageKey_Validation() throws IOException {
							
							GetRequestMethod GetReq =new GetRequestMethod(); 
							ApiKey APIKeyTAG =new ApiKey();
							Validations valid =new Validations();
							KeyParameters ResponseLanguage =new KeyParameters();
							
							APIKeyTAG.getEndpointValidation("ProgrammeDetails");
							ResponseLanguage.TagNotPresent(ExcelUtility.map.get("Tag"));
							Response response=GetReq.dynamicgetRequest(KeyParameters.map.get("DynamicURL")); 
							int ActualStatus= response.getStatusCode();
							String ActualCode=Integer.toString(ActualStatus);
							valid.statusCodeValidation(ActualCode,ExcelUtility.map.get("StatusCode"));
							valid.schemaValidation(response.asString(), JsonSchemaValidator.matchesJsonSchemaInClasspath("Schema/ProgrammeDetails_RL.json"));		
							
							}
					 @Test(enabled=true)
						public void ResponseLanguageValue_Validation() throws IOException {
							
							GetRequestMethod GetReq =new GetRequestMethod(); 
							ApiKey APIKeyTAG =new ApiKey();
							Validations valid =new Validations();
							KeyParameters ResponseLanguage =new KeyParameters();
							
							APIKeyTAG.getEndpointValidation("ProgrammeDetails");
							ResponseLanguage.InValidValuePresent(ExcelUtility.map.get("Tag"));
							Response response=GetReq.dynamicgetRequest(KeyParameters.map.get("DynamicURL")); 
							int ActualStatus= response.getStatusCode();
							String ActualCode=Integer.toString(ActualStatus);
							valid.statusCodeValidation(ActualCode,ExcelUtility.map.get("StatusCode"));
							valid.schemaValidation(response.asString(), JsonSchemaValidator.matchesJsonSchemaInClasspath("Schema/ProgrammeDetailsLanguage.json"));		
							
							}
						@Test(enabled=true)
						public void noResponseLanguageValue_Validation() throws IOException {
							ResponseValidation APIKeyTAG =new ResponseValidation();
							APIKeyTAG.noResponseLanguageValue();
							
							
							}
						@Test(enabled=true)
						public void noResponseLanguageInvalidValue_Validation() throws IOException {
							ApiKey APIKeyTAG =new ApiKey();
							APIKeyTAG.getEndpointValidation("ProgrammeDetails");
							APIKeyTAG.invalidAPIKeyValue(ExcelUtility.map.get("Tag"),Sheetname);
							Validations valid =new Validations();
							valid.getResponseValue(map.get("TestCase"),Sheetname);	
							}
						@Test(enabled=true)
						public void LanguageAssigned_Validation() throws IOException {
							ResponseValidation APIKeyTAG =new ResponseValidation();
							APIKeyTAG.ResponseLanguage_Validation();
							}
					
						@Test(enabled=true)
						public void ProgrammeID_Validation() throws IOException {
							ResponseValidation APIKeyTAG =new ResponseValidation();
							APIKeyTAG.ProgrammeID_Validation();
							}
						@Test(enabled=true)
						public void Manadatory_DataValidation() throws IOException {
							ResponseValidation APIKeyTAG =new ResponseValidation();
							APIKeyTAG.Manadate_DataValidation();
							}
						@Test(enabled=true)
						public void Optional_EN__DataValidation() throws IOException {
							ResponseValidation APIKeyTAG =new ResponseValidation();
							APIKeyTAG.Optional_EN_DataValidation();
							}
						@Test(enabled=true)
						public void Optional_Other__DataValidation() throws IOException {
							ResponseValidation APIKeyTAG =new ResponseValidation();
							APIKeyTAG.Optional_EN_DataValidation();
							}
}

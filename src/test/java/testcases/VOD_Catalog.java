package testcases;

import java.io.IOException;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;

import assertions.Validations;
import commonFunctions.ApiKey;
import commonFunctions.DatabaseConnection;
import commonFunctions.EPGQueries;
import commonFunctions.ExcelUtility;
import commonFunctions.KeyParameters;
import commonFunctions.ResponseValidation;
import commonFunctions.VODQueries;
import io.restassured.module.jsv.JsonSchemaValidator;
import io.restassured.response.Response;
import reports.ExtentTestManager;
import steps.GetRequestMethod;

@Listeners(reports.TestListener.class)
public class VOD_Catalog {
	
	public static HashMap<String, String> map = new HashMap<String, String>();
	 String filePath = System.getProperty("user.dir")+"\\Data";
	 String DynamicURL;
	 String Sheetname ="VOD_Catalog";
	 		

		  @BeforeMethod 
		  public void beforeMethod(ITestResult result) throws IOException
		  { 
		  map.put("TestCase", result.getMethod().getMethodName()); 
		  ExcelUtility excelread=new ExcelUtility();
		  excelread.readExcel(filePath,"StandardAPI.xlsx","VOD_Catalog",map.get("TestCase")); 
		  DatabaseConnection Connector= new DatabaseConnection();
		  Connector.DBConnection("VOD_DBNAME");
		  }
		  @AfterMethod 
		  public void afterMethod(ITestResult result) throws IOException
		  {
			 DatabaseConnection Connector= new DatabaseConnection();
			 Connector.closeDBConnection();
		  }
		  @Test(enabled=true)
			public void noAPIKey_Validation() throws IOException {
				ApiKey APIKeyTAG =new ApiKey();
				APIKeyTAG.getEndpointValidation("VOD_Catalog");
				APIKeyTAG.noAPIKeyTag(ExcelUtility.map.get("Tag"),"VOD_Catalog");
				Validations valid =new Validations();
				valid.getResponseValue(map.get("TestCase"),"VOD_Catalog");
				
			}
			@Test(enabled=true)
			public void noAPIKeyValue_Validation() throws IOException {
				ApiKey APIKeyTAG =new ApiKey();
				APIKeyTAG.getEndpointValidation("VOD_Catalog");
				APIKeyTAG.noAPIKeyValue(ExcelUtility.map.get("Tag"),"VOD_Catalog"); 
				Validations valid =new Validations();
				valid.getResponseValue(map.get("TestCase"),"VOD_Catalog");
			}
			@Test(enabled=true)
			public void validAPIKeyValueOfDifferentProduct_Validation() throws IOException {
				ApiKey APIKeyTAG =new ApiKey();
				APIKeyTAG.getEndpointValidation("VOD_Catalog");
				APIKeyTAG.validAPIKeyValueOfDifferentProduct(ExcelUtility.map.get("Tag"),"VOD_Catalog");
				Validations valid =new Validations();
				valid.getResponseValue(map.get("TestCase"),"VOD_Catalog");	
			}
			@Test(enabled=true)
			public void invalidAPIKeyValue_Validation() throws IOException {
				ApiKey APIKeyTAG =new ApiKey();
				APIKeyTAG.getEndpointValidation("VOD_Catalog");
				APIKeyTAG.invalidAPIKeyValue(ExcelUtility.map.get("Tag"),"VOD_Catalog");
				Validations valid =new Validations();
				valid.getResponseValue(map.get("TestCase"),"VOD_Catalog");	
			}
			 @Test(enabled=true)
				public void noResponseFormatKey_Validation() throws IOException {
				 GetRequestMethod GetReq =new GetRequestMethod(); 
					ApiKey APIKeyTAG =new ApiKey();
					KeyParameters KParameters =new KeyParameters();
					Validations valid =new Validations();
					APIKeyTAG.getEndpointValidation("VOD_Catalog");
					KParameters.TagNotPresent(ExcelUtility.map.get("Tag"));
					Response response=GetReq.dynamicgetRequest(KeyParameters.map.get("DynamicURL")); 
					int ActualStatus= response.getStatusCode();
					String ActualCode=Integer.toString(ActualStatus);
					valid.statusCodeValidation(ActualCode,ExcelUtility.map.get("StatusCode"));
					valid.schemaValidation(response.asString(), JsonSchemaValidator.matchesJsonSchemaInClasspath("Schema/VODCatalog.json"));		
					}
			 @Test(enabled=true)
				public void noResponseFormatInvalidValue_Validation() throws IOException {
				 ApiKey APIKeyTAG =new ApiKey();
					APIKeyTAG.getEndpointValidation("VOD_Catalog");
					APIKeyTAG.invalidAPIKeyValue(ExcelUtility.map.get("Tag"),Sheetname);
					Validations valid =new Validations();
					valid.getResponseValue(map.get("TestCase"),Sheetname);	
					}
			 @Test(enabled=true)
				public void noResponseFormatValue_Validation() throws IOException {
				 GetRequestMethod GetReq =new GetRequestMethod(); 
					ApiKey APIKeyTAG =new ApiKey();
					KeyParameters KParameters =new KeyParameters();
					Validations valid =new Validations();
					APIKeyTAG.getEndpointValidation("VOD_Catalog");
					KParameters.TagValueNoPresent(ExcelUtility.map.get("Tag"));
					Response response=GetReq.dynamicgetRequest(KeyParameters.map.get("DynamicURL")); 
					int ActualStatus= response.getStatusCode();
					String ActualCode=Integer.toString(ActualStatus);
					valid.statusCodeValidation(ActualCode,ExcelUtility.map.get("StatusCode"));
					valid.schemaValidation(response.asString(), JsonSchemaValidator.matchesJsonSchemaInClasspath("Schema/VODCatalog.json"));		
					}
			 
			 @Test(enabled=true)
				public void noFilterTypeKey_Validation() throws IOException {
				 GetRequestMethod GetReq =new GetRequestMethod(); 
					ApiKey APIKeyTAG =new ApiKey();
					KeyParameters KParameters =new KeyParameters();
					Validations valid =new Validations();
					APIKeyTAG.getEndpointValidation("VOD_Catalog");
					KParameters.TagNotPresent(ExcelUtility.map.get("Tag"));
					Response response=GetReq.dynamicgetRequest(KeyParameters.map.get("DynamicURL")); 
					int ActualStatus= response.getStatusCode();
					String ActualCode=Integer.toString(ActualStatus);
					valid.statusCodeValidation(ActualCode,ExcelUtility.map.get("StatusCode"));
					valid.schemaValidation(response.asString(), JsonSchemaValidator.matchesJsonSchemaInClasspath("Schema/VODCatalog.json"));		
					}
			 @Test(enabled=true)
				public void noFilterTypeInvalidValue_Validation() throws IOException {
				 ApiKey APIKeyTAG =new ApiKey();
					APIKeyTAG.getEndpointValidation("VOD_Catalog");
					APIKeyTAG.invalidAPIKeyValue(ExcelUtility.map.get("Tag"),Sheetname);
					Validations valid =new Validations();
					valid.getResponseValue(map.get("TestCase"),Sheetname);	
					}
			 @Test(enabled=true)
				public void noFilterTypeValue_Validation() throws IOException {
				 GetRequestMethod GetReq =new GetRequestMethod(); 
					ApiKey APIKeyTAG =new ApiKey();
					KeyParameters KParameters =new KeyParameters();
					Validations valid =new Validations();
					APIKeyTAG.getEndpointValidation("VOD_Catalog");
					KParameters.TagValueNoPresent(ExcelUtility.map.get("Tag"));
					Response response=GetReq.dynamicgetRequest(KeyParameters.map.get("DynamicURL")); 
					int ActualStatus= response.getStatusCode();
					String ActualCode=Integer.toString(ActualStatus);
					valid.statusCodeValidation(ActualCode,ExcelUtility.map.get("StatusCode"));
					valid.schemaValidation(response.asString(), JsonSchemaValidator.matchesJsonSchemaInClasspath("Schema/VODCatalog.json"));		
					}
			 @Test(enabled=true)
				public void noPagenoTag_Validation() throws IOException {
				 GetRequestMethod GetReq =new GetRequestMethod(); 
					ApiKey APIKeyTAG =new ApiKey();
					KeyParameters KParameters =new KeyParameters();
					Validations valid =new Validations();
					APIKeyTAG.getEndpointValidation("VOD_Catalog");
					KParameters.TagNotPresent(ExcelUtility.map.get("Tag"));
					Response response=GetReq.dynamicgetRequest(KeyParameters.map.get("DynamicURL")); 
					int ActualStatus= response.getStatusCode();
					String ActualCode=Integer.toString(ActualStatus);
					valid.statusCodeValidation(ActualCode,ExcelUtility.map.get("StatusCode"));
					valid.schemaValidation(response.asString(), JsonSchemaValidator.matchesJsonSchemaInClasspath("Schema/VODCatalog.json"));		
					}
			 @Test(enabled=true)
				public void noPagenoInvalidValue_Validation() throws IOException {
				 ApiKey APIKeyTAG =new ApiKey();
					APIKeyTAG.getEndpointValidation("VOD_Catalog");
					APIKeyTAG.invalidAPIKeyValue(ExcelUtility.map.get("Tag"),Sheetname);
					Validations valid =new Validations();
					valid.getResponseValue(map.get("TestCase"),Sheetname);	
					}
			 @Test(enabled=true)
				public void noPagenoTagValue_Validation() throws IOException {
				 GetRequestMethod GetReq =new GetRequestMethod(); 
					ApiKey APIKeyTAG =new ApiKey();
					KeyParameters KParameters =new KeyParameters();
					Validations valid =new Validations();
					APIKeyTAG.getEndpointValidation("VOD_Catalog");
					KParameters.TagValueNoPresent(ExcelUtility.map.get("Tag"));
					Response response=GetReq.dynamicgetRequest(KeyParameters.map.get("DynamicURL")); 
					int ActualStatus= response.getStatusCode();
					String ActualCode=Integer.toString(ActualStatus);
					valid.statusCodeValidation(ActualCode,ExcelUtility.map.get("StatusCode"));
					valid.schemaValidation(response.asString(), JsonSchemaValidator.matchesJsonSchemaInClasspath("Schema/VODCatalog.json"));		
					}
			 @Test(enabled=true)
				public void noRecordSizeTag_Validation() throws IOException {
				 GetRequestMethod GetReq =new GetRequestMethod(); 
					ApiKey APIKeyTAG =new ApiKey();
					KeyParameters KParameters =new KeyParameters();
					Validations valid =new Validations();
					APIKeyTAG.getEndpointValidation("VOD_Catalog");
					KParameters.LogoType_Tag(ExcelUtility.map.get("Tag"));
					Response response=GetReq.dynamicgetRequest(KeyParameters.map.get("DynamicURL")); 
					int ActualStatus= response.getStatusCode();
					String ActualCode=Integer.toString(ActualStatus);
					valid.statusCodeValidation(ActualCode,ExcelUtility.map.get("StatusCode"));
					valid.schemaValidation(response.asString(), JsonSchemaValidator.matchesJsonSchemaInClasspath("Schema/VODCatalog.json"));		
					}
			 @Test(enabled=true)
				public void noRecordSizeInvalidValue_Validation() throws IOException {
				 ApiKey APIKeyTAG =new ApiKey();
				 KeyParameters KParameters =new KeyParameters();
					APIKeyTAG.getEndpointValidation("VOD_Catalog");
					KParameters.LastTagInvalidValue(ExcelUtility.map.get("Tag"),Sheetname);
					Validations valid =new Validations();
					valid.getResponseValue(map.get("TestCase"),Sheetname);	
					}
			 @Test(enabled=true)
				public void noRecordSizeTagValue_Validation() throws IOException {
				 GetRequestMethod GetReq =new GetRequestMethod(); 
					ApiKey APIKeyTAG =new ApiKey();
					KeyParameters KParameters =new KeyParameters();
					Validations valid =new Validations();
					APIKeyTAG.getEndpointValidation("VOD_Catalog");
					KParameters.LogoType_Value(ExcelUtility.map.get("Tag"));
					Response response=GetReq.dynamicgetRequest(KeyParameters.map.get("DynamicURL")); 
					int ActualStatus= response.getStatusCode();
					String ActualCode=Integer.toString(ActualStatus);
					valid.statusCodeValidation(ActualCode,ExcelUtility.map.get("StatusCode"));
					valid.schemaValidation(response.asString(), JsonSchemaValidator.matchesJsonSchemaInClasspath("Schema/VODCatalog.json"));		
					}
			 @Test(enabled=true)
				public void noContextTag_Validation() throws IOException {
					ApiKey APIKeyTAG =new ApiKey();
					KeyParameters Kparameter =new KeyParameters();
					APIKeyTAG.getEndpointValidation("VOD_Catalog");
					Kparameter.ErrorTagNotPresent(ExcelUtility.map.get("Tag"),Sheetname);
					Validations valid =new Validations();
					valid.getResponseValue(map.get("TestCase"),Sheetname);	
				}
				@Test(enabled=true)
				public void noContextValue_Validation() throws IOException {
					ApiKey APIKeyTAG =new ApiKey();
					KeyParameters Kparameter =new KeyParameters();
					APIKeyTAG.getEndpointValidation("VOD_Catalog");
					Kparameter.ErrorTagValueNoPresent(ExcelUtility.map.get("Tag"),Sheetname);
					Validations valid =new Validations();
					valid.getResponseValue(map.get("TestCase"),Sheetname);	
				}
				 @Test(enabled=true)
					public void noheadendIdTag_Validation() throws IOException {
						ApiKey APIKeyTAG =new ApiKey();
						KeyParameters Kparameter =new KeyParameters();
						APIKeyTAG.getEndpointValidation("VOD_Catalog");
						Kparameter.ErrorTagNotPresent(ExcelUtility.map.get("Tag"),Sheetname);
						Validations valid =new Validations();
						valid.getResponseValue(map.get("TestCase"),Sheetname);	
					}
				 @Test(enabled=true)
					public void noheadendIdValue_Validation() throws IOException {
						ApiKey APIKeyTAG =new ApiKey();
						KeyParameters Kparameter =new KeyParameters();
						APIKeyTAG.getEndpointValidation("VOD_Catalog");
						Kparameter.ErrorTagValueNoPresent(ExcelUtility.map.get("Tag"),Sheetname);
						Validations valid =new Validations();
						valid.getResponseValue(map.get("TestCase"),Sheetname);	
					}
				 @Test(enabled=true)
					public void noheadendIdInvalidValue_Validation() throws IOException {
						ApiKey APIKeyTAG =new ApiKey();
						KeyParameters Kparameter =new KeyParameters();
						APIKeyTAG.getEndpointValidation("VOD_Catalog");
						Kparameter.ErrorInValidValue(ExcelUtility.map.get("Tag"),Sheetname);
						Validations valid =new Validations();
						valid.getResponseValue(map.get("TestCase"),Sheetname);	
					}
				 @Test(enabled=true)
					public void CatalogProgrammeList_Validation() throws IOException {
					 ResponseValidation jsonOperations = new ResponseValidation();
						if (!jsonOperations.PageSize_CatalogNumberValidation())
						{
							System.out.println("Channel Data is as Expected");
							ExtentTestManager.getTest().log(Status.PASS, "PROGRAM DETAILS ARE VALIDATED");  
						} else 
						{ 
							System.out.println("Channel Data Mismatch");
							Assert.fail("Channel Data Mismatch");
							ExtentTestManager.getTest().log(Status.FAIL, "PROGRAM DETAILS ARE MISMATCHED"); 
				 }
				 }
				 @Test(enabled=true)
					public void MetadataSchema_Validation() throws IOException {
						ApiKey APIKeyTAG =new ApiKey();
						KeyParameters PageNo =new KeyParameters();
						GetRequestMethod GetReq =new GetRequestMethod();
						Validations valid =new Validations();
						APIKeyTAG.getEndpointValidation("Catalog_Metadata");
						PageNo.MultipleValuesPresent(ExcelUtility.map.get("Tag"));
						Response response=GetReq.dynamicgetRequest(KeyParameters.map.get("DynamicURL"));
						int ActualStatus= response.getStatusCode();
						String ActualCode=Integer.toString(ActualStatus);
						valid.statusCodeValidation(ActualCode,ExcelUtility.map.get("StatusCode"));
						valid.schemaValidation(response.asString(), JsonSchemaValidator.matchesJsonSchemaInClasspath("Schema/Catalog_MetadataSchema.json"));		
					}
				 @Test(enabled=true)
					public void MetadataSchema_1_Validation() throws IOException {
						ApiKey APIKeyTAG =new ApiKey();
						KeyParameters PageNo =new KeyParameters();
						GetRequestMethod GetReq =new GetRequestMethod();
						Validations valid =new Validations();
						APIKeyTAG.getEndpointValidation("Catalog_Metadata");
						PageNo.MultipleValuesPresent(ExcelUtility.map.get("Tag"));
						Response response=GetReq.dynamicgetRequest(KeyParameters.map.get("DynamicURL"));
						int ActualStatus= response.getStatusCode();
						String ActualCode=Integer.toString(ActualStatus);
						valid.statusCodeValidation(ActualCode,ExcelUtility.map.get("StatusCode"));
						valid.schemaValidation(response.asString(), JsonSchemaValidator.matchesJsonSchemaInClasspath("Schema/Catalog_MetadataSchema_1.json"));		
					}
				 @Test(enabled=true)
				 public void MetadataSchema_2_Validation() throws IOException {
						ApiKey APIKeyTAG =new ApiKey();
						KeyParameters PageNo =new KeyParameters();
						GetRequestMethod GetReq =new GetRequestMethod();
						Validations valid =new Validations();
						APIKeyTAG.getEndpointValidation("Catalog_Metadata");
						PageNo.MultipleValuesPresent(ExcelUtility.map.get("Tag"));
						Response response=GetReq.dynamicgetRequest(KeyParameters.map.get("DynamicURL"));
						int ActualStatus= response.getStatusCode();
						String ActualCode=Integer.toString(ActualStatus);
						valid.statusCodeValidation(ActualCode,ExcelUtility.map.get("StatusCode"));
						valid.schemaValidation(response.asString(), JsonSchemaValidator.matchesJsonSchemaInClasspath("Schema/Catalog_MetadataSchema_2.json"));		
					}
				 
				 @Test(enabled=true)
				 public void CountOfRecords_Validation() throws IOException {
					 ApiKey APIKeyTAG =new ApiKey();
						KeyParameters PageNo =new KeyParameters();
						GetRequestMethod GetReq =new GetRequestMethod();
						Validations valid =new Validations();
						JSONArray expectedChannels;
						APIKeyTAG.getEndpointValidation("Catalog_User");
						PageNo.MultipleValuesPresent(ExcelUtility.map.get("Tag"));
						Response response=GetReq.dynamicgetRequest(KeyParameters.map.get("DynamicURL"));
						int ActualStatus= response.getStatusCode();
						String ActualCode=Integer.toString(ActualStatus);
						valid.statusCodeValidation(ActualCode,ExcelUtility.map.get("StatusCode"));
						ResponseValidation jsonOperations = new ResponseValidation(); 
						JSONObject channels =jsonOperations.getChannels(response.asString(),"programmeslist"); 
						VODQueries Queries =new VODQueries();	
						int recordcount;
						DatabaseConnection Connector= new DatabaseConnection();
						Queries.PublishedCount();
						String Query=VODQueries.map.get("Query");
						Query=Query.replace("%s",ExcelUtility.map.get("OperatorID"));
						System.out.println(Query);
						Connector.getResult(Query);	
						Connector.getTotalRecords();
						if(ExcelUtility.map.get("InvalidValue").equalsIgnoreCase("tvshow")) {
							recordcount=DatabaseConnection.map.get("TVShowCount");
						}
						else {
							recordcount=DatabaseConnection.map.get("MovieCount");	
						}
						System.out.println(recordcount);
						int pagerecordcount=0;
						JSONObject expectedChannel = (JSONObject) channels;
						for (String expectedkey : expectedChannel.keySet()) {
						if(expectedkey.contains("platforms")){
							expectedChannels = expectedChannel.getJSONArray(expectedkey);
							pagerecordcount=expectedChannels.getJSONObject(0).getInt("operatortotalrecordcount");
							System.out.println(pagerecordcount);
							if(pagerecordcount==recordcount) {
								  ExtentTestManager.getTest().log(Status.PASS,"Expected Count :"+DatabaseConnection.map.get("RowCount")+" Actual Count : "+pagerecordcount); 
								  //System.out.println(expectedChannels);
								  } else {
								  ExtentTestManager.getTest().log(Status.FAIL,"Expected Count :"+DatabaseConnection.map.get("RowCount")+" Actual Count : "+pagerecordcount);
								  Assert.fail(); 
								  } 
						}
						}
				 }
				 

}

package testcases;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;

import org.json.JSONException;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import commonFunctions.DatabaseConnection;
import commonFunctions.ExcelUtility;
import commonFunctions.ImportProcessValidation;


@Listeners(reports.TestListener.class)
public class ImportProcess {

	public static HashMap<String, String> map = new HashMap<String, String>();
	 String filePath = System.getProperty("user.dir")+"\\Data";
	 String DynamicURL;
	 String Sheetname ="IMPORT PROCESS";
	 		

		  @BeforeMethod 
		  public void beforeMethod(ITestResult result) throws IOException
		  { 
		  map.put("TestCase", result.getMethod().getMethodName()); 
		  ExcelUtility excelread=new ExcelUtility();
		  excelread.readExcel(filePath,"StandardAPI.xlsx",Sheetname,map.get("TestCase")); 
		  DatabaseConnection Connector= new DatabaseConnection();
		  Connector.DBConnection("OVD_DBName");
		  }

		  @AfterMethod
		  public void afterMethod(ITestResult result) throws IOException
		  {
			 DatabaseConnection Connector= new DatabaseConnection();
			 Connector.closeDBConnection();
		  }
		  @Test(enabled=true)
			public void TC_01() throws IOException, JSONException, SQLException {
			  ImportProcessValidation validation =new ImportProcessValidation();
			  validation.Import_Consistency();
		  }
		  @Test(enabled=true)
			public void TC_02() throws IOException, JSONException, SQLException {
			  ImportProcessValidation validation =new ImportProcessValidation();
			  validation.Import_Consistency();
		  }
		  @Test(enabled=true)
			public void TC_03() throws IOException, JSONException, SQLException {
			  ImportProcessValidation validation =new ImportProcessValidation();
			  validation.Import_Consistency();
		  }
		  @Test(enabled=true)
			public void TC_04() throws IOException, JSONException, SQLException {
			  ImportProcessValidation validation =new ImportProcessValidation();
			  validation.Import_Consistency();
		  }
		  @Test(enabled=true)
			public void TC_05() throws IOException, JSONException, SQLException {
			  ImportProcessValidation validation =new ImportProcessValidation();
			  validation.Import_Consistency();
		  }
		  @Test(enabled=true)
			public void TC_06() throws IOException, JSONException, SQLException {
			  ImportProcessValidation validation =new ImportProcessValidation();
			  validation.Import_Consistency();
		  }
		  @Test(enabled=true)
			public void TC_07() throws IOException, JSONException, SQLException {
			  ImportProcessValidation validation =new ImportProcessValidation();
			  validation.Import_Consistency();
		  }
		  @Test(enabled=true)
			public void TC_08() throws IOException, JSONException, SQLException {
			  ImportProcessValidation validation =new ImportProcessValidation();
			  validation.Import_Consistency();
		  }
		  @Test(enabled=true)
			public void TC_09() throws IOException, JSONException, SQLException {
			  ImportProcessValidation validation =new ImportProcessValidation();
			  validation.Import_Consistency();
		  }
		  @Test(enabled=true)
			public void TC_10() throws IOException, JSONException, SQLException {
			  ImportProcessValidation validation =new ImportProcessValidation();
			  validation.Import_Consistency();
		  }
		  @Test(enabled=true)
			public void TC_11() throws IOException, JSONException, SQLException {
			  ImportProcessValidation validation =new ImportProcessValidation();
			  validation.Import_Consistency();
		  }
		  @Test(enabled=true)
			public void TC_12() throws IOException, JSONException, SQLException {
			  ImportProcessValidation validation =new ImportProcessValidation();
			  validation.Import_Consistency();
		  }
		  @Test(enabled=true)
			public void TC_13() throws IOException, JSONException, SQLException {
			  ImportProcessValidation validation =new ImportProcessValidation();
			  validation.Import_Consistency();
		  }
		  @Test(enabled=true)
			public void TC_14() throws IOException, JSONException, SQLException {
			  ImportProcessValidation validation =new ImportProcessValidation();
			  validation.Import_Consistency();
		  }
		  
	
}

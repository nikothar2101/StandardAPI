package assertions;

import java.io.IOException;

import org.hamcrest.Matcher;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import commonFunctions.ApiKey;
import commonFunctions.ExcelUtility;
import reports.ExtentTestManager;



public class Validations {
	 String filePath = System.getProperty("user.dir")+"\\Data";
	 
	 /*Function Name-getResponseValue
	  Purpose- To validate Status Code, ID, Description from excel to response*/
	public void getResponseValue(String TestcaseName,String SheetName) throws IOException {
		statusCodeValidation(ExcelUtility.map.get("StatusCode"),ApiKey.map.get("ActualStatusCode")); 
		 if(ExcelUtility.map.get("ExpectedID")!=null) {
		  ExpectionID_Validation(ExcelUtility.map.get("ExpectedID"),ApiKey.map.get("ID"));
		  }
		 if(ExcelUtility.map.get("ExpectedDescription")!=null) { 
		 ExpectionDescription_Validation(ExcelUtility.map.get("ExpectedDescription"),ApiKey.map.get("Description"));
		 }
	}
	/*Function Name-getResponseValue
	  Purpose- To validate Status Code, ID, Description from excel to response*/
	public void comparePageSize() throws IOException {
		int pageSize= Integer.parseInt(ExcelUtility.map.get("InvalidValue")); 
		 if(pageSize >100) {
			 ExtentTestManager.getTest().log(Status.PASS, "PAGESIZE VALUE IS "+pageSize+" WHICH GREATER THAN 100");
		 }
		 else {
			 ExtentTestManager.getTest().log(Status.INFO, "PAGESIZE VALUE IS "+pageSize+" WHICH LESS THAN 100"); 
		 }
	}
	
	/*Function Name-schemaValidation
	  Purpose- To validate Schema*/
	public void schemaValidation(String responseBody, Matcher<?> matcher){
		
			if (!matcher.matches(responseBody)) {
				System.out.println(responseBody);
				System.out.println(matcher);
				ExtentTestManager.getTest().log(Status.FAIL, "SCHEMA VALIDATION FAILED");
				 Assert.fail();
			}
			else {
				ExtentTestManager.getTest().log(Status.PASS, "SCHEMA IS VALIDATED");
			}
		}
	/*Function Name-statusCodeValidation
	  Purpose- To validate status Code */
	public void statusCodeValidation(String ExpectedCode, String ActualCode) {
		if(ExpectedCode.equalsIgnoreCase(ActualCode))
		{		
		 ExtentTestManager.getTest().log(Status.PASS, "EXPECTED CODE: "+ExpectedCode+" ACTUAL CODE :"+ActualCode);
		 }
		else {
			ExtentTestManager.getTest().log(Status.FAIL, "EXPECTED CODE: "+ExpectedCode+" ACTUAL CODE :"+ActualCode);
		    Assert.fail();
		}
		
		
	}
	/*Function Name-message_Validation
	  Purpose- To validate Message when result is zero*/
	public void message_Validation(String ExpectedMessage, String ActualMessage) {
		if(ExpectedMessage.equalsIgnoreCase(ActualMessage))
		{		
		 ExtentTestManager.getTest().log(Status.PASS, "EXPECTED MESSAGE: "+ExpectedMessage+" ACTUAL MESSAGE :"+ActualMessage);
		 }
		else {
			ExtentTestManager.getTest().log(Status.FAIL, "EXPECTED MESSAGE: "+ExpectedMessage+" ACTUAL MESSAGE :"+ActualMessage);
		    Assert.fail();
		}
		
	}
	/*Function Name-response_Validation
	  Purpose- To validate Response value when result is zero*/
	public void response_Validation(String ExpectedResponse, String ActualResponse) {
		if(ExpectedResponse.equalsIgnoreCase(ActualResponse))
		{		
		 ExtentTestManager.getTest().log(Status.PASS, "EXPECTED RESPONSE: "+ExpectedResponse+" ACTUAL RESPONSE :"+ActualResponse);
		 }
		else {
			ExtentTestManager.getTest().log(Status.FAIL, "EXPECTED RESPONSE: "+ExpectedResponse+" ACTUAL RESPONSE :"+ActualResponse);
		    Assert.fail();
		}
		
		
	}
	/*Function Name-ExpectionID_Validation
	  Purpose- To validate Expection ID when some error is present in response*/
	public void ExpectionID_Validation(String ExpectedID, String ActualID) {
		if(ExpectedID.trim().equalsIgnoreCase(ActualID.trim()))
		{		
		 ExtentTestManager.getTest().log(Status.PASS, "EXPECTED EXPECTION ID: "+ExpectedID+" ACTUAL EXPECTION ID: "+ActualID);
		 }
		else {
			ExtentTestManager.getTest().log(Status.FAIL, "EXPECTED EXPECTION ID: "+ExpectedID+" ACTUAL EXPECTION ID: "+ActualID);
		    Assert.fail();
		}
		
	}
	/*Function Name-response_Validation
	  Purpose- To validate Description when some error is present in response*/
	public void ExpectionDescription_Validation(String ExpectedDescription, String ActualDescription) {
		if(ExpectedDescription.trim().equalsIgnoreCase(ActualDescription.trim()))
		{		
		 ExtentTestManager.getTest().log(Status.PASS, "EXPECTED DESCRIPTION: "+ExpectedDescription+" ACTUAL DESCRIPTION: "+ActualDescription);
		 }
		else {
			ExtentTestManager.getTest().log(Status.FAIL, "EXPECTED DESCRIPTION: "+ExpectedDescription+" ACTUAL DESCRIPTION: "+ActualDescription);
		    Assert.fail();
		}
		
	}
	
}



package response;

import java.util.HashMap;

import com.fasterxml.jackson.annotation.JsonProperty;


public class AdvancedSearchNoValueResponse {
	public static HashMap<String, String> map = new HashMap<String, String>();
	@JsonProperty("response")
	public Response response;
	
	
	public Response getResponse() {
		return response;
	}


	public void setResponse(Response response) {
		this.response = response;
	}


	public  class Response{
		
		@JsonProperty("responsestatus")
		String responsestatus;
		public String getResponsestatus() {
			map.put("responsestatus", responsestatus);
			System.out.println(responsestatus);
			return responsestatus;
		}
		public void setResponsestatus(String responsestatus) {
			this.responsestatus = responsestatus;
		}
		@JsonProperty("message")
		String message;
		
		public String getMessage() {
			map.put("message", message);
			System.out.println(responsestatus);
			return message;
		}
		public void setMessage(String message) {
			this.message = message;
		}
		
	}
}

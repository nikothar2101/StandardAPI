package response.Model;


public class Programme{
    public String getChannelname() {
		return channelname;
	}
	public void setChannelname(String channelname) {
		this.channelname = channelname;
	}
	public String getChannelid() {
		return channelid;
	}
	public void setChannelid(String channelid) {
		this.channelid = channelid;
	}
	public String getChannellogo() {
		return channellogo;
	}
	public void setChannellogo(String channellogo) {
		this.channellogo = channellogo;
	}
	public String getChanneldisplayname() {
		return channeldisplayname;
	}
	public void setChanneldisplayname(String channeldisplayname) {
		this.channeldisplayname = channeldisplayname;
	}
	public String getLogofileurl() {
		return logofileurl;
	}
	public void setLogofileurl(String logofileurl) {
		this.logofileurl = logofileurl;
	}
	public String getLcn() {
		return lcn;
	}
	public void setLcn(String lcn) {
		this.lcn = lcn;
	}
	public String getLanguagename() {
		return languagename;
	}
	public void setLanguagename(String languagename) {
		this.languagename = languagename;
	}
	public String getEndtime() {
		return endtime;
	}
	public void setEndtime(String endtime) {
		this.endtime = endtime;
	}
	public String getScheduleid() {
		return scheduleid;
	}
	public void setScheduleid(String scheduleid) {
		this.scheduleid = scheduleid;
	}
	public String getImagefilepath() {
		return imagefilepath;
	}
	public void setImagefilepath(String imagefilepath) {
		this.imagefilepath = imagefilepath;
	}
	public String getGenre() {
		return genre;
	}
	public void setGenre(String genre) {
		this.genre = genre;
	}
	public String getM2esubcategoryname() {
		return m2esubcategoryname;
	}
	public void setM2esubcategoryname(String m2esubcategoryname) {
		this.m2esubcategoryname = m2esubcategoryname;
	}
	public String getProgramid() {
		return programid;
	}
	public void setProgramid(String programid) {
		this.programid = programid;
	}
	public String getProgrammename() {
		return programmename;
	}
	public void setProgrammename(String programmename) {
		this.programmename = programmename;
	}
	public String getStarttime() {
		return starttime;
	}
	public void setStarttime(String starttime) {
		this.starttime = starttime;
	}
	public String getTimestring() {
		return timestring;
	}
	public void setTimestring(String timestring) {
		this.timestring = timestring;
	}
	public String getSearchfallback() {
		return searchfallback;
	}
	public void setSearchfallback(String searchfallback) {
		this.searchfallback = searchfallback;
	}
	public String channelname;
    public String channelid;
    public String channellogo;
    public String channeldisplayname;
    public String logofileurl;
    public String lcn;
    public String languagename;
    public String endtime;
    public String scheduleid;
    public String imagefilepath;
    public String genre;
    public String m2esubcategoryname;
    public String programid;
    public String programmename;
    public String starttime;
    public String timestring;
    public String searchfallback;
}







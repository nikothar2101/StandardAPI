package response;

import java.util.HashMap;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonProperty;

import response.ErrorResponse.Exception;
@XmlAccessorType (XmlAccessType.FIELD)
@XmlRootElement(name="Exception")
public class ErrorResponseXML {
	
	public static HashMap<String, String> map = new HashMap<String, String>();
	/*
	 * @XmlElement(name="Exception") public Exception Exception;
	 * 
	 * public Exception getException() { return Exception; }
	 * 
	 * public void setException(Exception exception) { Exception = exception; }
	 */
	
	@XmlElement(name="ID")
	String ID;
	@XmlElement(name="Description")
	String Description;
	public String getID() {
		
		return ID;
	}
	public String getDescription() {
		return Description;
	}
	
	/*public static class Exception{
		
		@XmlElement(name="ID")
		String ID;
		@XmlElement(name="Description")
		String Description;
		public String getID() {
			
			return ID;
		}
		public String getDescription() {
			return Description;
		}*/
		
	//}

}

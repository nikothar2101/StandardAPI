package response;

import java.util.HashMap;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ErrorResponse {
	public static HashMap<String, String> map = new HashMap<String, String>();
	@JsonProperty("Exception")
	public Exception Exception;
	
	public Exception getException() {
		return Exception;
	}

	public void setException(Exception exception) {
		Exception = exception;
	}

	public  class Exception{
		
		@JsonProperty("ID")
		String ID;
		@JsonProperty("Description")
		String Description;
		public String getID() {
			map.put("ID", ID);
			return ID;
		}
		public void setID(String iD) {
			ID = iD;
		}
		public String getDescription() {
			map.put("Description", Description);
			return Description;
		}
		public void setDescription(String description) {
			Description = description;
		}
		
	}
	
	
}

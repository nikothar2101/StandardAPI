package response;
import java.util.HashMap;

import com.fasterxml.jackson.annotation.JsonProperty;


public class ErrorCelebrityResponse {

		public static HashMap<String, String> map = new HashMap<String, String>();
		@JsonProperty("exceptiondetails")
		public Exception Exceptiondetails;
		
		public Exception getExceptiondetails() {
			return Exceptiondetails;
		}
		public void setExceptiondetails(Exception exceptiondetails) {
			Exceptiondetails = exceptiondetails;
		}
		public  class Exception{
		
			@JsonProperty("exceptionmessage")
			String Exceptionmessage;
			public String getExceptionmessage() {
				map.put("ExpectedMessage", Exceptionmessage);
				return Exceptionmessage;
			}
			public void setExceptionmessage(String exceptionmessage) {
				Exceptionmessage = exceptionmessage;
			}
			@JsonProperty("description")
			String Description;
			public String getDescription() {
				map.put("Description", Description);
				return Description;
			}
			public void setDescription(String description) {
				Description = description;
			}
			
		}
		
		




}

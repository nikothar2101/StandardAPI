package commonFunctions;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Properties;

import org.json.JSONObject;

import com.aventstack.extentreports.Status;

import assertions.Validations;
import io.restassured.matcher.RestAssuredMatchers;
import io.restassured.module.jsv.JsonSchemaValidator;
import io.restassured.response.Response;
import reports.ExtentTestManager;
import response.AdvancedSearchNoValueResponse;
import response.ErrorCelebrityResponse;
import response.ErrorResponse;
import response.ErrorResponseXML;
import steps.GetRequestMethod;

public class ApiKey {
	public static HashMap<String, String> map = new HashMap<String, String>();
	String URL;
	String DynamicURL;
	InputStream inputStream;
	Properties prop = new Properties();
	String propFileName = "config.properties";
	/*
	  Function Name-getEndpointValidation 
	  Purpose- For getting endpoint from config.properties file and based on testcase it will pick the endpoint
	 */
	public void getEndpointValidation(String EndpointAPI) throws IOException {
		readPropertiesFile("config.properties");
		inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
		if (inputStream != null) {
			try {
				prop.load(inputStream);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
		}
		// Properties prop = 
		if(EndpointAPI.equals("AdvanceSearch")) {
			URL = prop.getProperty("AdvanceSearchAPI");
		}
		else if(EndpointAPI.equals("AdvanceSearchAPINoResponseLanguage")) {
			
			URL = prop.getProperty("AdvanceSearchAPINoResponseLanguage");
		}
		else if(EndpointAPI.equals("CelebrityAPI")) {
			URL = prop.getProperty("CelebrityAPI");
		}
		else if(EndpointAPI.equals("VOD_Catalog")) {
			URL = prop.getProperty("VOD_Catalog");
		}
		else if(EndpointAPI.equals("Catalog_User")) {
			URL = prop.getProperty("Catalog_User");
		}
		else if(EndpointAPI.equals("Catalog_Metadata")) {
			URL = prop.getProperty("Catalog_Metadata");
		}
		else if(EndpointAPI.equals("OVDStreaming")) {
			URL = prop.getProperty("OVDStreaming");
		}
		else if(EndpointAPI.equals("ProgrammeDetails")) {
			URL = prop.getProperty("ProgrammeDetails");
		}
		else if(EndpointAPI.equals("P6ProgrammeDetails")) {
			URL = prop.getProperty("P6ProgrammeDetails");
		}
		else if(EndpointAPI.equals("HeadEndID_OVDStreaming")) {
			URL = prop.getProperty("HeadEndID_OVDStreaming");
		}
		else if(EndpointAPI.isEmpty()) {
			ExtentTestManager.getTest().log(Status.FAIL, "Endpoint API IS BLANK/EMPTY");
		}
		else {
			ExtentTestManager.getTest().log(Status.FAIL, "Endpoint API IS NOT PRESENT IN PROPERTIES FILE");
		}
		 map.put("EndpointURL", URL);
		 ExtentTestManager.getTest().log(Status.PASS, "ENDPOINT URL FOR "+EndpointAPI+" IS:"+URL);
		System.out.println("API is  : "+EndpointAPI+" Endpoint is "+URL);
	}
	public String getEndpointValidation1(String EndpointAPI) throws IOException {
		readPropertiesFile("config.properties");
		inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
		if (inputStream != null) {
			try {
				prop.load(inputStream);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
		}
		// Properties prop = 
		if(EndpointAPI.equals("AdvanceSearch")) {
			URL = prop.getProperty("AdvanceSearchAPI");
		}
		else if(EndpointAPI.equals("AdvanceSearchAPINoResponseLanguage")) {
			
			URL = prop.getProperty("AdvanceSearchAPINoResponseLanguage");
		}
		else if(EndpointAPI.equals("CelebrityAPI")) {
			URL = prop.getProperty("CelebrityAPI");
		}
		else if(EndpointAPI.equals("VOD_Catalog")) {
			URL = prop.getProperty("VOD_Catalog");
		}
		else if(EndpointAPI.equals("Catalog_User")) {
			URL = prop.getProperty("Catalog_User");
		}
		else if(EndpointAPI.equals("Catalog_Metadata")) {
			URL = prop.getProperty("Catalog_Metadata");
		}
		else if(EndpointAPI.equals("OVDStreaming")) {
			URL = prop.getProperty("OVDStreaming");
		}
		else if(EndpointAPI.equals("ProgrammeDetails")) {
			URL = prop.getProperty("ProgrammeDetails");
		}
		else if(EndpointAPI.equals("P6ProgrammeDetails")) {
			URL = prop.getProperty("P6ProgrammeDetails");
		}
		else if(EndpointAPI.equals("NextSchedule")) {
			URL = prop.getProperty("NextSchedule");
		}
		else if(EndpointAPI.equals("HeadEndID_OVDStreaming")) {
			URL = prop.getProperty("HeadEndID_OVDStreaming");
		}
		
		else if(EndpointAPI.isEmpty()) {
			ExtentTestManager.getTest().log(Status.FAIL, "Endpoint API IS BLANK/EMPTY");
		}
		else {
			ExtentTestManager.getTest().log(Status.FAIL, "Endpoint API IS NOT PRESENT IN PROPERTIES FILE");
		}
		 //map.put("EndpointURL", URL);
		 ExtentTestManager.getTest().log(Status.PASS, "ENDPOINT URL FOR "+EndpointAPI+" IS:"+URL);
		System.out.println("API is  : "+EndpointAPI+" Endpoint is "+URL);
		return URL;
	}
	private void readPropertiesFile(String string) {
		// TODO Auto-generated method stub
		
	}
	/*
	  Function Name-getEndpointValidation 
	  Purpose- Remove Tag from Endpoint
	 */
	public void noAPIKeyTag(String TagName, String Sheetname) throws IOException {
		
		 String[] NoTag = URL.split(TagName+"=");
		 String[] NoTag1= NoTag[1].split("&",2);
		 DynamicURL=NoTag[0]+NoTag1[1];
		 if(Sheetname.equalsIgnoreCase("AdvanceSearch") ) {
		 toFetchResponseAttributes();
		 }
		 else {
			 toFetchResponseAttributesCelebrityAPI();
		 }
		  
	} 
	/*
	  Function Name-getEndpointValidation 
	  Purpose- fetch the parameters/attributes from response and store when response has error
	 */
	public void toFetchResponseAttributes() throws IOException{
		GetRequestMethod GetReq =new GetRequestMethod();
		Validations valid =new Validations();
		KeyParameters KeyParameterURL =new KeyParameters();
		if(DynamicURL==null) {
		DynamicURL=KeyParameterURL.map.get("DynamicURL");
		}
		ExtentTestManager.getTest().log(Status.INFO, "DYNAMINC URL IS : "+DynamicURL);
		 Response response=GetReq.DynamicgetRequestError(DynamicURL);
		 System.out.println(response.asString());
		// System.out.println(GetReq.dynamicgetRequest(DynamicURL));
		 JSONObject obj =new JSONObject(response.asString());
		 int ActualStatus= response.getStatusCode();
		 String ActualCode=Integer.toString(ActualStatus);
		 map.put("ActualStatusCode", ActualCode);
		 System.out.println("Actual Status Code:"+ActualCode);
		valid.schemaValidation(response.asString(), JsonSchemaValidator.matchesJsonSchemaInClasspath("Schema/ErrorResponseSchema.json"));		
		//((JSONObject) obj.get("Exception")).getString("Description");
		//System.out.println(((JSONObject) obj.get("Exception")).getString("ID"));
		map.put("ID",((JSONObject) obj.get("Exception")).getString("ID")); 
		 
		 map.put("Description",((JSONObject) obj.get("Exception")).getString("Description"));  
	}
	/*
	  Function Name-getEndpointValidation 
	  Purpose- fetch the parameters/attributes from response and store when response has error for Celebrity API
	 */
	public void toFetchResponseAttributesCelebrityAPI() throws IOException{
		GetRequestMethod GetReq =new GetRequestMethod();
		Validations valid =new Validations();
		KeyParameters KeyParameterURL =new KeyParameters();
		if(DynamicURL==null) {
		DynamicURL=KeyParameterURL.map.get("DynamicURL");
		}
		
		ExtentTestManager.getTest().log(Status.INFO, "DYNAMINC URL IS : "+DynamicURL);
		 Response response=GetReq.dynamicgetRequest(DynamicURL);
		 System.out.println(GetReq.dynamicgetRequest(DynamicURL));
		 ErrorCelebrityResponse errorResponse=response.as(ErrorCelebrityResponse.class);
		 int ActualStatus= response.getStatusCode();
		 String ActualCode=Integer.toString(ActualStatus);
		 map.put("ActualStatusCode", ActualCode);
		 System.out.println("Actual Status Code:"+ActualCode);
		valid.schemaValidation(response.asString(), JsonSchemaValidator.matchesJsonSchemaInClasspath("Schema/ErrorCelebrityAPI.json"));		
		 map.put("ID",errorResponse.getExceptiondetails().getExceptionmessage());
		 map.put("Description",errorResponse.getExceptiondetails().getDescription());
	}
	/*
	  Function Name-toFetchResponseAttributesXML 
	  Purpose- fetch the parameters/attributes from response in XML and validate ID and Description
	 */
		public void toFetchResponseAttributesXML() throws IOException{
			GetRequestMethod GetReq =new GetRequestMethod();
			Validations valid =new Validations();
			KeyParameters KeyParameterURL =new KeyParameters();
			if(DynamicURL==null) {
			DynamicURL=KeyParameterURL.map.get("DynamicURL");
			}
			 
			ExtentTestManager.getTest().log(Status.INFO, "DYNAMINC URL IS : "+DynamicURL);
			 Response response=GetReq.dynamicgetRequest(DynamicURL);
			 System.out.println(GetReq.dynamicgetRequest(DynamicURL));
			 JSONObject obj =new JSONObject(response.asString());
			// ErrorResponseXML errorResponse=response.as(ErrorResponseXML.class);
			 int ActualStatus= response.getStatusCode();
			 String ActualCode=Integer.toString(ActualStatus);
			 map.put("ActualStatusCode", ActualCode);
			 valid.schemaValidation(response.asString(), RestAssuredMatchers.matchesXsdInClasspath("Schema/ErrorResponseXML.xsd"));
			 map.put("ID",((JSONObject) obj.get("Exception")).getString("ID")); 
			 map.put("Description",((JSONObject) obj.get("Exception")).getString("Description")); 
			// map.put("ID",errorResponse.getID()); 
			 //System.out.println(errorResponse.getDescription());
			 //map.put("Description",errorResponse.getDescription()); 
		}
		/*
		  Function Name-toFetchResponseAttributesFromNoValue 
		  Purpose- fetch the parameters/attributes from response and store
		 */
		public void toFetchResponseAttributesFromNoValue() throws IOException{
			GetRequestMethod GetReq =new GetRequestMethod();
			Validations valid =new Validations();
			KeyParameters KeyParameterURL =new KeyParameters();
			if(DynamicURL==null) {
			DynamicURL=KeyParameterURL.map.get("DynamicURL");
			}
			System.out.println(DynamicURL);
			ExtentTestManager.getTest().log(Status.INFO, "DYNAMINC URL IS : "+DynamicURL);
			 Response response=GetReq.dynamicgetRequest(DynamicURL);
			// System.out.println(GetReq.dynamicgetRequest(DynamicURL));
			 JSONObject obj =new JSONObject(response.asString());
			// AdvancedSearchNoValueResponse NoValueResponse=response.as(AdvancedSearchNoValueResponse.class);
			 int ActualStatus= response.getStatusCode();
			 String ActualCode=Integer.toString(ActualStatus);
			 map.put("ActualStatusCode", ActualCode);
			valid.schemaValidation(response.asString(), JsonSchemaValidator.matchesJsonSchemaInClasspath("Schema/AdvanceSearchNoResultSchema.json"));		
			map.put("ActualMessage",((JSONObject) obj.get("response")).getString("message"));
			map.put("ActualResponseStatus",((JSONObject) obj.get("response")).getString("responsestatus"));
		}
		/*
		  Function Name-noAPIKeyValue 
		  Purpose- remove APIkey value and make it blank
		 */
	public void noAPIKeyValue(String TagName,String Sheetname) throws IOException {
		
		String[] NoTag = URL.split(TagName+"=");
		 String[] NoTag1= NoTag[1].split("&",2);
		 DynamicURL=NoTag[0]+TagName+"=&"+NoTag1[1];
		 if(Sheetname.equalsIgnoreCase("AdvanceSearch")) {
		 toFetchResponseAttributes();
		 }
		 else {
			 toFetchResponseAttributesCelebrityAPI();
		 }
		 }
	/*
	  Function Name-invalidAPIKeyValue 
	  Purpose- Remove valid APIkey value and enter invalid value
	 */
		public void invalidAPIKeyValue(String TagName,String SheetName) throws IOException {
		String[] NoTag = URL.split(TagName+"=");
		String[] NoTag1= NoTag[1].split("&",2);
		DynamicURL=NoTag[0]+TagName+"="+ExcelUtility.map.get("InvalidValue")+"&"+NoTag1[1];
		if(SheetName.equalsIgnoreCase("AdvanceSearch")) {
		toFetchResponseAttributes();
		}
		else {
			toFetchResponseAttributesCelebrityAPI();
		}
	}
		/*
		  Function Name-validAPIKeyValueOfDifferentProduct 
		  Purpose- remove valid APIkey value and enter valid value of different product autoSearch value
		 */

			public void validAPIKeyValueOfDifferentProduct(String TagName,String SheetName) throws IOException {
		 	 String[] NoTag = URL.split(TagName+"=");
			 String[] NoTag1= NoTag[1].split("&",2);
			 DynamicURL=NoTag[0]+TagName+"="+ExcelUtility.map.get("InvalidValue")+"&"+NoTag1[1];
			 if(SheetName.equalsIgnoreCase("AdvanceSearch")) {
					toFetchResponseAttributes();
					}
					else {
						toFetchResponseAttributesCelebrityAPI();
					}
	}
	}



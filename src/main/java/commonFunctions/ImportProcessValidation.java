package commonFunctions;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.HashMap;
import java.util.Random;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import reports.ExtentTestManager;

public class ImportProcessValidation {
	public static Logger log = LogManager.getLogger("file");
	public static HashMap<String, Integer> map = new HashMap<String, Integer>();
	String Query="";
	public static Connection connection;
	Statement statement;
	public static ResultSet resultSet;
	
	public String IMPORTPROCESS() 
	{
		Query="Select DISTINCT top 100 VODSRID "
		+ "from BATMAN.MEdiae2e_UAT.dbo.VCMS_VODCatalog "
		+ "where operatorID=%s and statusid=2 ";	
		System.out.println(Query);
		return Query;
	}
	
	public String VODPROGRAMME_IMPORT() 
	{
		Query="Select * from BATMAN.MEdiae2e_UAT.dbo.vodprogramme where VODSRID=%s";
		System.out.println(Query);
		return Query;
	}
	public String IMPORTPROCESS_VALIDATION() 
	{
		Query="select vc.VODSRID,  a.AttributeName,atr.* from BATMAN.MEdiae2e_UAT.dbo.VCMS_VODCatalog vc "
		+ "inner join BATMAN.MEdiae2e_UAT.dbo.VCMS_VODAttributeMapping atr on atr.VODSRID=vc.VODSRID and atr.statusid=2 "
		+ "inner join BATMAN.MEdiae2e_UAT.dbo.Attribute a on a.AttributeID =atr.AttributeID "
		+ "where vc.statusid=2  and vc.operatorID=%s and vc.VODSRID=%d ";	
		System.out.println(Query);
		return Query;
	}
	
	public void Import_Consistency() throws IOException, JSONException, SQLException 
	{
		DatabaseConnection Connector= new DatabaseConnection();
		Random random = new Random();   
		IMPORTPROCESS();
		Query=Query.replace("%s",ExcelUtility.map.get("OperatorID"));
		System.out.println(Query);
		Connector.getResult(Query);
		JSONObject db=Connector.IMPORTVALUE();
		JSONObject actualChannel = (JSONObject) db;
		JSONArray actualChannels = actualChannel.getJSONArray("IMPORT");
		for (int j = 0; j <= 5;j++) {
			int ran = random.nextInt(100);
			System.out.println(ran);
			String VODSRID=actualChannels.getJSONObject(ran).getString("VODSRID");
			System.out.println(VODSRID);
			
			ExtentTestManager.getTest().log(Status.INFO, "VALIDATING FOR VODSRID: "+VODSRID);
			
			//For getting value from VOD programme
			VODPROGRAMME_IMPORT();
			Query=Query.replace("%s",VODSRID);
			System.out.println(Query);
			Connector.getResult(Query);
			JSONObject VODPROGRAMME_DB_IMPORT=Connector.VODPROGRAMME_IMPORT_db();
			JSONObject actualChannel_VP_DB_IMPORT = (JSONObject) VODPROGRAMME_DB_IMPORT;
			JSONArray actualChannels_VP_DB_IMPORT = actualChannel_VP_DB_IMPORT.getJSONArray("VODPROGRAMME_IMPORT_db");	
			
			//For getting value from Attribute Table
			IMPORTPROCESS_VALIDATION();
			Query=Query.replace("%d",VODSRID);
			Query=Query.replace("%s",ExcelUtility.map.get("OperatorID"));
			System.out.println(Query);
			Connector.getResult(Query);
			JSONObject ATTRIBUTE_DATA=Connector.IMPORTPROCESS_VALIDATION_db();
			JSONObject actualChannel_ATTRIBUTE_DATA = (JSONObject) ATTRIBUTE_DATA;
			JSONArray actualChannels_ATTRIBUTE_DATA = actualChannel_ATTRIBUTE_DATA.getJSONArray("IMPORTPROCESS_VALIDATION_DB");	
			int VDP_Length=actualChannels_VP_DB_IMPORT.length();
			//int Attribute_Length=actualChannels_ATTRIBUTE_DATA.length();
			int Temp_Attribute_Length=actualChannels_ATTRIBUTE_DATA.length();
			//System.out.println(VDP_Length);
			//System.out.println(Attribute_Length);
			String Attribute=ExcelUtility.map.get("Attributes");
			for(int i=0;i<VDP_Length;i++) 
			{
				int count=0;
				for(int k=0;k<Temp_Attribute_Length;k++) 
				{
					
					//String VDP_DATA=actualChannels_VP_DB_IMPORT.getJSONObject(i).getString("ProgrammeName");
					//System.out.println(VDP_DATA);
					//String Attribute_DATA=actualChannels_ATTRIBUTE_DATA.getJSONObject(k).getString("AttributeValue");
					String AttributeID_DATA=actualChannels_ATTRIBUTE_DATA.getJSONObject(k).getString("AttributeID");
					//System.out.println(Attribute_DATA);
					//System.out.println(AttributeID_DATA);
					
					if(Attribute.contains(",")){
						char abc=Attribute.charAt(0);
						System.out.println(abc);
						if(abc==',') {
							Attribute=Attribute.substring(1);	
						}
						String[]TempValues=Attribute.split(",");
						for(String S:TempValues) {
							String[] Tag = S.split("=");
							String AttributeName=Tag[0];
							String AttributeID=Tag[1];
							//System.out.println(AttributeName);
							//System.out.println(AttributeID);
							if(AttributeID_DATA.equalsIgnoreCase(AttributeID)) {
								try {
									//boolean temp=actualChannels_VP_DB_IMPORT.getJSONObject(i).isNull(Attribute);
									//System.out.println(temp);
									if(actualChannels_VP_DB_IMPORT.getJSONObject(i).isNull(Attribute)==false) {
										if(actualChannels_ATTRIBUTE_DATA.getJSONObject(k).getString("AttributeValue")==null) {
											ExtentTestManager.getTest().log(Status.PASS, "ATTRIBUTE "+AttributeName+" IS MATCHED ");	
										}
										else {
											ExtentTestManager.getTest().log(Status.FAIL, "ATTRIBUTE "+AttributeName+" IS NOT MATCHED ");
											ExtentTestManager.getTest().log(Status.FAIL, "VALUE IN VODPROGRAMME TABLE IS NULL"
											+" VALUE IN ATTRIBUTE TABLE: "+actualChannels_ATTRIBUTE_DATA.getJSONObject(k).getString("AttributeValue"));
										}
									}
									
									else {
								if (actualChannels_VP_DB_IMPORT.getJSONObject(i).getString(AttributeName).equals(actualChannels_ATTRIBUTE_DATA.getJSONObject(k).getString("AttributeValue"))) 
								{
									ExtentTestManager.getTest().log(Status.PASS, "ATTRIBUTE "+AttributeName+" IS MATCHED ");
									actualChannels_ATTRIBUTE_DATA.remove(k);
									Temp_Attribute_Length=actualChannels_ATTRIBUTE_DATA.length();
									k=0;count++;
									if(AttributeName.equalsIgnoreCase("ProgrammeName")) {
										Attribute=Attribute.replace(AttributeName+"=", "");
										
									}
									else {
									Attribute=Attribute.replace(","+AttributeName+"=", "");
									}
									Attribute=Attribute.replace(AttributeID, "");
									
									System.out.println(Attribute);
									break;
								}
									
								else {
									ExtentTestManager.getTest().log(Status.FAIL, "ATTRIBUTE "+AttributeName+" IS NOT MATCHED ");
									ExtentTestManager.getTest().log(Status.FAIL, "VALUE IN VODPROGRAMME TABLE: "
									+actualChannels_VP_DB_IMPORT.getJSONObject(i).getString(AttributeName)+" VALUE IN ATTRIBUTE TABLE: "
									+actualChannels_ATTRIBUTE_DATA.getJSONObject(k).getString("AttributeValue"));
									actualChannels_ATTRIBUTE_DATA.remove(k);
									Temp_Attribute_Length=actualChannels_ATTRIBUTE_DATA.length();
									k=0;
									Attribute=Attribute.replace(","+AttributeName+"=", "");
									Attribute=Attribute.replace(AttributeID, "");
									count++;
									break;
								}
								
							}
								}
							catch(Exception e) {
								System.out.print("Value is null");
							}
							}
							
							
						}
						
					}
					
				if(count==actualChannels_ATTRIBUTE_DATA.length()) {
					ExtentTestManager.getTest().log(Status.INFO, "TOTAL RECORDS "+count+" IN ATTRIBUTE TABLE IS VALIDATED FOR VODSRID: "+VODSRID);
					break;
				}
				
				
					
				}
				if(Temp_Attribute_Length>0) {
					
					ExtentTestManager.getTest().log(Status.PASS, "ATRRIBUTE(S) NOT VALIDATED: "+actualChannels_ATTRIBUTE_DATA);
					
				}
				
			}
			
			
		}
	}
	
	
	
	
	
}

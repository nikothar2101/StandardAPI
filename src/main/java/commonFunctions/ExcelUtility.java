package commonFunctions;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.aventstack.extentreports.Status;

import io.restassured.response.Response;
import reports.ExtentTestManager;
import steps.GetRequestMethod;

public class ExcelUtility {
	public static HashMap<String, String> map = new HashMap<String, String>();
	//public static HashMap<String, Set> att = new HashMap<String, Set>();
	 XSSFWorkbook excelWorkbook;
	

	    public String getData(LinkedHashMap<String, String> data, String key) {

	        //System.out.println("getData");

	        if (data.get(key) != null && data.get(key).length() > 0) {

	            return data.get(key);



	        } else {
	            System.out.println("else");
	            return "";
	        }
	    }
		/*Function Name-readExcel
		  Purpose- To read data from excel in string format with respective to Test cases.*/
		   
	    public void readExcel(String filePath,String fileName,String sheetName,String Testname) throws IOException{
	    	File file =    new File(filePath+"\\"+fileName);
	        FileInputStream inputStream = new FileInputStream(file);
	        Workbook WorkBook = null;
	        String fileExtensionName = fileName.substring(fileName.indexOf("."));
	        if(fileExtensionName.equals(".xlsx")){
	        WorkBook = new XSSFWorkbook(inputStream);
	        }
	        else if(fileExtensionName.equals(".xls")){
	            WorkBook = new HSSFWorkbook(inputStream);
	        }
	       
	        Sheet SHeet = WorkBook.getSheet(sheetName);
	      
	        int rowCount = SHeet.getLastRowNum()-SHeet.getFirstRowNum();
	        Set<String> hash_Set = new HashSet<String>();
	        for (int i = 0; i < rowCount+1; i++) {
	            Row row = SHeet.getRow(i);
	            int match = 0;
	            for (int j = 0; j < row.getLastCellNum(); j++) {
	                String Testcasename=row.getCell(j).getStringCellValue().toString();
	                if(Testcasename.equalsIgnoreCase(Testname)) {
	                	for(j = 1; j <= row.getLastCellNum(); j++) {
	                		try {
	                		String Values=row.getCell(j).getStringCellValue().toString();
	                		if(!Values.isEmpty()){
	                			/*if(Values.contains(",")) {
	                				System.out.println(Values);
	                				 map.put("Attributes", Values);
	                			}
	                				*//*String[]TempValues=Values.split(",");
	                				for(String S:TempValues) {
	                					String[] Tag = S.split("_");
	                					Map<String, List<String>> hm = new HashMap<String, List<String>>();
	                					List<String> values = new ArrayList<String>();
	                					values.add(Tag[1].toLowerCase());
	                					//values.add(Tag[1].toLowerCase());
	                					hm.put(Tag[0], values);
	                					System.out.println(hm.get(Tag[0]));
	   		                		 System.out.println(Tag[0]+": "+Tag[1]);
	   		                		 match++;
	                				}*/
	                			
	                		//	else {
	                			
	                			 String[] Tag = Values.split("_");
		                		 map.put(Tag[0], Tag[1]);
		                		 System.out.println(Tag[0]+": "+Tag[1]);
		                		 match++;
	                			//}
	                		}
	                		}
	                		//}
	                		catch(Exception e) {
	                			// System.out.println("Value in excel is empty/Blank");
	                		}
	                	}
	                	if(match >0) {
	                		//System.out.println("Found");
	                		i = rowCount+1;
	                		break;
	                		
	                	}
	                }
							
	                else {
	                	break;
	                }
	            }
	            //System.out.println();
	        } 
	    }
		/*Function Name-readAPI
		  Purpose- To read all API URL from excel in string format and perform JSON COMPARE.*/
	    public void readAPI()   
	    {  
	    	GetRequestMethod GetReq =new GetRequestMethod(); 
	    	Response response = null;
	    	Response response1;
	    try  
	    {  
	    File file = new File(System.getProperty("user.dir")+"\\Data"+"\\StandardAPI.xlsx");   //creating a new file instance  
	    FileInputStream fis = new FileInputStream(file);   //obtaining bytes from the file  
	    //creating Workbook instance that refers to .xlsx file  
	    XSSFWorkbook wb = new XSSFWorkbook(fis);
	    XSSFSheet sheet = wb.getSheet("JSONCOMPARE");  
	    int rowCount = sheet.getLastRowNum()-sheet.getFirstRowNum();
	     System.out.println(rowCount);
	     for (int i = 1; i < rowCount+1; i++) {
	    	 Row row = sheet.getRow(i);
	    	 for (int j = 0; j < row.getLastCellNum(); j++) {
	    		 String Testcasename=row.getCell(j).getStringCellValue().toString();
	    		 ExtentTestManager.getTest().log(Status.INFO, "VALIDATING JSON FOR TESTCASE :"+Testcasename);
	    		 for(j = 1; j < row.getLastCellNum(); j++) {
             		String Values=row.getCell(j).getStringCellValue().toString();
             		System.out.println(Values);
             		if(j==1) {
             		response=GetReq.dynamicgetRequest(Values);
             		}
             		else if(j==2){
             			response1=GetReq.dynamicgetRequest(Values);
                 		ResponseValidation.compareJSONDynamic(response.asString(),response1.asString());
             		}
             		
	    		 }
	    	 }
	     }
	     wb.close();
	    }
	    catch(Exception e)  
	    {  
	    e.printStackTrace();  
	    }
	    
}}






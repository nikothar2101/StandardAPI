package commonFunctions;

import java.util.HashMap;

public class VODQueries {
	
public static HashMap<String, String> map = new HashMap<String, String>();
	
	/*
	  Function Name-Program Details
	  Purpose- Purpose- Query For getting Program List/Details for Catalog API.
	 */

public String Catalogprogramlist() {
	String Query="select ProgrammeId,CMSProgrammeID,ProgrammeName,ProgrammeLanguage, "
			+ "ProgrammeGenre,Synopsis,Cast,ProductionYear,StartDate, EndDate, SourceEpisodeid, SourceSeasonid "
			+ "from vodprogramme where ApprovedStatus in (%s) and "
			+ "CatalogStatusID=2 and OperatorId= %f and ContentType in ('%a') "
			+ "order by LastUpdatedOn desc ";
	System.out.println(Query);
	map.put("Query",Query);
	return Query;
}
public String MappedPublishCount() {
	String Query="select count(1),operatorid,ContentType from vodprogramme where CatalogStatusID=2  and ContentType in ('movie','program') and OperatorId=%s"
			+ "and ApprovedStatus in (1,2,5,6) group by operatorid,ContentType union select count(1),operatorid,ContentType from vodprogramme "
			+ "where CatalogStatusID=2 and OperatorId=%s  and ContentType in ('episode','tvseries') and programmeid in (select programmeid from vodprogramme "
			+ "where CatalogStatusID=2  and ApprovedStatus in (1,2,5,6)and ContentType='program' and OperatorId=%s) "
			+ "group by operatorid,ContentType order by 2 ";
	System.out.println(Query);
	map.put("Query",Query);
	return Query;
}
		public String PublishedCount() {
			String Query="select count(1)as Count,operatorid,ContentType from vodprogramme "
			+ "where CatalogStatusID=2  and ContentType in ('movie','program') and ApprovedStatus=5 and OperatorId=%s group by operatorid,ContentType union "
			+ "select count(1)as Count,operatorid,ContentType from vodprogramme "
			+ "where CatalogStatusID=2 and OperatorId=%s and ContentType in ('episode','tvseries') and programmeid in ( "
			+ "select programmeid from vodprogramme where CatalogStatusID=2  and ApprovedStatus=5 and ContentType='program' and OperatorId=%s) group by operatorid,ContentType order by 2 ";
			System.out.println(Query);
			map.put("Query",Query);
			return Query;
}
		public String AllCount() {
			String Query="select count(1),operatorid,ContentType from vodprogramme "
			+ "where CatalogStatusID=2  and ContentType in ('movie','program') and ApprovedStatus in (0,1,2,5,6) and OperatorId=%s "
			+ "group by operatorid,ContentType union select count(1),operatorid,ContentType from vodprogramme "
			+ "where CatalogStatusID=2  and ContentType in ('episode','tvseries') and OperatorId=%s "
			+ "and programmeid in (select programmeid from vodprogramme "
			+ "where CatalogStatusID=2  and ApprovedStatus in (0,1,2,5,6) and ContentType='program' and OperatorId=%s) group by operatorid,ContentType order by 2";
			System.out.println(Query);
			map.put("Query",Query);
			return Query;
}
		
		public String EpisodeLevel() {
			String Query="select EpisodeTitle,ContentType,EpisodeNumber, "
			+ "EpisodeDescription,EpisodeImage,EpisodeReleasedate from vodprogramme "
			+ "where SourceEpisodeid= '%s' and ContentType='Episode'";
			System.out.println(Query);
			map.put("Query",Query);
			return Query;
}
		
		public String TvSeriesLevel() {
			String Query="select ContentType, SeasonName,SeasonImage from vodprogramme "
			+ "where SourceSeasonid= '%s' "
			+ "and ContentType='Tvseries'";
			System.out.println(Query);
			map.put("Query",Query);
			return Query;
}
		public String ProgramLevel() {
			String Query="select programmename,ContentType, Synopsis,ProgramImage,ProgramReleaseDate "
			+ "from vodprogramme where ProgrammeId= '%s' "
			+ "and ContentType='Program'";
			System.out.println(Query);
			map.put("Query",Query);
			return Query;
}

		public String USERID() {
			String Query="select UserID from CLOUDLSN.EPGCloud.dbo.apiregistration "
			+ "where  apikey='%s' ";
			System.out.println(Query);
			map.put("USERID",Query);
			return Query;
}
		
		public String OVDSTREAMING() {
			String Query="Select top 100 v.ProgrammeId, v.ProgrammeName,v.ContentType, v.ProgramImage, "
			+ "v.Synopsis,v.ProductionYear,v.ProgrammeLanguage,v.OperatorId ,vc.UpdateAction "
			+ "from vodprogramme v join vodprogramme_%s vc on V.vodsrid=vc.vodsrid "		
			+ "order by vc.UpdateID ";
			System.out.println(Query);
			map.put("OVDSTREAMING",Query);
			return Query;
}
		public String PROGRAMMEID() {
			String Query="Select top 10 ProgrammeID,ProgrammeName,StatusID from Programme "
			+ "where StatusID=2 ORDER BY NEWID() ";
			System.out.println(Query);
			map.put("PROGRAMMEID",Query);
			return Query;
}
		public String HEADENDID_DATA() {
			String Query="Select top 20  vc.UpdateAction,v.ProgrammeName,v.SeasonName,v.EpisodeTitle,v.ContentType, "
					+"v.ProgrammeGenre,v.ProductionYear,v.ProgrammeLanguage, "
					+"v.ProgrammeId,v.SourceEpisodeid, v.SourceSeasonid "
					+"from vodprogramme v join vodprogramme_%d vc on V.vodsrid=vc.vodsrid "
			+ "where v.OperatorId=%s and vc.UpdateID>%f order by vc.UpdateID";
			System.out.println(Query);
			return Query;
}
		
		
		
		public String VOD_PROGRAMMEID() {
			String Query="Select top 1 VODSRID,ProgrammeID,CMSProgrammeID,ProgrammeName,ContentType,OperatorId,CatalogStatusID "
			+ "From VODProgramme Where CatalogStatusID = 2 And CMSProgrammeID =%s ";
			System.out.println(Query);
			return Query;
}
		public String MANDATE_DATA_VALIDATION() {
			String Query="Select top 10 vp.CMSProgrammeID ,P.ProgrammeName, Mec.M2eCategoryName, "
			+"submec.M2eSubCategoryName, lan.LanguageName from VODProgramme vp join Programme P "
			+"on vp.CMSProgrammeID=P.ProgrammeID join ProgrammeCategoryAssociation PCA "
			+"on P.ProgrammeID=PCA.ProgrammeID join M2eCategory Mec "
			+"on PCA.M2eCategoryID=Mec.M2eCategoryID join M2eSubCategory SubMec "
			+"on PCA.M2eSubCategoryID=SubMec.M2eSubCategoryID join Language Lan "
			+"on p.LanguageID=Lan.LanguageID "
			+ "where P.StatusID=2 and Mec.StatusID=2 and lan.StatusID=2 and SubMec.StatusID=2 ORDER BY NEWID() ";
			System.out.println(Query);
			return Query;
}
		public String OPTIONAL_EN_DATA_VALIDATION() {
			String Query="Select top 10 vp.CMSProgrammeID ,PSD.ShortDescription,PWD.WapDescription,P.Certificate, "
			+"P.ProductionYear from VODProgramme vp join Programme P "
			+"on vp.CMSProgrammeID=P.ProgrammeID  join ProgrammeShortDescription PSD "
			+"on PSD.ProgrammeID=P.ProgrammeID join ProgrammeWapDescription PWD "
			+"on P.ProgrammeID=PWD.ProgrammeID "
			+ "where P.StatusID=2  ORDER BY NEWID() ";
			System.out.println(Query);
			return Query;
}	
		
}
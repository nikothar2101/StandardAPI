package commonFunctions;

import java.util.HashMap;

public class EPGQueries {
	public static HashMap<String, String> map = new HashMap<String, String>();
	
	/*
	  Function Name-Celebrityprogramlist 
	  Purpose- Purpose- Query For getting Program List/Details for Celebrity API.
	 */
	public String Celebrityprogramlist() {
		String Query=" Declare "
				+ " @CastID Bigint=%s "
				+ "Select @CastID As castid,RoleName,Priority,programmeId,Programmename,releasedate,ProgrammeLanguage "
				+ "From ( "
				+ "Select RoleName,Priority,programmeId,Programmename,releasedate,ProgrammeLanguage,OperatorID "
				+ "From ("
				+ "Select  CRPM.RoleId,R.RoleName,CRPM.Priority,CRPM.ProgramID As programmeId "
				+ ",P.ProgrammeName ,Cast(P.FirstAirDate As Date) As releasedate,L.LanguageName As ProgrammeLanguage "
				+ ",Row_Number() Over(Partition By CRPM.ProgramID Order by Priority asc) As rnk "
				+ ",LTrim(RTrim(IsNull(Cast(VP.OperatorID As Varchar),''))) As OperatorID "
				+ "From CastRoleProgramMapping CRPM "
				+ "Inner join RoleMaster R   On R.RoleID		   = CRPM.RoleID "
				+ "Inner Join Programme P    On P.ProgrammeID	   = CRPM.ProgramID And P.StatusID = 2 "
				+ "Left join Language L      On L.LanguageID	   = P.LanguageID   And L.StatusID = 2 "
				+ "Left Join VODProgramme VP On VP.CMSProgrammeID = CRPM.ProgramID And VP.CatalogStatusID = 2 "
				+ "Where CRPM.CastID = @CastID "
				+ ") A "
				+ "Where A.rnk = 1 )A "
				+ "Group by RoleName,Priority,programmeId,Programmename,releasedate,ProgrammeLanguage "
				+ "Order by Year(A.releasedate) desc,A.Priority ";
		System.out.println(Query);
		map.put("Query",Query);
		return Query;
		
	}
	/*
	  Function Name-CastDetails 
	  Purpose- Query For getting Cast Details for Celebrity API.
	 */
	public String CastDetails() {
		String CastDetails="Declare "
				+ "@CastID Bigint= %s "
				+",@CastImageWidth   Int = 0, @CastImageHeight  Int = 0 "
				+"Select Top 1 @CastImageWidth = ResizeWidth,@CastImageHeight = ResizeHeight From EPG..VW_APIAspectRatioResizeDimensions "
				+"Where EntityTypeID = 2 And AspectRatioID = 2 And ProvisionID = 1"
				+"Order By ResizeWidth desc "
				+"Select CM.castID As castid, Name As castname,Case "
				+"When @CastImageHeight > 0 And @CastImageWidth > 0 Then dbo.FN_GetCastImage(CM.CastID,'3:4',@CastImageWidth,@CastImageHeight) "
				+"Else '' End castImage ,CM.height,Cast(CM.DOB As Date) As dob "
				+"From CastMaster CM Where cm.castid = @CastID";
		map.put("CastDetails",CastDetails);
		return CastDetails;
		
	}
	/*
	  Function Name-CastImageDimension 
	  Purpose- Query For getting CastImageURL Dimensions(Width and Height) Detail for Celebrity API.
	 */
	public String CastImageDimension() {
		String CastImageDetails= "Select ResizeWidth,ResizeHeight From EPG..VW_APIAspectRatioResizeDimensions"
				+" Where EntityTypeID = 2 And AspectRatioID = 2 And ProvisionID = 1 Order By ResizeWidth desc";
		map.put("CastDetails",CastImageDetails);
		return CastImageDetails;
	}
	
	public String APIRegistration_Metadata() {
		String Query= " Declare "
				+ " @APIkey Varchar(50) ='%s' "
				+ "Select A.UserID,EmailID,A.ToDate,C.ProductName,D.PackageName,F.MetaDataName, G.PropertyValue As AssignedBucket, "
				+ "H.PropertyValue As EnabledHierarchy, K.PropertyValue As ContentType "
				+ "From APIregistration A "
				+ "Inner Join Productpackagemapping B  On B.ProductPackageID = A.ProductPackageID "
				+ "Inner Join Products C			    On C.ProductID		  = B.ProductID "
				+ "Inner Join Package D			    On D.PackageID		  = B.PackageID "
				+ "Inner join PackageMetaDataMapping E On D.PackageID		  = E.PackageID "
				+ "Inner join MetaData	F				On F.MetaDataID		  = E.MetaDataID "
				+ "left join  UserLevelConfigurations G On G.UserID		  = A.UserID		And G.PropertyID = 1044 "
				+ "left join  UserLevelConfigurations H On H.UserID		  = A.UserID		And H.PropertyID = 1045 "
				+ "left join  UserLevelConfigurations K On K.UserID		  = A.UserID		And K.PropertyID = 1043 "
				+ "Where A.APIKey = @APIkey "
				+ "Order By Case WHEN F.MetaDataName like 'Commercial%' "
				+ "Then 1 else 0 End desc";
		map.put("SpecificAPI_Metadata",Query);
		return Query;
	}
	
	public String LanguageAssigned_ProgrammeDetails() {
		String Query= "Select A.UserID,A.APIKey,C.ProductName,D.PackageName, G.PropertyValue As LanguageAssigned "
				+ "From APIregistration A "
				+ "Inner Join Productpackagemapping B  On B.ProductPackageID = A.ProductPackageID "
				+ "Inner Join Products C			    On C.ProductID		  = B.ProductID "
				+ "Inner Join Package D			    On D.PackageID		  = B.PackageID "
				+ "left join  APILevelConfigurations G On G.UserID		  = A.UserID		And G.PropertyID = 1005 "
				+ "and A.UserProdID=G.UserProdID "
				+ "Where C.ProductName='VODProgrammeDetails' and A.UserID=3370516 "
				+ "Order By PackageName ";
		return Query;
	}
	
	public String ProgrammeDetails_Language() {
		String Query= "Select A.UserID,A.APIKey,C.ProductName,D.PackageName, G.PropertyValue As LanguageAssigned "
				+ "From APIregistration A "
				+ "Inner Join Productpackagemapping B  On B.ProductPackageID = A.ProductPackageID "
				+ "Inner Join Products C			    On C.ProductID		  = B.ProductID "
				+ "Inner Join Package D			    On D.PackageID		  = B.PackageID "
				+ "left join  APILevelConfigurations G On G.UserID		  = A.UserID		And G.PropertyID = 1005 "
				+ "and A.UserProdID=G.UserProdID "
				+ "Where C.ProductName='VODProgrammeDetails' and A.UserID=3370516 "
				+ "and A.APIKey='%s' "
				+ "Order By PackageName ";
		return Query;
	}

}

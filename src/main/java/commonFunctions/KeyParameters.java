package commonFunctions;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Properties;

import com.aventstack.extentreports.Status;

import reports.ExtentTestManager;

public class KeyParameters {

	public static HashMap<String, String> map = new HashMap<String, String>();
	String EndpointURL;
	String DynamicURL;
	InputStream inputStream;
	Properties prop = new Properties();
	String propFileName = "config.properties";

	/*
	  Function Name-Common_InvalidreturnNoValue 
	  Purpose- To validate Tag when Invalid Value is entered in request
	 */

	public void Common_InvalidreturnNoValue(String TagName) throws IOException {
		try {
		ApiKey apiKeyTag = new ApiKey();
		EndpointURL = ApiKey.map.get("EndpointURL");
		String[] NoTag = EndpointURL.split(TagName + "=");
		String[] NoTag1 = NoTag[1].split("&", 2);
		DynamicURL = NoTag[0] + TagName + "=" + ExcelUtility.map.get("InvalidValue") + "&" + NoTag1[1];
		System.out.println(DynamicURL);
		map.put("DynamicURL", DynamicURL);
		apiKeyTag.toFetchResponseAttributesFromNoValue();
		}
		catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
			
		
	}
	/*
	  Function Name-noLanguage_Tag 
	  Purpose- To validate Language Tag when Invalid Tag is not entered in request
	 */

	public void noLanguage_Tag() throws IOException {
		EndpointURL = ApiKey.map.get("EndpointURL");
		String[] NoTag = EndpointURL.split("&language=");
		String[] NoTag1 = NoTag[1].split("&", 2);
		DynamicURL = NoTag[0] + "&" + NoTag1[1];
		System.out.println(DynamicURL);
		map.put("DynamicURL", DynamicURL);
	}
	/*
	  Function Name-noLanguage_Value 
	  Purpose- To validate Language Tag when value is not entered in request
	 */
	public void noLanguage_Value() throws IOException {
		EndpointURL = ApiKey.map.get("EndpointURL");
		String[] NoTag = EndpointURL.split("&language=");
		String[] NoTag1 = NoTag[1].split("&", 2);
		DynamicURL = NoTag[0] + "&language=&" + NoTag1[1];
		System.out.println(DynamicURL);
		map.put("DynamicURL", DynamicURL);
	}

	/*
	  Function Name-noResponseFormatKeyTag 
	  Purpose- To validate Response Format Tag when Tag is not entered in request
	 */
	
	public void noResponseFormatKeyTag() throws IOException {
		ApiKey apiKeyTag = new ApiKey();
		EndpointURL = ApiKey.map.get("EndpointURL");
		String[] NoTag = EndpointURL.split("responseformat=");
		String[] NoTag1 = NoTag[1].split("&", 2);
		DynamicURL = NoTag[0] + NoTag1[1];
		map.put("DynamicURL", DynamicURL);
		apiKeyTag.toFetchResponseAttributesXML();

	}
	/*
	  Function Name-XMLErrorInValidValue 
	  Purpose- To validate XML Response
	 */
	
	public void XMLErrorInValidValue(String TagName) throws IOException {
		ApiKey apiKeyTag = new ApiKey();
		EndpointURL = ApiKey.map.get("EndpointURL");
		String[] NoTag = EndpointURL.split(TagName + "=");
		String[] NoTag1 = NoTag[1].split("&", 2);
		DynamicURL = NoTag[0] + TagName + "=&" + NoTag1[1];
		map.put("DynamicURL", DynamicURL);
		apiKeyTag.toFetchResponseAttributesXML();
	}

	/*
	  Function Name-XMLErrorTagValueNoPresent 
	  Purpose- To validate XML Response when tag value is not present
	 */
	
	public void XMLErrorTagValueNoPresent(String TagName) throws IOException {
		ApiKey apiKeyTag = new ApiKey();
		EndpointURL = ApiKey.map.get("EndpointURL");
		String[] NoTag = EndpointURL.split(TagName + "=");
		String[] NoTag1 = NoTag[1].split("&", 2);
		DynamicURL = NoTag[0] + TagName + "=&" + NoTag1[1];
		map.put("DynamicURL", DynamicURL);
		apiKeyTag.toFetchResponseAttributesXML();
	}

	/*
	  Function Name-Language_InvalideValue 
	  Purpose- To validate Response when  value is Ivalid
	 */
	public void Language_InvalideValue() throws IOException {
		ApiKey apiKeyTag = new ApiKey();
		EndpointURL = ApiKey.map.get("EndpointURL");
		String[] NoTag = EndpointURL.split("&language=");
		String[] NoTag1 = NoTag[1].split("&", 2);
		DynamicURL = NoTag[0] + "&language=" + ExcelUtility.map.get("InvalidValue") + "&" + NoTag1[1];
		System.out.println(DynamicURL);
		map.put("DynamicURL", DynamicURL);
		apiKeyTag.toFetchResponseAttributesFromNoValue();
	}
	/*
	  Function Name-LogoType_Tag 
	  Purpose- To validate Response when  Tag is not present
	 */
	public void LogoType_Tag(String TagName) throws IOException {
		EndpointURL = ApiKey.map.get("EndpointURL");
		String[] NoTag = EndpointURL.split(TagName+"=");
		DynamicURL = NoTag[0];
		System.out.println(DynamicURL);
		map.put("DynamicURL", DynamicURL);
		ExtentTestManager.getTest().log(Status.INFO, "DYNAMIC URL IS :" + DynamicURL);
	}
	/*
	  Function Name-LogoType_Value 
	  Purpose- To validate Response when  value is not present
	 */
	public void LogoType_Value(String TagName) throws IOException {
		EndpointURL = ApiKey.map.get("EndpointURL");
		String[] NoTag = EndpointURL.split(TagName+"=");
		DynamicURL = NoTag[0] + TagName+"=";
		System.out.println(DynamicURL);
		map.put("DynamicURL", DynamicURL);
		ExtentTestManager.getTest().log(Status.INFO, "DYNAMIC URL IS :" + DynamicURL);
	}
	/*
	  Function Name-LogoType_InvalidValue 
	  Purpose- To validate Response when  value is Invalid
	 */
	public void LogoType_InvalidValue(String TagName,String Sheetname) throws IOException {
		ApiKey apiKeyTag = new ApiKey();
		EndpointURL = ApiKey.map.get("EndpointURL");
		String[] NoTag = EndpointURL.split(TagName+"=");
		DynamicURL = NoTag[0] + TagName+"=" + ExcelUtility.map.get("InvalidValue");
		System.out.println(DynamicURL);
		map.put("DynamicURL", DynamicURL);
		ExtentTestManager.getTest().log(Status.INFO, "DYNAMIC URL IS :" + DynamicURL);
		if(Sheetname.equalsIgnoreCase("OVDStreaming")) {
			apiKeyTag.toFetchResponseAttributesCelebrityAPI();
			}
	}
	public void LastTagInvalidValue(String TagName,String SheetName) throws IOException {
		ApiKey key= new ApiKey();
		EndpointURL = ApiKey.map.get("EndpointURL");
		String[] NoTag = EndpointURL.split(TagName+"=");
		DynamicURL = NoTag[0] + TagName+"=" + ExcelUtility.map.get("InvalidValue");
		map.put("DynamicURL", DynamicURL);
		if(SheetName.equalsIgnoreCase("AdvanceSearch")) {
			key.toFetchResponseAttributes();
		}
		else {
			key.toFetchResponseAttributesCelebrityAPI();
		}
	}
	/*
	  Function Name-TagNotPresent 
	  Purpose- Common function for removing tag name
	 */
	public void TagNotPresent(String TagName) throws IOException {
		EndpointURL = ApiKey.map.get("EndpointURL");
		String[] NoTag = EndpointURL.split(TagName + "=");
		String[] NoTag1 = NoTag[1].split("&", 2);
		DynamicURL = NoTag[0] + NoTag1[1];
		map.put("DynamicURL", DynamicURL);
		ExtentTestManager.getTest().log(Status.INFO, "DYNAMIC URL IS :" + DynamicURL);
	}
	/*
	  Function Name-ErrorTagNotPresent 
	  Purpose- Common function for removing tag name when response has error
	 */
	public void ErrorTagNotPresent(String TagName, String Sheetname) throws IOException {
		ApiKey apiKeyTag = new ApiKey();
		EndpointURL = ApiKey.map.get("EndpointURL");
		String[] NoTag = EndpointURL.split(TagName + "=");
		String[] NoTag1 = NoTag[1].split("&", 2);
		DynamicURL = NoTag[0] + NoTag1[1];
		map.put("DynamicURL", DynamicURL);
		if(Sheetname.equalsIgnoreCase("AdvancedSearch")) {
		apiKeyTag.toFetchResponseAttributes();
		}else {
			apiKeyTag.toFetchResponseAttributesCelebrityAPI();
		}
	}
	/*
	  Function Name-TagValueNoPresent 
	  Purpose- Common function for removing TagValue
	 */
	public void TagValueNoPresent(String TagName) throws IOException {
		EndpointURL = ApiKey.map.get("EndpointURL");
		String[] NoTag = EndpointURL.split(TagName + "=");
		String[] NoTag1 = NoTag[1].split("&", 2);
		DynamicURL = NoTag[0] + TagName + "=&" + NoTag1[1];
		map.put("DynamicURL", DynamicURL);
		ExtentTestManager.getTest().log(Status.INFO, "DYNAMIC URL IS :" + DynamicURL);
	}
	/*
	  Function Name-ErrorTagValueNoPresent 
	  Purpose- Common function for removing value
	 */
	public void ErrorTagValueNoPresent(String TagName, String Sheetname) throws IOException {
		ApiKey apiKeyTag = new ApiKey();
		EndpointURL = ApiKey.map.get("EndpointURL");
		String[] NoTag = EndpointURL.split(TagName + "=");
		String[] NoTag1 = NoTag[1].split("&", 2);
		DynamicURL = NoTag[0] + TagName + "=&" + NoTag1[1];
		map.put("DynamicURL", DynamicURL);
		if(Sheetname.equalsIgnoreCase("AdvanceSearch")) {
		apiKeyTag.toFetchResponseAttributes();}
		else {
			apiKeyTag.toFetchResponseAttributesCelebrityAPI();
		}
	}
	/*
	  Function Name-InValidValuePresent 
	  Purpose- Common function for adding invalid Value
	 */
	public String InValidValuePresent(String TagName) throws IOException {
		EndpointURL = ApiKey.map.get("EndpointURL");
		String[] NoTag = EndpointURL.split(TagName + "=");
		String[] NoTag1 = NoTag[1].split("&", 2);
		System.out.println(ExcelUtility.map.get("InvalidValue"));
		DynamicURL = NoTag[0] + TagName + "=" + ExcelUtility.map.get("InvalidValue") + "&" + NoTag1[1];
		map.put("DynamicURL", DynamicURL);
		ExtentTestManager.getTest().log(Status.INFO, "DYNAMIC URL IS :" + DynamicURL);
		System.out.println(DynamicURL);
		return DynamicURL;
	}
	/*
	  Function Name-InValidValuePresent 
	  Purpose- Common function for adding invalid Value
	 */
	public void MaxOffSet(String TagName) throws IOException {
		EndpointURL = ApiKey.map.get("EndpointURL");
		String[] NoTag = EndpointURL.split(TagName + "=");
		String[] NoTag1 = NoTag[1].split("&", 2);
		System.out.println(ResponseValidation.map.get("maxchangeoffset"));
		DynamicURL = NoTag[0] + TagName + "=" + ResponseValidation.map.get("maxchangeoffset") + "&" + NoTag1[1];
		map.put("DynamicURL", DynamicURL);
		ExtentTestManager.getTest().log(Status.INFO, "DYNAMIC URL IS :" + DynamicURL);
		System.out.println(DynamicURL);
	}
	
	/*
	  Function Name-InValidValuePresent 
	  Purpose- Common function for adding invalid Value
	 */
	public void MultipleValuesPresent(String TagName) throws IOException {
		EndpointURL = ApiKey.map.get("EndpointURL");
		String[] NoTag = EndpointURL.split(TagName + "=");
		String[] NoTag1 = NoTag[1].split("&", 2);
		System.out.println(ExcelUtility.map.get("InvalidValue"));
		DynamicURL = NoTag[0] + TagName + "=" + ExcelUtility.map.get("InvalidValue") + "&" + NoTag1[1];
		System.out.println(DynamicURL);
		NoTag = DynamicURL.split("headendids=");
		NoTag1 = NoTag[1].split(";", 2);
		System.out.println(ExcelUtility.map.get("OperatorID"));
		DynamicURL = NoTag[0] + "headendids=" + ExcelUtility.map.get("OperatorID") + ";" + NoTag1[1];
		map.put("DynamicURL", DynamicURL);
		ExtentTestManager.getTest().log(Status.INFO, "DYNAMIC URL IS :" + DynamicURL);
		System.out.println(DynamicURL);
	}
	
	/*
	  Function Name-InValidValuePresent 
	  Purpose- Common function for adding invalid Value
	 */
	public void OVD_MultipleValue(String TagName) throws IOException {
		EndpointURL = ApiKey.map.get("EndpointURL");
		String[] NoTag = EndpointURL.split(TagName + "=");
		String[] NoTag1 = NoTag[1].split("&", 2);
		System.out.println(ExcelUtility.map.get("InvalidValue"));
		DynamicURL = NoTag[0] + TagName + "=" + ExcelUtility.map.get("InvalidValue") + "&" + NoTag1[1];
		System.out.println(DynamicURL);
		NoTag = DynamicURL.split("limit=");
		//NoTag1 = NoTag[1].split("&", 2);
		System.out.println(ExcelUtility.map.get("limit"));
		DynamicURL = NoTag[0] + "limit=" + ExcelUtility.map.get("Limit");
		map.put("DynamicURL", DynamicURL);
		ExtentTestManager.getTest().log(Status.INFO, "DYNAMIC URL IS :" + DynamicURL);
		System.out.println(DynamicURL);
	}
	
	public void PageSize_Number(int i) throws IOException {
		
		String[] NoTag = DynamicURL.split("pageno" + "=");
		String[] NoTag1 = NoTag[1].split("&", 2);
		DynamicURL = NoTag[0] + "pageno=" + i + "&" + NoTag1[1];
		map.put("DynamicURL", DynamicURL);
		ExtentTestManager.getTest().log(Status.INFO, "DYNAMIC URL IS :" + DynamicURL);
		System.out.println(DynamicURL);
	}
	
	/*
	  Function Name-ErrorInValidValue 
	  Purpose- Common function for adding invalid Value when response has error
	 */
	public void ErrorInValidValue(String TagName,String Sheetname) throws IOException {
		ApiKey apiKeyTag = new ApiKey();
		EndpointURL = ApiKey.map.get("EndpointURL");
		String[] NoTag = EndpointURL.split(TagName + "=");
		String[] NoTag1 = NoTag[1].split("&", 2);
		DynamicURL = NoTag[0] + TagName + "=" + ExcelUtility.map.get("InvalidValue") + "&" + NoTag1[1];
		map.put("DynamicURL", DynamicURL);
		if(Sheetname.equalsIgnoreCase("AdvanceSearch")) {
		apiKeyTag.toFetchResponseAttributes();
		}
		else {
			apiKeyTag.toFetchResponseAttributesCelebrityAPI();
		}
	}
	/*
	  Function Name-ContextTagNotPresent 
	  Purpose- Common function for removing tag name from context
	 */
	public void ContextTagNotPresent(String TagName) throws IOException {
		EndpointURL = ApiKey.map.get("EndpointURL");
		String[] NoTag = EndpointURL.split(TagName + "=");
		String[] NoTag1 = NoTag[1].split(";", 2);
		if (TagName.contains("channelimagewidth") || TagName.contains("programmeimagewidth")) {
			DynamicURL = NoTag[0] + NoTag1[1];
			if (TagName.contains("channelimagewidth")) {
				String channelimagewidth[] = DynamicURL.split("channelimageheight=");
				String[] FinalTag = channelimagewidth[1].split(";", 2);
				DynamicURL = channelimagewidth[0] + FinalTag[1];
			} else {
				String channelimagewidth[] = DynamicURL.split("programmeimageheight=");
				String[] FinalTag = channelimagewidth[1].split("&", 2);
				DynamicURL = channelimagewidth[0] + FinalTag[1];
			}
		}

		else {
			DynamicURL = NoTag[0] + NoTag1[1];
		}
		map.put("DynamicURL", DynamicURL);
		ExtentTestManager.getTest().log(Status.INFO, "DYNAMIC URL IS :" + DynamicURL);
	}
	/*
	  Function Name-ContextTagNotPresent 
	  Purpose- Common function for removing tag name from context
	 */
	public void ErrorContextTagNotPresent(String TagName,String Sheetname) throws IOException {
		ApiKey apiKeyTag = new ApiKey();
		EndpointURL = ApiKey.map.get("EndpointURL");
		String[] NoTag = EndpointURL.split(TagName + "=");
		String[] NoTag1 = NoTag[1].split(";", 2);
		DynamicURL = NoTag[0] + NoTag1[1];
		map.put("DynamicURL", DynamicURL);
		if(Sheetname.equalsIgnoreCase("AdvanceSearch")) {
		apiKeyTag.toFetchResponseAttributes();
		}
		else{
			apiKeyTag.toFetchResponseAttributesCelebrityAPI();
		}
	}
	/*
	  Function Name-ContextValueNotPresent 
	  Purpose- Common function for removing Value from Context
	 */
	public void ContextValueNotPresent(String TagName) throws IOException {
		EndpointURL = ApiKey.map.get("EndpointURL");
		String[] NoTag = EndpointURL.split(TagName + "=");
		String[] NoTag1 = NoTag[1].split(";", 2);
		DynamicURL = NoTag[0] + TagName + "=;" + NoTag1[1];
		map.put("DynamicURL", DynamicURL);
		ExtentTestManager.getTest().log(Status.INFO, "DYNAMIC URL IS :" + DynamicURL);
	}
	/*
	  Function Name-ErrorContextValueNotPresent 
	  Purpose- Common function for removing Value from Context when response has error
	 */
	public void ErrorContextValueNotPresent(String TagName,String Sheetname) throws IOException {
		ApiKey apiKeyTag = new ApiKey();
		EndpointURL = ApiKey.map.get("EndpointURL");
		String[] NoTag = EndpointURL.split(TagName + "=");
		String[] NoTag1 = NoTag[1].split(";", 2);
		if (TagName.contains("channelimagewidth") || TagName.contains("programmeimagewidth")) {
			DynamicURL = NoTag[0] + TagName + "=;" + NoTag1[1];
			if (TagName.contains("channelimagewidth")) {
				String channelimagewidth[] = DynamicURL.split("channelimageheight=");
				String[] FinalTag = channelimagewidth[1].split(";", 2);
				DynamicURL = channelimagewidth[0] + "channelimageheight=;" + FinalTag[1];
			} else {
				String channelimagewidth[] = DynamicURL.split("programmeimageheight=");
				String[] FinalTag = channelimagewidth[1].split("&", 2);
				if(Sheetname.contains("ProgrammeDetails")) {
					DynamicURL = channelimagewidth[0] + "programmeimageheight=&" + FinalTag[1];
				
				}
				else {
					 DynamicURL = channelimagewidth[0] + "programmeimageheight=;" + FinalTag[1];
				}
			}
		} else {
			DynamicURL = NoTag[0] + TagName + "=;" + NoTag1[1];
		}
		map.put("DynamicURL", DynamicURL);
		if(Sheetname.equalsIgnoreCase("AdvanceSearch")) {
		apiKeyTag.toFetchResponseAttributes();
		}
		else {
			apiKeyTag.toFetchResponseAttributesCelebrityAPI();
		}
		}
		
	/*
	  Function Name-ErrorContextInvalidValuePresent 
	  Purpose- Common function for removing Value from Context when response has error
	 */
	public void ErrorContextInvalidValuePresent(String TagName, String Sheetname) throws IOException {
		// GetRequestMethod GetReq =new GetRequestMethod();
		ApiKey apiKeyTag = new ApiKey();
		EndpointURL = ApiKey.map.get("EndpointURL");
		String[] NoTag = EndpointURL.split(TagName + "=");
		String[] NoTag1 = NoTag[1].split(";", 2);
		if (TagName.contains("channelimagewidth") || TagName.contains("programmeimagewidth")) {
			DynamicURL = NoTag[0] + TagName + "=" + ExcelUtility.map.get("InvalidValue") + ";" + NoTag1[1];
			if (TagName.contains("channelimagewidth")) {
				String channelimagewidth[] = DynamicURL.split("channelimageheight=");
				String[] FinalTag = channelimagewidth[1].split(";", 2);
				DynamicURL = channelimagewidth[0] + TagName + "=" + ExcelUtility.map.get("InvalidValue") + ";"
						+ FinalTag[1];
			} else {
				// DynamicURL=
				// NoTag[0]+TagName+"="+ExcelUtility.map.get("InvalidValue")+";"+NoTag1[1];
				if(Sheetname.equalsIgnoreCase("AdvanceSearch")) {
				String channelimagewidth[] = DynamicURL.split("programmeimageheight=");
				String[] FinalTag = channelimagewidth[1].split("&", 2);
				DynamicURL = channelimagewidth[0] + "programmeimageheight=" + ExcelUtility.map.get("InvalidValue") + ";"
						+ FinalTag[1];
				System.out.println(DynamicURL);
				}
			}
		} else {
			DynamicURL = NoTag[0] + NoTag1[1];
			DynamicURL = NoTag[0] + TagName + "=" + ExcelUtility.map.get("InvalidValue") + ";" + NoTag1[1];
		}

		map.put("DynamicURL", DynamicURL);
		if(Sheetname.equalsIgnoreCase("AdvanceSearch")) {
		apiKeyTag.toFetchResponseAttributes();}
		else {
			apiKeyTag.toFetchResponseAttributesCelebrityAPI();
		}
	}
	/*
	  Function Name-ContextInvalidValuePresent 
	  Purpose- Common function for removing Value from Context
	 */
	public void ContextInvalidValuePresent(String TagName) throws IOException {
		EndpointURL = ApiKey.map.get("EndpointURL");
		String[] NoTag = EndpointURL.split(TagName + "=");
		String[] NoTag1 = NoTag[1].split(";", 2);
		DynamicURL = NoTag[0] + TagName + "=" + ExcelUtility.map.get("InvalidValue") + ";" + NoTag1[1];
		map.put("DynamicURL", DynamicURL);
		ExtentTestManager.getTest().log(Status.INFO, "DYNAMIC URL IS :" + DynamicURL);
	}
	/*
	  Function Name-ContextInvalidValuePresent_NoValue 
	  Purpose- Common function for removing Value from Context when response has zero record
	 */
	public void ContextInvalidValuePresent_NoValue(String TagName) throws IOException {
		ApiKey apiKeyTag = new ApiKey();
		EndpointURL = ApiKey.map.get("EndpointURL");
		String[] NoTag = EndpointURL.split(TagName + "=");
		String[] NoTag1 = NoTag[1].split(";", 2);
		DynamicURL = NoTag[0] + TagName + "=" + ExcelUtility.map.get("InvalidValue") + ";" + NoTag1[1];
		map.put("DynamicURL", DynamicURL);
		ExtentTestManager.getTest().log(Status.INFO, "DYNAMIC URL IS :" + DynamicURL);
		apiKeyTag.toFetchResponseAttributesFromNoValue();
	}
	/*
	  Function Name-ContextNoValuePresent_NoValue 
	  Purpose- Common function for removing Value from Context when response has zero records
	 */
	public void ContextNoValuePresent_NoValue(String TagName) throws IOException {
		ApiKey apiKeyTag = new ApiKey();
		EndpointURL = ApiKey.map.get("EndpointURL");
		String[] NoTag = EndpointURL.split(TagName + "=");
		String[] NoTag1 = NoTag[1].split(";", 2);
		DynamicURL = NoTag[0] + TagName + "=;" + NoTag1[1];
		map.put("DynamicURL", DynamicURL);
		ExtentTestManager.getTest().log(Status.INFO, "DYNAMIC URL IS :" + DynamicURL);
		apiKeyTag.toFetchResponseAttributesFromNoValue();
	}
	/*
	  Function Name-ContextNoTagPresent_NoValue 
	  Purpose- Common function for removing Value from Context when response has zero records
	 */
	public void ContextNoTagPresent_NoValue(String TagName,String Sheetname) throws IOException {
		ApiKey apiKeyTag = new ApiKey();
		EndpointURL = ApiKey.map.get("EndpointURL");
		String[] NoTag = EndpointURL.split(TagName + "=");
		if(NoTag[1].contains(";")) {
		String[] NoTag1 = NoTag[1].split(";", 2);
		DynamicURL = NoTag[0] + NoTag1[1];
		}
		else {
			
			String[] NoTag1 = NoTag[1].split("&", 2);
			DynamicURL = NoTag[0] +"&"+ NoTag1[1];
			System.out.println(DynamicURL);
		}
		map.put("DynamicURL", DynamicURL);
		ExtentTestManager.getTest().log(Status.INFO, "DYNAMIC URL IS :" + DynamicURL);
		if(Sheetname.equalsIgnoreCase("AdvanceSearch")) {
		apiKeyTag.toFetchResponseAttributesFromNoValue();}
		else {
			apiKeyTag.toFetchResponseAttributesCelebrityAPI();
		}
	}
	/*
	  Function Name-CastImageValidation 
	  Purpose- function for adding dimension for Cast Image in Celebrity API
	 */
	public void CastImageValidation() throws IOException {
		//ApiKey apiKeyTag = new ApiKey();
		EndpointURL = ApiKey.map.get("EndpointURL");
		String[] NoTag = EndpointURL.split("castimagewidth=");
		String[] NoTag1 = NoTag[1].split("&", 2);
		String[] Tag= NoTag1[1].split("castimageheight=");
		String[] Tag1=Tag[1].split("&",2);
		String[] castID =Tag1[1].split("castid=");
		String[] castID1=castID[1].split("&",2);
		DynamicURL = NoTag[0] + "castimagewidth=" + ExcelUtility.map.get("CastImageWidth")
		+ "&castimageheight="+ExcelUtility.map.get("CastImageHeight")+"&castid="
		+ ExcelUtility.map.get("CastID")+"&" + castID1[1];
		map.put("DynamicURL", DynamicURL);
		ExtentTestManager.getTest().log(Status.INFO, "DYNAMINC URL IS : "+DynamicURL);
		}

}

package commonFunctions;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;

import com.aventstack.extentreports.Status;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import assertions.Validations;
import io.restassured.module.jsv.JsonSchemaValidator;
import io.restassured.response.Response;
import reports.ExtentTestManager;
import response.ErrorCelebrityResponse;
import steps.GetRequestMethod;

public class ResponseValidation {
	public static HashMap<String, Integer> map = new HashMap<String, Integer>();
	public static HashMap<String, String> MapS = new HashMap<String, String>();
	
	public static void compareJSONDynamic(String source, String target) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			JsonNode sourceNode = mapper.readTree(source);
			JsonNode targetNode = mapper.readTree(target);
			if(targetNode.equals(sourceNode)) {
				ExtentTestManager.getTest().log(Status.PASS, "RESPONSE IS SEMANTICALLY IDENTICAL ");
			}
			else {
				ExtentTestManager.getTest().log(Status.WARNING, "RESPONSE IS NOT SEMANTICALLY IDENTICAL ");
			}
			//return targetNode.equals(sourceNode);
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
	/*
	  Function Name-getChannels 
	  Purpose- Returns Result for Cast Image Dimensions in Celebrity API and store in hashmap.
	 */
	public static void compareJSON(String source, String target) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			//HashMap<String, Map<String,String>> mapLIVECopy = new HashMap<String,Map<String,String>>();
			JsonNode sourceNode = mapper.readTree(source);
			JsonNode targetNode = mapper.readTree(target);
			if(targetNode.has("Exception")|| sourceNode.has("Exception")) {
				ExtentTestManager.getTest().log(Status.FAIL, "RESPONSE :"+targetNode);
			    //Assert.fail();
			}else {
			if(targetNode.equals(sourceNode)) {
				ExtentTestManager.getTest().log(Status.PASS, "RESPONSE IS SEMANTICALLY IDENTICAL "+targetNode);
			}
			else {
				ResponseValidation jsonOperations = new ResponseValidation();
				JSONObject Source = jsonOperations.getChannels(source,"searchprogramme");
				JSONObject Target = jsonOperations.getChannels(target,"searchprogramme");
				JSONObject SourceChannel = (JSONObject) Source;
				JSONObject TargetChannel = (JSONObject) Target;
				if (!jsonOperations.validateChannelDetailResults(SourceChannel, TargetChannel))
				{
					System.out.println("Channel Data is as Expected " );
					ExtentTestManager.getTest().log(Status.PASS, "PROGRAM DETAILS ARE VALIDATED");  
				} else 
				{ 
					System.out.println("Channel Data Mismatch");
					ExtentTestManager.getTest().log(Status.FAIL, "PROGRAM DETAILS ARE MISMATCHED/NOt VALIDATED"); 
				}
				//ExtentTestManager.getTest().log(Status.WARNING, "RESPONSE IS NOT SEMANTICALLY IDENTICAL");
				//System.out.println();
			}
			
		} 
		}catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
	/*
	  Function Name-getChannels 
	  Purpose- Returns JSON Result in Celebrity API and store in hashmap.
	 */
	public JSONObject getChannels(String responseString, String ListName)
	{
		JSONObject Channels = new JSONObject();
		try
		{
			JSONObject response = new JSONObject(responseString);
			Channels = response.getJSONObject(ListName);
			System.out.println(Channels);
		}
		catch (Exception e)
		{
			ExtentTestManager.getTest().log(Status.FAIL,"Exception Occured! Message: " + e.getLocalizedMessage());
			Assert.fail("Exception Occured! Message: " + e.getLocalizedMessage());
		}
		return Channels;
	}
/*
 		Function Name-validateCastImageDimension 
 		Purpose- Function for validating cast Dimensions of castImageURL in Celebrity API
 */
	public void validateCastImageDimension( Object expected) throws SQLException {
		DatabaseConnection Connector= new DatabaseConnection();
		JSONObject expectedChannel = (JSONObject) expected;
		EPGQueries Queries =new EPGQueries();
		Queries.CastImageDimension();
		String Query=EPGQueries.map.get("CastDetails");
		System.out.println(Query);
		String CastWidthActual= ExcelUtility.map.get("CastImageWidth");
		String CastHeightActual= ExcelUtility.map.get("CastImageHeight");
		String ResponseValue="";
		int matchCount=0;
		for (String expectedkey : expectedChannel.keySet()) {
				if(expectedkey.equalsIgnoreCase("castimageurl")) {
					String DBValue = DatabaseConnection.castDetails.get(expectedkey);
					ResponseValue= (String) expectedChannel.get(expectedkey);
					System.out.println(ResponseValue);
					  System.out.print(DBValue);
					  if(ResponseValue.contains(CastWidthActual) && ResponseValue.contains(CastHeightActual)) {
						  ExtentTestManager.getTest().log(Status.PASS,"CAST IMAGE DIMENSION IS SAME AS ENTERED DIMENSION "+CastWidthActual +"---"+CastHeightActual);
						 break;
					  }
					  else {
						  Connector.getCastDimensions();
							  	String DBHeight= DatabaseConnection.castDetails.get("DBHeight");
							    String DBWidth= DatabaseConnection.castDetails.get("DBWidth");
							    String[] Img = ResponseValue.split("/");
							    String[] sizearray = Img[Img.length - 2].split("x");
							    System.out.println(sizearray[0]);
								System.out.println(sizearray[1]);
							    int DBHEIGHT =Integer.parseInt(DBHeight);
							    int DBWIDTH=Integer.parseInt(DBWidth);
							    int ActualWidth=Integer.parseInt(sizearray[0]);
								int ActualHeight=Integer.parseInt(sizearray[1]);
								if(DBWIDTH >= ActualWidth && DBHEIGHT >= ActualHeight ) {
								Connector.getResult(Query);
							    while(DatabaseConnection.resultSet.next()) {
								String height = DatabaseConnection.resultSet.getString("ResizeHeight");
								int CastHeight = Integer.parseInt(height);
								String width = DatabaseConnection.resultSet.getString("ResizeWidth");
								int CastWidth = Integer.parseInt(width);
								if(ActualWidth==CastWidth && ActualHeight==CastHeight) {
								ExtentTestManager.getTest().log(Status.PASS, "DBHEIGHT: "+DBHEIGHT+" RESPONSEHEIGHT: "+ActualHeight+" DBWIDTH: "+DBWIDTH+" RESPONSEWIDTH: "+ActualWidth);
								matchCount++;
								break;
								}
								}
							    if(matchCount==0) {
							    	ExtentTestManager.getTest().log(Status.FAIL, "DIMENSIONS IN RESPONSE IS NOT PRESENR IN DB");
							    	Assert.fail();
							    }
							}
								else {
								ExtentTestManager.getTest().log(Status.FAIL, "CASTIMAGE DIMENSION IS GREATER THAN DATABASE DEFINED DIMENSION");
								ExtentTestManager.getTest().log(Status.FAIL, "DBHEIGHT: "+DBHEIGHT+" RESPONSEHEIGHT: "+ActualHeight+" DBWIDTH: "+DBWIDTH+" RESPONSEWIDTH: "+ActualWidth);
								Assert.fail();
								}
					  }
				}
						
				}
	}
	/*
	  Function Name-validateCastDetails 
	  Purpose- Validate Cast Details from Database of Celebrity API.
	 */
	public boolean validateCastDetails( Object expected) 

	{
		DatabaseConnection Connector= new DatabaseConnection();
		boolean mismatch = false; 
		JSONObject expectedChannel = (JSONObject) expected;
		Connector.getCastDetails();
		String ResponseValue="";
		for (String expectedkey : expectedChannel.keySet()) {
			if(expectedkey.equalsIgnoreCase("programlist")||expectedkey.equalsIgnoreCase("pagerecordcount")
					||expectedkey.equalsIgnoreCase("totalrecordcount")) {
				ExtentTestManager.getTest().log(Status.INFO, expectedkey.toUpperCase()+" IS NOT VALIDATED");
			}
			else{
				try {
				String DBValue = DatabaseConnection.castDetails.get(expectedkey);
				if(DBValue==null) {
					try {
				ResponseValue= (String) expectedChannel.get(expectedkey);
				System.out.println(ResponseValue);
				  System.out.print(DBValue);
				}catch(Exception e) {
					ExtentTestManager.getTest().log(Status.PASS,expectedkey.toUpperCase()+": IS NULL IN RESPONSE AND DB");
					
				}}
				else {
					ResponseValue= (String) expectedChannel.get(expectedkey);
				if(ResponseValue.contains(DBValue)) {
					ExtentTestManager.getTest().log(Status.PASS,"EXPECTED "+expectedkey+": "+ ResponseValue+ " ACTUAL "+expectedkey+": "+DBValue);
					
				}
				else {
					ExtentTestManager.getTest().log(Status.FAIL,"EXPECTED "+expectedkey+": "+ ResponseValue+ " ACTUAL "+expectedkey+": "+DBValue);
					mismatch=true;				
				}
				}}catch(Exception e) {
					ExtentTestManager.getTest().log(Status.INFO,"EXPECTED "+expectedkey.toUpperCase()+": IS NULL");
					
				}}}
			
		return mismatch;
	}
	/*
	  Function Name-validateChannelDetailResult 
	  Purpose- Validate Programme Details for celebrity API.
	 */
	public boolean validateChannelDetailResult( Object expected, Object actual) 

	{
		boolean mismatch = false; 
		JSONObject expectedChannel = (JSONObject) expected;
		JSONObject actualChannel = (JSONObject) actual;
		for (String expectedkey : expectedChannel.keySet()) {
			if (actualChannel.has(expectedkey)) {
				JSONArray expectedChannels = expectedChannel.getJSONArray(expectedkey);
				JSONArray actualChannels = actualChannel.getJSONArray(expectedkey);
				int matchCount = 0;
				for (int i = 0; i < expectedChannels.length(); i++) {
					for (int j = 0; j < actualChannels.length(); j++) {
						
						if (expectedChannels.getJSONObject(i).getString("programmeid").equals(actualChannels.getJSONObject(j).getString("programmeId"))) {
							if(expectedChannels.getJSONObject(i).getString("programmename").equals(actualChannels.getJSONObject(j).getString("Programmename"))) {
								if(!expectedChannels.getJSONObject(i).isNull("releasedate"))
								{
									System.out.println(expectedChannels.getJSONObject(i).get("releasedate"));
								if(expectedChannels.getJSONObject(i).getString("releasedate").equals(actualChannels.getJSONObject(j).getString("releasedate"))) {
								}
								}
								if(expectedChannels.getJSONObject(i).getString("rolename").equals(actualChannels.getJSONObject(j).getString("RoleName"))) {
										if(expectedChannels.getJSONObject(i).getString("priority").equals(actualChannels.getJSONObject(j).getString("Priority"))) {
											ExtentTestManager.getTest().log(Status.PASS,"Expected Response :" +expectedChannels.getJSONObject(i)+" Actual Response : "+actualChannels.getJSONObject(j));
											matchCount++;
											break;
										}	
									}	
								}
							}
						else {
							System.out.println("Mis-Matched");
							}
						}
				}
				if (expectedChannels.length() == matchCount ) {
					ExtentTestManager.getTest().log(Status.PASS,"TOTAL PROGRAM IN PROGRAM LIST IS/ARE " + expectedChannels.length() + " AS EXPECTED");
				} else {
					mismatch = true;
					ExtentTestManager.getTest().log(Status.ERROR,"TOTAL PROGRAM IN PROGRAM LIST IS/ARE " + expectedChannels.length() + " AS EXPECTED");
				}
			}
			else if(expectedkey.contains("totalrecordcount")){
				int totalrecordcount= (Integer) expectedChannel.get(expectedkey);
				int recordcount=DatabaseConnection.map.get("RowCount");
				  if(totalrecordcount==recordcount) {
				  ExtentTestManager.getTest().log(Status.PASS,"Expected Count :"+DatabaseConnection.map.get("RowCount")+" Actual Count : "+totalrecordcount); break; 
				  } else {
				  ExtentTestManager.getTest().log(Status.FAIL,"Expected Count :"+DatabaseConnection.map.get("RowCount")+" Actual Count : "+totalrecordcount); Assert.fail(); }
				 
			}
			else if(expectedkey.contains("castname") || expectedkey.contains("castid") || expectedkey.contains("height") || expectedkey.contains("dob") || expectedkey.contains("pagerecordcount") ){
				ExtentTestManager.getTest().log(Status.INFO,"CAST DETAILS ARE NOT VALIDATED");
			}
		}
		return mismatch;
	}
	/*
	  Function Name-PageSize_NumberValidation 
	  Purpose- Validating ProgrammRecords when Record is gretaer than Page Size 100 WRT to pageNo.
	 */
	public boolean PageSize_NumberValidation() throws IOException {
		boolean mismatch = false; 
		int matchCount=0;
		JSONArray expectedChannels;
		GetRequestMethod GetReq =new GetRequestMethod();
		EPGQueries Queries =new EPGQueries();	
		ApiKey APIKeyTAG =new ApiKey();
		KeyParameters PageNo =new KeyParameters();
		Validations valid =new Validations();
		APIKeyTAG.getEndpointValidation("CelebrityAPI");
		PageNo.InValidValuePresent(ExcelUtility.map.get("Tag"));
		Response response=GetReq.dynamicgetRequest(KeyParameters.map.get("DynamicURL")); 
		int ActualStatus= response.getStatusCode();
		String ActualCode=Integer.toString(ActualStatus);
		valid.statusCodeValidation(ActualCode,ExcelUtility.map.get("StatusCode"));
		ResponseValidation jsonOperations = new ResponseValidation();
		JSONObject channels = jsonOperations.getChannels(response.asString(),"celebritydetails");
		DatabaseConnection Connector= new DatabaseConnection();
		Queries.Celebrityprogramlist();
		String Query=EPGQueries.map.get("Query");
		Query=Query.replace("%s",ExcelUtility.map.get("CastID") );
		System.out.println(Query);
		Connector.getResult(Query);
		JSONObject db = Connector.getChannels();
		JSONObject expectedChannel = (JSONObject) channels;
		JSONObject actualChannel = (JSONObject) db;
		int totalrecordcount= (Integer) expectedChannel.get("totalrecordcount");
		int pagerecordcount = (Integer) expectedChannel.get("pagerecordcount");
		int pagerecordcountIncreement=pagerecordcount;
		if(totalrecordcount==pagerecordcount) {
			validateChannelDetailResult(channels, db);
		}
		else {
			for (String expectedkey : expectedChannel.keySet()) {
				if (actualChannel.has(expectedkey)) {
					expectedChannels = expectedChannel.getJSONArray(expectedkey);
					System.out.println(expectedChannels);
					JSONArray actualChannels = actualChannel.getJSONArray(expectedkey);
					int PageNumber=1;
					int i=0;
					for (int j = 0; j < actualChannels.length();j++) {
					for (i = 0; i < pagerecordcount;i++) {
							if (expectedChannels.getJSONObject(i).getString("programmeid").equals(actualChannels.getJSONObject(j).getString("programmeId"))) 
							{
								if(expectedChannels.getJSONObject(i).getString("programmename").equals(actualChannels.getJSONObject(j).getString("Programmename"))) {
									if(!expectedChannels.getJSONObject(i).isNull("releasedate"))
									{
										System.out.println(expectedChannels.getJSONObject(i).get("releasedate"));
									if(expectedChannels.getJSONObject(i).getString("releasedate").equals(actualChannels.getJSONObject(j).getString("releasedate"))) {
									}}
									if(expectedChannels.getJSONObject(i).getString("rolename").equals(actualChannels.getJSONObject(j).getString("RoleName"))) {
											if(expectedChannels.getJSONObject(i).getString("priority").equals(actualChannels.getJSONObject(j).getString("Priority"))) {
												ExtentTestManager.getTest().log(Status.PASS,"Expected Response :" +expectedChannels.getJSONObject(i)+" Actual Response : "+actualChannels.getJSONObject(j));
												matchCount++;
												break;
											}}}}	
							else {
								//System.out.println("Value Of i: "+i);
								//System.out.println("Value Of j: "+j);
								}
							while(matchCount >= pagerecordcount) {
								if(totalrecordcount==pagerecordcountIncreement) {
									ExtentTestManager.getTest().log(Status.PASS,"All "+totalrecordcount+" RECORDS ARE VALIDATED");
									break;
								}
								else {
								PageNumber++;
								PageNo.PageSize_Number(PageNumber);
								response=GetReq.dynamicgetRequest(KeyParameters.map.get("DynamicURL"));
								channels = jsonOperations.getChannels(response.asString(),"celebritydetails");
								expectedChannel = (JSONObject) channels;
								expectedChannels = expectedChannel.getJSONArray(expectedkey);
								pagerecordcount = (Integer) expectedChannel.get("pagerecordcount");
								pagerecordcountIncreement=pagerecordcount+pagerecordcountIncreement;
								matchCount=0;
								i=-1;
								continue;
								}
							}}
					}}
				while(matchCount >= pagerecordcount) {
					if(totalrecordcount==pagerecordcountIncreement) {
						ExtentTestManager.getTest().log(Status.PASS,"All "+totalrecordcount+" RECORDS ARE VALIDATED");
						matchCount =0;
						break;
					}
					else {
						ExtentTestManager.getTest().log(Status.FAIL,"All "+totalrecordcount+" RECORDS ARE VALIDATED");
						}
					}
				if(expectedkey.contains("totalrecordcount")){
					int totalrecord= (Integer) expectedChannel.get(expectedkey);
					System.out.println(totalrecord);
					int recordcount=DatabaseConnection.map.get("RowCount");
					System.out.print(recordcount);
					  if(totalrecord==recordcount) {
					  ExtentTestManager.getTest().log(Status.PASS,"Expected Count :"+DatabaseConnection.map.get("RowCount")+" Actual Count : "+totalrecordcount); break; 
					  } else {
					  ExtentTestManager.getTest().log(Status.FAIL,"Expected Count :"+DatabaseConnection.map.get("RowCount")+" Actual Count : "+totalrecordcount); Assert.fail(); }
					 
				}
				else if(expectedkey.contains("castname") || expectedkey.contains("castid") || expectedkey.contains("height") || expectedkey.contains("dob") || expectedkey.contains("pagerecordcount") ){
					ExtentTestManager.getTest().log(Status.INFO,"CAST DETAILS ARE NOT VALIDATED IN THIS TESTCASE");
				}
			}
		}
		return mismatch;
		
	}
	public boolean PageSize_CatalogNumberValidation() throws IOException {
		boolean mismatch = false; 
		int matchCount=0;
		JSONArray expectedChannels;
		JSONObject programminfo;
		GetRequestMethod GetReq =new GetRequestMethod();
		VODQueries Queries =new VODQueries();	
		ApiKey APIKeyTAG =new ApiKey();
		KeyParameters PageNo =new KeyParameters();
		Validations valid =new Validations();
		APIKeyTAG.getEndpointValidation("Catalog_User");
		PageNo.InValidValuePresent(ExcelUtility.map.get("Tag"));
		Response response=GetReq.dynamicgetRequest(KeyParameters.map.get("DynamicURL"));
		int ActualStatus= response.getStatusCode();
		String ActualCode=Integer.toString(ActualStatus);
		valid.statusCodeValidation(ActualCode,ExcelUtility.map.get("StatusCode"));
		//valid.schemaValidation(response.asString(), JsonSchemaValidator.matchesJsonSchemaInClasspath("Schema/CatalogUserSpecific.json"));		
		ResponseValidation jsonOperations = new ResponseValidation(); 
		JSONObject channels =jsonOperations.getChannels(response.asString(),"programmeslist"); 
		DatabaseConnection Connector= new DatabaseConnection();
		Queries.Catalogprogramlist();
		String Query=VODQueries.map.get("Query");
		Query=Query.replace("%s",ExcelUtility.map.get("ApprovedStatus"));
		Query=Query.replace("%f",ExcelUtility.map.get("OperatorID"));
		Query=Query.replace("%a",ExcelUtility.map.get("ContentType"));
		System.out.println(Query);
		Connector.getResult(Query);
		JSONObject db = Connector.getChannelsCatalog();
		JSONObject expectedChannel = (JSONObject) channels;
		JSONObject actualChannel = (JSONObject) db;
		int PageNumber=1;
		int pagerecordcount = 0,pagerecordcountIncreement=0;
		int totalrecordcount= (Integer) expectedChannel.get("totalrecordcount");
		//-
		for (String expectedkey : expectedChannel.keySet()) {
		if(expectedkey.contains("platforms")){
			expectedChannels = expectedChannel.getJSONArray(expectedkey);
			pagerecordcount=expectedChannels.getJSONObject(0).getInt("operatortotalrecordcount");
			System.out.println(pagerecordcount);
			pagerecordcountIncreement=totalrecordcount;
			int recordcount=DatabaseConnection.map.get("RowCount");
			System.out.print(recordcount);
			  if(pagerecordcount==recordcount) {
			  ExtentTestManager.getTest().log(Status.PASS,"Expected Count :"+DatabaseConnection.map.get("RowCount")+" Actual Count : "+pagerecordcount); 
			  expectedChannels=expectedChannels.getJSONObject(0).getJSONArray("results");
			  //System.out.println(expectedChannels);
			  } else {
			  ExtentTestManager.getTest().log(Status.FAIL,"Expected Count :"+DatabaseConnection.map.get("RowCount")+" Actual Count : "+totalrecordcount);
			  Assert.fail(); 
			  } 
			  int i=0;
			  JSONArray actualChannels = actualChannel.getJSONArray("programinfo");
			  for (int j = 0; j < actualChannels.length();j++) {
			  for (i = 0; i < totalrecordcount;i++) {
			  programminfo=expectedChannels.getJSONObject(i).getJSONObject("programinfo");
					//System.out.println(expectedChannels);
					//int PageNumber=1;
						if (programminfo.get("platformprogrammeid").equals(actualChannels.getJSONObject(j).getString("ProgrammeId"))) 
						{
							System.out.println(programminfo.get("platformprogrammeid"));
							System.out.println(actualChannels.getJSONObject(j).getString("ProgrammeId"));
						if(programminfo.getString("platformprogrammename").equals(actualChannels.getJSONObject(j).getString("ProgrammeName"))) 
							if(!programminfo.isNull("programmeid"))
							{	
						{
							if(programminfo.getString("programmeid").equals(actualChannels.getJSONObject(j).getString("CMSProgrammeID"))) 
							{
							}
							}
							if(programminfo.getString("programmelanguage").equals(actualChannels.getJSONObject(j).getString("ProgrammeLanguage"))) 
								{
								if(programminfo.getString("synopsis").equals(actualChannels.getJSONObject(j).getString("Synopsis"))) 
									{
									if(programminfo.getString("programmegenre").equals(actualChannels.getJSONObject(j).getString("ProgrammeGenre"))) 
										{
										if(!programminfo.isNull("cast"))
										{
										if(programminfo.getString("cast").equals(actualChannels.getJSONObject(j).getString("Cast")))
										{
											
										}}
										if(!programminfo.isNull("platformseasonid"))
										{
										if(programminfo.getString("platformseasonid").equals(actualChannels.getJSONObject(j).getString("SourceSeasonid")))
										{
											
										}}
										if(!programminfo.isNull("platformepisodeid"))
										{
										if(programminfo.getString("platformepisodeid").equals(actualChannels.getJSONObject(j).getString("SourceEpisodeid")))
										{
											
										}}
										
											if(programminfo.getInt("platformproductionyear")==(actualChannels.getJSONObject(j).getInt("ProductionYear")))
												{
												if(programminfo.get("licensestartdate").equals(actualChannels.getJSONObject(j).getString("StartDate")))
													{
													if(programminfo.get("licenseenddate").equals(actualChannels.getJSONObject(j).getString("EndDate")))
														{
														ExtentTestManager.getTest().log(Status.PASS,"METADATA VALIDATED FROM PROGRAMMEID: "+programminfo.get("platformprogrammeid"));
														matchCount++;
														break;								
														}
													}
												}
										}	
								}
							}
						}
						else {
							ExtentTestManager.getTest().log(Status.WARNING,"METADATA FOR PROGRAMMEID: "+programminfo.get("platformprogrammeid")+" IS NOT SAME");
							
						}
						}
							else {
								System.out.println("Not Matched");
								System.out.println("Value Of i: "+i);
								System.out.println("Value Of j: "+j);
								}
							while(matchCount >= totalrecordcount) {
								if(pagerecordcount==pagerecordcountIncreement) {
									ExtentTestManager.getTest().log(Status.PASS,"All "+pagerecordcount+" RECORDS ARE VALIDATED");
									break;
								}
								else {
								PageNumber++;
								PageNo.PageSize_Number(PageNumber);
								response=GetReq.dynamicgetRequest(KeyParameters.map.get("DynamicURL"));
								channels = jsonOperations.getChannels(response.asString(),"programmeslist");
								expectedChannel = (JSONObject) channels;
								expectedChannels = expectedChannel.getJSONArray("platforms");
								totalrecordcount= (Integer) expectedChannel.get("totalrecordcount");
								//pagerecordcount = (Integer) expectedChannel.get("totalrecordcount");
								expectedChannels=expectedChannels.getJSONObject(0).getJSONArray("results");
								 actualChannels = actualChannel.getJSONArray("programinfo");
								  
								//expectedChannels = expectedChannel.getJSONArray(expectedkey);
								pagerecordcountIncreement=totalrecordcount+pagerecordcountIncreement;
								matchCount=0;
								i=-1;
								continue;
								}
							}
							
					//}
					
					}}}
					else if(expectedkey.contains("totalrecordcount")) {
						ExtentTestManager.getTest().log(Status.INFO,"TOTAL RECORDS PRESENR IS/ARE: "+totalrecordcount);
					}
				while(matchCount >= totalrecordcount) {
					if(pagerecordcount==pagerecordcountIncreement) {
						ExtentTestManager.getTest().log(Status.PASS,"All "+pagerecordcount+" RECORDS ARE VALIDATED");
							matchCount =0;
						break;
					}
					else if(pagerecordcount==0){
						break;
					}
					else {
						ExtentTestManager.getTest().log(Status.FAIL,"All "+totalrecordcount+" RECORDS ARE VALIDATED");
						}
					}
		}
		return mismatch;
		
	}
	public boolean validateCatalogDetailResult( Object expected, Object actual) 

	{
		boolean mismatch = false; 
		JSONObject expectedChannel = (JSONObject) expected;
		JSONObject actualChannel = (JSONObject) actual;
		for (String expectedkey : expectedChannel.keySet()) {
			if (actualChannel.has(expectedkey)) {
				JSONArray expectedChannels = expectedChannel.getJSONArray(expectedkey);
				JSONArray actualChannels = actualChannel.getJSONArray(expectedkey);
				int matchCount = 0;
				for (int i = 0; i < expectedChannels.length(); i++) {
					for (int j = 0; j < actualChannels.length(); j++) {
						
						if (expectedChannels.getJSONObject(i).getString("programmeid").equals(actualChannels.getJSONObject(j).getString("programmeId"))) {
							if(expectedChannels.getJSONObject(i).getString("programmename").equals(actualChannels.getJSONObject(j).getString("Programmename"))) {
								if(!expectedChannels.getJSONObject(i).isNull("releasedate"))
								{
									System.out.println(expectedChannels.getJSONObject(i).get("releasedate"));
								if(expectedChannels.getJSONObject(i).getString("releasedate").equals(actualChannels.getJSONObject(j).getString("releasedate"))) {
								}
								}
								if(expectedChannels.getJSONObject(i).getString("rolename").equals(actualChannels.getJSONObject(j).getString("RoleName"))) {
										if(expectedChannels.getJSONObject(i).getString("priority").equals(actualChannels.getJSONObject(j).getString("Priority"))) {
											ExtentTestManager.getTest().log(Status.PASS,"Expected Response :" +expectedChannels.getJSONObject(i)+" Actual Response : "+actualChannels.getJSONObject(j));
											matchCount++;
											break;
										}	
									}	
								}
							}
						else {
							System.out.println("Mis-Matched");
							}
						}
				}
				if (expectedChannels.length() == matchCount ) {
					ExtentTestManager.getTest().log(Status.PASS,"TOTAL PROGRAM IN PROGRAM LIST IS/ARE " + expectedChannels.length() + " AS EXPECTED");
				} else {
					mismatch = true;
					ExtentTestManager.getTest().log(Status.ERROR,"TOTAL PROGRAM IN PROGRAM LIST IS/ARE " + expectedChannels.length() + " AS EXPECTED");
				}
			}
			else if(expectedkey.contains("totalrecordcount")){
				int totalrecordcount= (Integer) expectedChannel.get(expectedkey);
				int recordcount=DatabaseConnection.map.get("RowCount");
				  if(totalrecordcount==recordcount) {
				  ExtentTestManager.getTest().log(Status.PASS,"Expected Count :"+DatabaseConnection.map.get("RowCount")+" Actual Count : "+totalrecordcount); break; 
				  } else {
				  ExtentTestManager.getTest().log(Status.FAIL,"Expected Count :"+DatabaseConnection.map.get("RowCount")+" Actual Count : "+totalrecordcount); Assert.fail(); }
				 
			}
			else if(expectedkey.contains("castname") || expectedkey.contains("castid") || expectedkey.contains("height") || expectedkey.contains("dob") || expectedkey.contains("pagerecordcount") ){
				ExtentTestManager.getTest().log(Status.INFO,"CAST DETAILS ARE NOT VALIDATED");
			}
		}
		return mismatch;
	}
	public boolean validateChannelDetailResults( Object expected, Object actual) 

	{
		boolean mismatch = false; 
		JSONObject expectedChannel = (JSONObject) expected;
		JSONObject actualChannel = (JSONObject) actual;
		for (String expectedkey : expectedChannel.keySet()) {
			if (actualChannel.has(expectedkey)) {
				JSONArray expectedChannels = expectedChannel.getJSONArray(expectedkey);
				JSONArray actualChannels = actualChannel.getJSONArray(expectedkey);
				if(expectedChannels.length()!=actualChannels.length()) {	
					ExtentTestManager.getTest().log(Status.FAIL,"RECORDS OF PROGRAMS FOR UAT "+expectedChannels.length()+" RECORDS OF PROGRAMS FOR LIVE "+actualChannels.length());
					ExtentTestManager.getTest().log(Status.INFO,"SKIPPING THE COMPARE JSON, SINCE RECORD COUNT IS NOT SAME");
					mismatch = true;
				}else {
					ExtentTestManager.getTest().log(Status.PASS,"RECORDS FOR PROGRAMS ARE SAME IN UAT AND LIVE");
				int matchCount = 0;
				for (int i = 0; i < expectedChannels.length(); i++) {
					for (int j = 0; j < actualChannels.length(); j++) {
						
						System.out.println(expectedChannels.getJSONObject(i).getString("programid"));
						System.out.println(actualChannels.getJSONObject(j).getString("programid"));
						
						if (expectedChannels.getJSONObject(i).getString("programid").equals(actualChannels.getJSONObject(j).getString("programid"))) {
						if(expectedChannels.getJSONObject(i).getString("programmename").equals(actualChannels.getJSONObject(j).getString("programmename")))
						{
						if(expectedChannels.getJSONObject(i).getString("starttime").equals(actualChannels.getJSONObject(j).getString("starttime"))){
						if(expectedChannels.getJSONObject(i).getString("channelname").equals(actualChannels.getJSONObject(j).getString("channelname"))){
						if(expectedChannels.getJSONObject(i).getString("endtime").equals(actualChannels.getJSONObject(j).getString("endtime"))){
							ExtentTestManager.getTest().log(Status.PASS,"Expected PROGRAMID :" +expectedChannels.getJSONObject(i).getString("programid")+" Actual PROGRAMID : "+actualChannels.getJSONObject(j).getString("programid"));
							matchCount++;
							break;
						}	
						}	
						}	
							}
							//matchCount++;
							//break;
						}
						else if (actualChannels.getJSONObject(j).getString("programid").contains(expectedChannels.getJSONObject(i).getString("programid"))) {
							matchCount++;
							break;
						}
					}
				}
				
				System.out.println(expectedChannels.length());
				if (expectedChannels.length() == matchCount) {
					System.out.println("Expected Count: "+ expectedChannels.length() +"MatchCount: "+matchCount);
					//log.info("Channel Count '" + expectedkey + "' are as Expected");
				} else {
					mismatch = true;
					System.out.println();
					//log.error("Channel Count '" + expectedkey + "'");
				}
			}}
			else {
				System.out.println("Expected Count: "+ expectedkey);
				//log.error("Channel not '" + expectedkey + "'not found!");
				mismatch = true;
				break;
			}
		}
		return mismatch;
	}
	
	public void LevelDataValidation() throws IOException {
		//JSONObject programminfo;
		ApiKey APIKeyTAG =new ApiKey();
		KeyParameters PageNo =new KeyParameters();
		GetRequestMethod GetReq =new GetRequestMethod();
		Validations valid =new Validations();
		//JSONArray expectedChannels = null;
		APIKeyTAG.getEndpointValidation("Catalog_User");
		PageNo.InValidValuePresent(ExcelUtility.map.get("Tag"));
		Response response=GetReq.dynamicgetRequest(KeyParameters.map.get("DynamicURL"));
		int ActualStatus= response.getStatusCode();
		String ActualCode=Integer.toString(ActualStatus);
		valid.statusCodeValidation(ActualCode,ExcelUtility.map.get("StatusCode"));
		
		
	}
	
	public void EpisodeLevelDataValidation() throws IOException {
		JSONObject programminfo;
		ApiKey APIKeyTAG =new ApiKey();
		KeyParameters PageNo =new KeyParameters();
		GetRequestMethod GetReq =new GetRequestMethod();
		Validations valid =new Validations();
		JSONArray expectedChannels = null;
		APIKeyTAG.getEndpointValidation("Catalog_User");
		PageNo.InValidValuePresent(ExcelUtility.map.get("Tag"));
		Response response=GetReq.dynamicgetRequest(KeyParameters.map.get("DynamicURL"));
		int ActualStatus= response.getStatusCode();
		String ActualCode=Integer.toString(ActualStatus);
		valid.statusCodeValidation(ActualCode,ExcelUtility.map.get("StatusCode"));
		ResponseValidation jsonOperations = new ResponseValidation(); 
		JSONObject channels =jsonOperations.getChannels(response.asString(),"programmeslist"); 
		VODQueries Queries =new VODQueries();	
		DatabaseConnection Connector= new DatabaseConnection();
		JSONObject expectedChannel = (JSONObject) channels;
		for (String expectedkey : expectedChannel.keySet()) {
		if(expectedkey.contains("platforms")){
			  for (int i = 0; i < expectedChannel.length();i++) {
			  programminfo=expectedChannels.getJSONObject(i).getJSONObject("programinfo");			
		Queries.EpisodeLevel();
		String Query=VODQueries.map.get("Query");
		int CountEpisode=0,CountProgram=0;//CountSeason=0;
		//JSONObject db = Connector.getEpisodeDetails();
		//JSONObject actualChannel = (JSONObject) db;
		//JSONArray actualChannels = actualChannel.getJSONArray("programinfo");
		if(!programminfo.isNull("platformepisodeid") && CountEpisode==0) {
			String EpisodeID=(String) programminfo.get("platformepisodeid");
			Query=Query.replace("%s",EpisodeID);
			System.out.println(Query);
			Connector.getResult(Query);	
						{
			expectedChannels = expectedChannel.getJSONArray(expectedkey);
			CountEpisode++;
						}
		}
		else if(!programminfo.isNull("platformepisodeid") && CountProgram==0) {
			String EpisodeID=(String) programminfo.get("platformepisodeid");
			Query=Query.replace("%s",EpisodeID);
			System.out.println(Query);
			Connector.getResult(Query);	
						{
			expectedChannels = expectedChannel.getJSONArray(expectedkey);
			CountProgram++;
						}
		}
		}
	}
		}
}
	
	/*
	  Function Name-MetadataValidation 
	  Purpose- Validate OVD Metadata along with EnableHierarchy .
	 */
	
	public void MetadataValidation() throws IOException {
		JSONArray expectedChannels;
		JSONObject programminfo;//expectedChannelsTemp;
		int IDCount=0;
		ApiKey APIKeyTAG =new ApiKey();
	  GetRequestMethod GetReq =new GetRequestMethod();
	  KeyParameters KParameters =new KeyParameters();
	  Validations valid =new Validations();
	  EPGQueries Queries =new EPGQueries();
	  ResponseValidation jsonOperations = new ResponseValidation(); 
	  
		APIKeyTAG.getEndpointValidation("OVDStreaming");
		KParameters.InValidValuePresent(ExcelUtility.map.get("Tag"));
		Response response=GetReq.dynamicgetRequest(KeyParameters.map.get("DynamicURL")); 
		int ActualStatus= response.getStatusCode();
		String ActualCode=Integer.toString(ActualStatus);
		valid.statusCodeValidation(ActualCode,ExcelUtility.map.get("StatusCode"));
		
		JSONObject channels =jsonOperations.getChannels(response.asString(),"programmeslist"); 
		DatabaseConnection Connector= new DatabaseConnection();
		Queries.APIRegistration_Metadata();
		String Query=EPGQueries.map.get("SpecificAPI_Metadata");
		Query=Query.replace("%s",ExcelUtility.map.get("InvalidValue"));
		System.out.println(Query);
		Connector.getResult(Query);
		JSONObject db = Connector.getMetadata();
		JSONObject expectedChannel = (JSONObject) channels;
		JSONObject actualChannel = (JSONObject) db;
		JSONArray actualChannels = actualChannel.getJSONArray("programmeslist");
		
		String AssignedBucket=actualChannels.getJSONObject(0).getString("AssignedBucket");
		System.out.print(AssignedBucket.length());
		if(AssignedBucket.length()==5 && AssignedBucket.contains("5")) {
			ExtentTestManager.getTest().log(Status.INFO,"ASSIGNED BUCKET IS : "+AssignedBucket+" ,HENCE COMES UNDER PUBLISHED");   
			  
		}
		else if(AssignedBucket.length()==21 && AssignedBucket.contains("0")) {
			ExtentTestManager.getTest().log(Status.INFO,"ASSIGNED BUCKET IS : "+AssignedBucket+" ,HENCE COMES UNDER All(UNMAPPED+MAPPED+PUBLISHED)");   
			
		}
		else {
			ExtentTestManager.getTest().log(Status.INFO,"ASSIGNED BUCKET IS : "+AssignedBucket+" ,HENCE COMES UNDER MAPPED+PUBLISHED");   
			
		}
		 int totalcount= (Integer) expectedChannel.get("changeoffset");
		for (String expectedkey : expectedChannel.keySet()) {
			if(expectedkey.contains("results")){
				expectedChannels = expectedChannel.getJSONArray(expectedkey);
				ExtentTestManager.getTest().log(Status.INFO,"TOTAL METADATA PRESENT IN DATABASE: "+actualChannels.length()); 
				JSONObject Responseobject= expectedChannels.getJSONObject(0);
			 //int count=0;
			 String Metadata = null;
			  for (int j = 0; j < actualChannels.length();j++) {
				  int count=0;
				  Metadata=actualChannels.getJSONObject(j).getString("MetaDataName");
				  System.out.println(Metadata);
				  for(Object key :Responseobject.keySet()) {
				  System.out.println(key);
				  if(key.toString().contains(Metadata.toLowerCase())) {
						  ExtentTestManager.getTest().log(Status.PASS,"METADATA: "+Metadata+" IS PRESENT IN RESPONSE.");
						  Responseobject.remove((String) key);
						  count ++;
						  break;
					  }
					 else if((Metadata.toLowerCase().contains("seasonid")&&key.toString().contains("seasonid")) 
							 ||(Metadata.toLowerCase().contains("episodeid")&&key.toString().contains("episodeid"))
							 ||(Metadata.equalsIgnoreCase("Commercials")&&key.toString().contains("commercial"))
							 ||(Metadata.equalsIgnoreCase("CommercialModel")&&key.toString().contains("commercial"))) {
						 ExtentTestManager.getTest().log(Status.PASS,"METADATA: "+Metadata+" IS PRESENT IN RESPONSE.");
						 count++;
						 Responseobject.remove((String) key);
						break;
					 }
				 
				  }
				  if(count==0) {
						 ExtentTestManager.getTest().log(Status.FAIL,"METADATA: "+Metadata+" IS NOT PRESENT IN RESPONSE.");
					 }
			}
			  if(!Responseobject.isEmpty()) {
				  for(Object key :Responseobject.keySet()) {
			  ExtentTestManager.getTest().log(Status.FAIL,"METADATA "+key+" IS NOT PRESENT IN DATABASE.");
				  }
			  }
			  String EnabledHierarchy=actualChannels.getJSONObject(0).getString("EnabledHierarchy");
				if(EnabledHierarchy.equalsIgnoreCase("0")) {
					ExtentTestManager.getTest().log(Status.INFO,"ENABLED-HIERARCHY IS : "+AssignedBucket+" ,HENCE SEASON AND EPISODE ID VALUE WILL BE NULL");
					for(int i=0;i<totalcount;i++) {
					programminfo=expectedChannels.getJSONObject(i);
					//String platformseasonid =programminfo.getString("platformseasonid");
					//System.out.println(platformseasonid);
				if(!(programminfo.isNull("platformseasonid") || programminfo.isNull("platformepisodeid"))) {
						ExtentTestManager.getTest().log(Status.FAIL,"VALUE IS NOT NULL,platformseasonid : "+programminfo.getString("platformseasonid")+" platformepisodeid : "+programminfo.getString("platformepisodeid"));
					}
					else {
							IDCount++;
						 }
					}
					if(IDCount==totalcount) {
						ExtentTestManager.getTest().log(Status.PASS,"VALUES ARE NULL");
					}
				}
				else {
						ExtentTestManager.getTest().log(Status.INFO,"ENABLED-HIERARCHY IS : "+AssignedBucket+" ,HENCE SEASON AND EPISODE ID VALUE MAY OR MAYNOT BE NULL");   
						for(int i=0;i<totalcount;i++) {
							programminfo=expectedChannels.getJSONObject(i);
							//String platformseasonid =programminfo.getString("platformseasonid");
							//System.out.println(platformseasonid);
						if(!(programminfo.isNull("platformseasonid") || programminfo.isNull("platformepisodeid"))) {
								ExtentTestManager.getTest().log(Status.PASS,"VALUE IS NOT NULL,platformseasonid : "+programminfo.getString("platformseasonid")+" platformepisodeid : "+programminfo.getString("platformepisodeid"));
							}
							else {
								ExtentTestManager.getTest().log(Status.PASS,"VALUE IS NULL,platformseasonid : "+programminfo.getString("platformseasonid")+" platformepisodeid : "+programminfo.getString("platformepisodeid"));
								 }
							}
				}
			  
			}
		}
	}

	
      public void MaxOffSet_Limit() throws IOException {
    	  ApiKey APIKeyTAG =new ApiKey();
		  GetRequestMethod GetReq =new GetRequestMethod();
		  KeyParameters KParameters =new KeyParameters();
		  Validations valid =new Validations();
		  ResponseValidation jsonOperations = new ResponseValidation(); 
			APIKeyTAG.getEndpointValidation("HeadEndID_OVDStreaming");
			//KParameters.InValidValuePresent(ExcelUtility.map.get("Tag"));
			Response response=GetReq.dynamicgetRequest(ApiKey.map.get("EndpointURL")); 
			int ActualStatus= response.getStatusCode();
			String ActualCode=Integer.toString(ActualStatus);
			valid.statusCodeValidation(ActualCode,ExcelUtility.map.get("StatusCode"));
			JSONObject channels =jsonOperations.getChannels(response.asString(),"programmeslist"); 
			JSONObject expectedChannel = (JSONObject) channels;
			Integer totalcount= (Integer) expectedChannel.get("maxchangeoffset");
			totalcount=totalcount+1;
			map.put("maxchangeoffset", totalcount);
			KParameters.MaxOffSet(ExcelUtility.map.get("Tag"));
			response=GetReq.dynamicgetRequest(ApiKey.map.get("EndpointURL")); 
			valid.schemaValidation(response.asString(), JsonSchemaValidator.matchesJsonSchemaInClasspath("Schema/OVDStreaming.json"));		
      }
	
      /*
	  Function Name-MetadataValidation 
	  Purpose- Validate OVD Metadata along with EnableHierarchy .
	 */
	
	public void OVDResponseValidation() throws IOException {
		JSONArray expectedChannels;
		JSONObject programminfo;//expectedChannelsTemp;
		ApiKey APIKeyTAG =new ApiKey();
		GetRequestMethod GetReq =new GetRequestMethod();
		KeyParameters KParameters =new KeyParameters();
		Validations valid =new Validations();
		VODQueries Queries =new VODQueries();
	  	ResponseValidation jsonOperations = new ResponseValidation(); 
		APIKeyTAG.getEndpointValidation("OVDStreaming");
		KParameters.OVD_MultipleValue(ExcelUtility.map.get("Tag"));
		Response response=GetReq.dynamicgetRequest(KeyParameters.map.get("DynamicURL")); 
		int ActualStatus= response.getStatusCode();
		String ActualCode=Integer.toString(ActualStatus);
		valid.statusCodeValidation(ActualCode,ExcelUtility.map.get("StatusCode"));
		JSONObject channels =jsonOperations.getChannels(response.asString(),"programmeslist"); 
		DatabaseConnection Connector= new DatabaseConnection();
		Queries.USERID();
		String Query=VODQueries.map.get("USERID");
		Query=Query.replace("%s",ExcelUtility.map.get("InvalidValue"));
		System.out.println(Query);
		Connector.getResult(Query);
		Connector.getUSERID();
		String USERID = DatabaseConnection.castDetails.get("UserID");
		Queries.OVDSTREAMING();
		Query=VODQueries.map.get("OVDSTREAMING");
		Query=Query.replace("%s",USERID);
		System.out.println(Query);
		Connector.getResult(Query);
		JSONObject db = Connector.OVDStreamingValue();
		JSONObject expectedChannel = (JSONObject) channels;
		JSONObject actualChannel = (JSONObject) db;
		JSONArray actualChannels = actualChannel.getJSONArray("programmeslist");
		for (String expectedkey : expectedChannel.keySet()) {
			if(expectedkey.contains("results")){
				expectedChannels = expectedChannel.getJSONArray(expectedkey);
				 for (int j = 0; j < actualChannels.length();j++) {
				programminfo=expectedChannels.getJSONObject(j);
				if (programminfo.get("platformprogrammeid").equals(actualChannels.getJSONObject(j).getString("ProgrammeId"))) 
				{
					if(programminfo.get("platformtitle").equals(actualChannels.getJSONObject(j).getString("ProgrammeName"))) 
				{
						if(programminfo.get("contenttype").equals(actualChannels.getJSONObject(j).getString("ContentType"))) 
						{
							if(programminfo.get("platformcontentimage").equals(actualChannels.getJSONObject(j).getString("ProgramImage"))) 
							{
								if(programminfo.get("platformsynopsis").equals(actualChannels.getJSONObject(j).getString("Synopsis"))) 
								{
									System.out.println(programminfo.get("productionyear"));
									System.out.println(actualChannels.getJSONObject(j).getString("ProductionYear"));
									if(programminfo.getInt("productionyear")==(actualChannels.getJSONObject(j).getInt("ProductionYear"))) 
									{
										if(programminfo.get("platformprogrammelanguage").equals(actualChannels.getJSONObject(j).getString("ProgrammeLanguage"))) 
										{
											if(programminfo.getInt("operatorid")==(actualChannels.getJSONObject(j).getInt("OperatorId"))) 
											{
												if(programminfo.get("actionflag").equals(actualChannels.getJSONObject(j).getString("UpdateAction"))) 
												{
													System.out.println(programminfo.get("platformprogrammeid"));
													System.out.println(actualChannels.getJSONObject(j).getString("ProgrammeId"));
													ExtentTestManager.getTest().log(Status.PASS, "VALIDATED FROM PROGRAMMEID: "+programminfo.get("platformprogrammeid"));
												
												}		
											}		
										}		
									}	
								}		
							}	
						}	
				}
					
				 }
				else {
					ExtentTestManager.getTest().log(Status.FAIL, "VALIDATION FROM PROGRAMMEID: "+programminfo.get("platformprogrammeid"));
					
				}
			}
		}
		
	}
		
	}  
	
	public void ContentType_OVDStreaming() throws IOException {
		JSONArray expectedChannels;
		JSONObject programminfo;
		
		 ApiKey APIKeyTAG =new ApiKey();
		  GetRequestMethod GetReq =new GetRequestMethod();
		  KeyParameters KParameters =new KeyParameters();
		  Validations valid =new Validations();
		  ResponseValidation jsonOperations = new ResponseValidation(); 
			APIKeyTAG.getEndpointValidation("OVDStreaming");
			//KParameters.InValidValuePresent(ExcelUtility.map.get("Tag"));
			Response response=GetReq.dynamicgetRequest(ApiKey.map.get("EndpointURL")); 
			int ActualStatus= response.getStatusCode();
			String ActualCode=Integer.toString(ActualStatus);
			valid.statusCodeValidation(ActualCode,ExcelUtility.map.get("StatusCode"));
			JSONObject channels =jsonOperations.getChannels(response.asString(),"programmeslist"); 
			JSONObject expectedChannel = (JSONObject) channels;
			Integer totalcount= (Integer) expectedChannel.get("changeoffset");
			Integer maxchangeoffset= (Integer) expectedChannel.get("maxchangeoffset");
			for (String expectedkey : expectedChannel.keySet()) {
				if(expectedkey.contains("results")){
					expectedChannels = expectedChannel.getJSONArray(expectedkey);
					 for (int j = 0; j < totalcount;j++) {
							programminfo=expectedChannels.getJSONObject(j);
							if (programminfo.get("platformprogrammeid")=="") {}
					 }
				}}
			if(totalcount==maxchangeoffset) 
			{
			map.put("maxchangeoffset", totalcount);
			KParameters.MaxOffSet(ExcelUtility.map.get("Tag"));
			response=GetReq.dynamicgetRequest(ApiKey.map.get("EndpointURL")); 
			}
			else {
				ExtentTestManager.getTest().log(Status.INFO, "TOTAL MAXCHANGEOFFSET :"+maxchangeoffset);
			}
		
	}
	
	public void ResponseLanguage_Validation() throws IOException {
		
		ApiKey APIKeyTAG =new ApiKey();
		GetRequestMethod GetReq =new GetRequestMethod();
		Validations valid =new Validations();
		EPGQueries Queries =new EPGQueries();
		DatabaseConnection Connector= new DatabaseConnection();
		
		String DynamicURL;
		String EndpointURL=APIKeyTAG.getEndpointValidation1("ProgrammeDetails");
		System.out.println(EndpointURL);
		String TagName=ExcelUtility.map.get("Tag");
		String TagName1=ExcelUtility.map.get("Tag1");
		String  APIKEY = null;
		
		//Database
		//Queries.LanguageAssigned_ProgrammeDetails();
		//String Query=EPGQueries.map.get("LanguageAssigned");
		String Query=Queries.LanguageAssigned_ProgrammeDetails();
		System.out.println(Query);
		Connector.getResult(Query);
		JSONObject db = Connector.getLanguageAssigned();
		JSONObject actualChannel = (JSONObject) db;
		JSONArray actualChannels = actualChannel.getJSONArray("programmedetails");
		
		for (int j = 0; j < actualChannels.length();j++) {
		APIKEY=actualChannels.getJSONObject(j).getString("APIKey");
		String  LanguageAssigned=actualChannels.getJSONObject(j).getString("LanguageAssigned");
		ExtentTestManager.getTest().log(Status.INFO, "VALIDATING FOR API: " +APIKEY+" AND LANGUAGE(S): " +LanguageAssigned);
		
		for (String expectedkey : LanguageAssigned.split(",")) {
			System.out.println(expectedkey);
			//String TempLanguage=expectedkey;
			if(expectedkey.contains("en")) {
				expectedkey="English";
			}
			else if(expectedkey.contains("hi")) {
				expectedkey="Hindi";
			}
			else if(expectedkey.contains("ta")) {
				expectedkey="Tamil";
			}
			else if(expectedkey.contains("ar")) {
				expectedkey="Arabic";
			}
		
		//EndpointURL = ApiKey.map.get("EndpointURL");
		String[] NoTag = EndpointURL.split( TagName+ "=");
		String[] NoTag1 = NoTag[1].split("&", 2);
		System.out.println(APIKEY);
		DynamicURL = NoTag[0] + TagName + "=" + APIKEY + "&" + NoTag1[1];
		System.out.println(DynamicURL);
		NoTag = DynamicURL.split(TagName1+"=");
		NoTag1 = NoTag[1].split("&", 2);
		System.out.println(expectedkey);
		DynamicURL = NoTag[0] + TagName1+"=" + expectedkey + "&" + NoTag1[1];
		//ExtentTestManager.getTest().log(Status.INFO, "DYNAMIC URL IS :" + DynamicURL);
		System.out.println(DynamicURL);
		Response response=GetReq.dynamicgetRequest(DynamicURL); 
		int ActualStatus= response.getStatusCode();
		String ActualCode=Integer.toString(ActualStatus);
		valid.statusCodeValidation(ActualCode,ExcelUtility.map.get("StatusCode"));
		
		 }
	}
		 ExtentTestManager.getTest().log(Status.INFO, "VALIDATING FOR API: " +APIKEY+" AND NEGATIVE LANGUAGE: FRENCH ");
			//EndpointURL = ApiKey.map.get("EndpointURL");
			
			String[] NoTag = EndpointURL.split( TagName+ "=");
			String[] NoTag1 = NoTag[1].split("&", 2);
			System.out.println(APIKEY);
			DynamicURL = NoTag[0] + TagName + "=" + APIKEY + "&" + NoTag1[1];
			System.out.println(DynamicURL);
			NoTag = DynamicURL.split(TagName1+"=");
			NoTag1 = NoTag[1].split("&", 2);
			DynamicURL = NoTag[0] + TagName1+"=French&" + NoTag1[1];
			//ExtentTestManager.getTest().log(Status.INFO, "DYNAMIC URL IS :" + DynamicURL);
			System.out.println(DynamicURL);
			Response response=GetReq.dynamicgetRequest(DynamicURL); 
			int ActualStatus= response.getStatusCode();
			String ActualCode=Integer.toString(ActualStatus);
			valid.statusCodeValidation(ActualCode,"400"); 
		
		
		
	}
	
public void ProgrammeID_Validation() throws IOException {
		
		ApiKey APIKeyTAG =new ApiKey();
		GetRequestMethod GetReq =new GetRequestMethod();
		Validations valid =new Validations();
		VODQueries Queries =new VODQueries();
		DatabaseConnection Connector= new DatabaseConnection(); 
		String EndpointURL=APIKeyTAG.getEndpointValidation1("ProgrammeDetails");
		String DynamicURL;
		String  ProgrammeID = null;
		String TagName=ExcelUtility.map.get("Tag");
		//Database
		String Query=Queries.PROGRAMMEID();
		System.out.println(Query);
		Connector.getResult(Query);
		JSONObject db = Connector.getProgrammeID();
		JSONObject actualChannel = (JSONObject) db;
		JSONArray actualChannels = actualChannel.getJSONArray("programmedetails");
		
		for (int j = 0; j < actualChannels.length();j++) {
		ProgrammeID=actualChannels.getJSONObject(j).getString("ProgrammeID");
		ExtentTestManager.getTest().log(Status.INFO, "VALIDATING FOR PROGRAMMEID: " +ProgrammeID+" OF RECORD " +j);
		Query=Queries.VOD_PROGRAMMEID();
		Query=Query.replace("%s", ProgrammeID);
		Connector.getResult(Query);
		int db_VOD = Connector.getProgrammeIDCount();
		System.out.println(db_VOD);
		if(db_VOD==0) {
			
			String[] NoTag = EndpointURL.split(TagName+"=");
			String[] NoTag1= NoTag[1].split("&",2);
			DynamicURL=NoTag[0]+TagName+"="+ProgrammeID+"&"+NoTag1[1];
			Response response=GetReq.dynamicgetRequest(DynamicURL);
			 ErrorCelebrityResponse errorResponse=response.as(ErrorCelebrityResponse.class);
			 int ActualStatus= response.getStatusCode();
				String ActualCode=Integer.toString(ActualStatus);
			//APIKeyTAG.invalidAPIKeyValue(ExcelUtility.map.get("Tag"),"ProgrammeDetails");
			//APIKeyTAG.toFetchResponseAttributesCelebrityAPI();
			valid.schemaValidation(response.asString(), JsonSchemaValidator.matchesJsonSchemaInClasspath("Schema/ErrorCelebrityAPI.json"));		
			valid.statusCodeValidation(ActualCode,ExcelUtility.map.get("StatusCode"));
			valid.ExpectionID_Validation(errorResponse.getExceptiondetails().getExceptionmessage(),ExcelUtility.map.get("ExpectedID"));
			valid.ExpectionDescription_Validation(errorResponse.getExceptiondetails().getDescription(),ExcelUtility.map.get("ExpectedDescription"));
		}
		else {
			String[] NoTag = EndpointURL.split(TagName+"=");
			String[] NoTag1= NoTag[1].split("&",2);
			DynamicURL=NoTag[0]+TagName+"="+ProgrammeID+"&"+NoTag1[1];
			Response response=GetReq.dynamicgetRequest(DynamicURL);
			int ActualStatus= response.getStatusCode();
			String ActualCode=Integer.toString(ActualStatus);
			valid.statusCodeValidation(ActualCode,"200");
			valid.schemaValidation(response.asString(), JsonSchemaValidator.matchesJsonSchemaInClasspath("Schema/ProgrammeDetails.json"));		
		
			
		}
		}
		
	
}
	public void Manadate_DataValidation() throws IOException {
	
	ApiKey APIKeyTAG =new ApiKey();
	GetRequestMethod GetReq =new GetRequestMethod();
	Validations valid =new Validations();
	VODQueries Queries =new VODQueries();
	DatabaseConnection Connector= new DatabaseConnection(); 
	ResponseValidation jsonOperations = new ResponseValidation(); 
	String EndpointURL=APIKeyTAG.getEndpointValidation1("ProgrammeDetails");
	String DynamicURL;
	String  ProgrammeID = null;
	  JSONArray expectedChannels;
	  JSONObject programminfo;
	String TagName=ExcelUtility.map.get("Tag");
	//Database
	String Query=Queries.MANDATE_DATA_VALIDATION();
	System.out.println(Query);
	Connector.getResult(Query);
	JSONObject db = Connector.getMandateProgDetailsFields();
	JSONObject actualChannel = (JSONObject) db;
	JSONArray actualChannels = actualChannel.getJSONArray("programmedetails");
	for (int j = 0; j < actualChannels.length();j++) {
	ProgrammeID=actualChannels.getJSONObject(j).getString("CMSProgrammeID");
		ExtentTestManager.getTest().log(Status.INFO, "VALIDATING FOR PROGRAMMEID: " +ProgrammeID+" OF RECORD " +j);
		String[] NoTag = EndpointURL.split(TagName+"=");
		String[] NoTag1= NoTag[1].split("&",2);
		DynamicURL=NoTag[0]+TagName+"="+ProgrammeID+"&"+NoTag1[1];
		System.out.println("Dynamic URL:"+DynamicURL);
		Response response=GetReq.dynamicgetRequest(DynamicURL);
		int ActualStatus= response.getStatusCode();
		String ActualCode=Integer.toString(ActualStatus);
		valid.statusCodeValidation(ActualCode,"200");
		JSONObject channels =jsonOperations.getChannels(response.asString(),"programmedetails");
		JSONObject expectedChannel = (JSONObject) channels;
		for (String expectedkey : expectedChannel.keySet()) {
			if(expectedkey.contains("programme")){
				expectedChannels = expectedChannel.getJSONArray(expectedkey);
				programminfo=expectedChannels.getJSONObject(0);
				System.out.println(programminfo.get("programmename"));
				JSONArray value=programminfo.getJSONArray("programmename");
				JSONObject valuetemp=value.getJSONObject(0);
				System.out.println(valuetemp.get("value"));
				if(valuetemp.get("value").equals(actualChannels.getJSONObject(j).getString("ProgrammeName"))) {
					ExtentTestManager.getTest().log(Status.PASS, "PROGRAM NAME EXPECTED :"+actualChannels.getJSONObject(j).getString("ProgrammeName")+
							" ACTUAL :"+valuetemp.get("value"));
				}
				else {
					ExtentTestManager.getTest().log(Status.FAIL, "PROGRAM NAME EXPECTED :"+actualChannels.getJSONObject(j).getString("ProgrammeName")+
							" ACTUAL :"+valuetemp.get("value"));
				}
				System.out.println(programminfo.get("programmegenre"));
				value=programminfo.getJSONArray("programmegenre");
				valuetemp=value.getJSONObject(0);
				System.out.println(valuetemp.get("value"));
				if(valuetemp.get("value").equals(actualChannels.getJSONObject(j).getString("M2eCategoryName"))) {
					ExtentTestManager.getTest().log(Status.PASS, "GENRE EXPECTED :"+actualChannels.getJSONObject(j).getString("M2eCategoryName")+
							" ACTUAL :"+valuetemp.get("value"));
				}
				else {
					ExtentTestManager.getTest().log(Status.FAIL, "GENRE NAME EXPECTED :"+actualChannels.getJSONObject(j).getString("M2eCategoryName")+
							" ACTUAL :"+valuetemp.get("value"));
				}
				System.out.println(programminfo.get("programmesubgenre"));
				value=programminfo.getJSONArray("programmesubgenre");
				valuetemp=value.getJSONObject(0);
				System.out.println(valuetemp.get("value"));
				if(valuetemp.get("value").equals(actualChannels.getJSONObject(j).getString("M2eSubCategoryName"))) {
					ExtentTestManager.getTest().log(Status.PASS, "SUB-GENRE EXPECTED :"+actualChannels.getJSONObject(j).getString("M2eSubCategoryName")+
							" ACTUAL :"+valuetemp.get("value"));
				}
				else {
					ExtentTestManager.getTest().log(Status.FAIL, "SUB-GENRE NAME EXPECTED :"+actualChannels.getJSONObject(j).getString("M2eSubCategoryName")+
							" ACTUAL :"+valuetemp.get("value"));
				}
				System.out.println(programminfo.get("programmelanguage"));
				if(programminfo.get("programmelanguage").equals(actualChannels.getJSONObject(j).getString("LanguageName"))) {
					ExtentTestManager.getTest().log(Status.PASS, "LANGUAGE EXPECTED :"+actualChannels.getJSONObject(j).getString("LanguageName")+
							" ACTUAL :"+programminfo.get("programmelanguage"));
				}
				else {
					ExtentTestManager.getTest().log(Status.FAIL, "LANGUAGE EXPECTED :"+actualChannels.getJSONObject(j).getString("LanguageName")+
							" ACTUAL :"+programminfo.get("programmelanguage"));
				}
			}}
		
		
	
	}
}
	public void Optional_EN_DataValidation() throws IOException {
		
		ApiKey APIKeyTAG =new ApiKey();
		GetRequestMethod GetReq =new GetRequestMethod();
		Validations valid =new Validations();
		VODQueries Queries =new VODQueries();
		DatabaseConnection Connector= new DatabaseConnection(); 
		ResponseValidation jsonOperations = new ResponseValidation(); 
		String EndpointURL=APIKeyTAG.getEndpointValidation1("ProgrammeDetails");
		String DynamicURL;
		String  ProgrammeID = null;
		JSONArray value;
		JSONObject valuetemp;
		  JSONArray expectedChannels;
		  JSONObject programminfo;
		String TagName=ExcelUtility.map.get("Tag");
		//Database
		String Query=Queries.OPTIONAL_EN_DATA_VALIDATION();
		System.out.println(Query);
		Connector.getResult(Query);
		JSONObject db = Connector.getOptionalProgDetailsFields();
		JSONObject actualChannel = (JSONObject) db;
		JSONArray actualChannels = actualChannel.getJSONArray("programmedetails");
		for (int j = 0; j < actualChannels.length();j++) {
		ProgrammeID=actualChannels.getJSONObject(j).getString("CMSProgrammeID");
			ExtentTestManager.getTest().log(Status.INFO, "VALIDATING FOR PROGRAMMEID: " +ProgrammeID+" OF RECORD " +j);
			String[] NoTag = EndpointURL.split(TagName+"=");
			String[] NoTag1= NoTag[1].split("&",2);
			DynamicURL=NoTag[0]+TagName+"="+ProgrammeID+"&"+NoTag1[1];
			System.out.println("Dynamic URL:"+DynamicURL);
			Response response=GetReq.dynamicgetRequest(DynamicURL);
			int ActualStatus= response.getStatusCode();
			String ActualCode=Integer.toString(ActualStatus);
			valid.statusCodeValidation(ActualCode,"200");
			JSONObject channels =jsonOperations.getChannels(response.asString(),"programmedetails");
			JSONObject expectedChannel = (JSONObject) channels;
			for (String expectedkey : expectedChannel.keySet()) {
				if(expectedkey.contains("programme")){
					expectedChannels = expectedChannel.getJSONArray(expectedkey);
					programminfo=expectedChannels.getJSONObject(0);
					System.out.println(programminfo.get("synopsis"));
					if(actualChannels.getJSONObject(j).get("WapDescription")=="null") {
						if(programminfo.isNull("synopsis")){
							ExtentTestManager.getTest().log(Status.PASS, "SYNOPSIS EXPECTED AND ACTUAL VALUES ARE NULL");
						}
						else {
							ExtentTestManager.getTest().log(Status.FAIL, "SYNOPSIS EXPECTED AND ACTUAL VALUES ARE NOT NULL");
						}
					}
					else {
					 value=programminfo.getJSONArray("synopsis");
					valuetemp=value.getJSONObject(0);
					System.out.println(valuetemp.get("value"));
					if(valuetemp.get("value").equals(actualChannels.getJSONObject(j).getString("WapDescription"))) {
						ExtentTestManager.getTest().log(Status.PASS, "SYNOPSIS EXPECTED :"+actualChannels.getJSONObject(j).getString("WapDescription")+
								" ACTUAL :"+valuetemp.get("value"));
					}
					else {
						ExtentTestManager.getTest().log(Status.FAIL, "SYNOPSIS EXPECTED :"+actualChannels.getJSONObject(j).getString("WapDescription")+
								" ACTUAL :"+valuetemp.get("value"));
					}
					}
					System.out.println(programminfo.get("synopsis90"));
					if(actualChannels.getJSONObject(j).get("ShortDescription")=="null") {
						if(programminfo.isNull("synopsis90")){
							ExtentTestManager.getTest().log(Status.PASS, "SYNOPSIS90 EXPECTED AND ACTUAL VALUES ARE NULL");
						}
						else {
							ExtentTestManager.getTest().log(Status.FAIL, "SYNOPSIS90 EXPECTED AND ACTUAL VALUES ARE NOT NULL");
						}
					}
					else {
					value=programminfo.getJSONArray("synopsis90");
					valuetemp=value.getJSONObject(0);
					System.out.println(valuetemp.get("value"));
					if(valuetemp.get("value").equals(actualChannels.getJSONObject(j).getString("ShortDescription"))
							||(valuetemp.isNull("value"))&&actualChannels.getJSONObject(j).get("ShortDescription")=="null") {
						ExtentTestManager.getTest().log(Status.PASS, "SYNOPSIS90 EXPECTED :"+actualChannels.getJSONObject(j).getString("ShortDescription")+
								" ACTUAL :"+valuetemp.get("value"));
					}
					else {
						ExtentTestManager.getTest().log(Status.FAIL, "SYNOPSIS90 EXPECTED :"+actualChannels.getJSONObject(j).getString("ShortDescription")+
								" ACTUAL :"+valuetemp.get("value"));
					}
					}
					System.out.println(programminfo.get("parentalrating"));
					System.out.println(actualChannels.getJSONObject(j).get("Certificate"));
					if(programminfo.get("parentalrating").equals(actualChannels.getJSONObject(j).get("Certificate"))
							||(programminfo.isNull("parentalrating"))&&actualChannels.getJSONObject(j).get("Certificate")=="null") {
						ExtentTestManager.getTest().log(Status.PASS, "PARENTAL-RATING EXPECTED :"+actualChannels.getJSONObject(j).getString("Certificate")+
								" ACTUAL :"+programminfo.get("parentalrating"));
					}
					else {
						ExtentTestManager.getTest().log(Status.FAIL, "PARENTAL-RATING EXPECTED :"+actualChannels.getJSONObject(j).getString("Certificate")+
								" ACTUAL :"+programminfo.get("parentalrating"));
					}
					System.out.println(programminfo.get("productionyear"));
					if(programminfo.get("productionyear").equals(actualChannels.getJSONObject(j).getInt("ProductionYear"))
							||(programminfo.isNull("productionyear")&&(actualChannels.getJSONObject(j).getInt("ProductionYear")==0))) {
						ExtentTestManager.getTest().log(Status.PASS, "PRODUCTION YEAR EXPECTED :"+actualChannels.getJSONObject(j).getInt("ProductionYear")+
								" ACTUAL :"+programminfo.get("productionyear"));
					}
					else {
						ExtentTestManager.getTest().log(Status.FAIL, "PRODUCTION YEAR EXPECTED :"+actualChannels.getJSONObject(j).getInt("ProductionYear")+
								" ACTUAL :"+programminfo.get("productionyear"));
					}
				}}
			
			
		
		}
	}
		public void noResponseLanguageValue() throws IOException {
	
	
			
			ApiKey APIKeyTAG =new ApiKey();
			GetRequestMethod GetReq =new GetRequestMethod();
			Validations valid =new Validations();
			EPGQueries Queries =new EPGQueries();
			DatabaseConnection Connector= new DatabaseConnection();
			
			String DynamicURL;
			String EndpointURL=APIKeyTAG.getEndpointValidation1("ProgrammeDetails");
			System.out.println(EndpointURL);
			String TagName=ExcelUtility.map.get("Tag");
			String TagName1=ExcelUtility.map.get("Tag1");
			String Value1=ExcelUtility.map.get("TAG1Value");
			
			//Database
			
			String Query=Queries.ProgrammeDetails_Language();
			Query=Query.replace("%s", Value1);
			System.out.println(Query);
			Connector.getResult(Query);
			JSONObject db = Connector.getLanguageAssigned();
			JSONObject actualChannel = (JSONObject) db;
			JSONArray actualChannels = actualChannel.getJSONArray("programmedetails");
			String  LanguageAssigned=actualChannels.getJSONObject(0).getString("LanguageAssigned");
			ExtentTestManager.getTest().log(Status.INFO, "API: " +Value1+" HAVING ENTITLED LANGUAGES: " +LanguageAssigned);
				String[] NoTag = EndpointURL.split(TagName + "=");
				String[] NoTag1 = NoTag[1].split("&", 2);
				DynamicURL = NoTag[0] + TagName + "=&" + NoTag1[1];

				NoTag = DynamicURL.split(TagName1+"=");
				NoTag1 = NoTag[1].split("&", 2);
				DynamicURL = NoTag[0] + TagName1+"="+Value1+"&" + NoTag1[1];
				ExtentTestManager.getTest().log(Status.INFO, "DYNAMIC URL IS :" + DynamicURL);
				System.out.println(DynamicURL);
				Response response=GetReq.dynamicgetRequest(DynamicURL); 
				
				int ActualStatus= response.getStatusCode();
				String ActualCode=Integer.toString(ActualStatus);
				valid.statusCodeValidation(ActualCode,ExcelUtility.map.get("StatusCode"));
				valid.schemaValidation(response.asString(), JsonSchemaValidator.matchesJsonSchemaInClasspath("Schema/ProgrammeDetails_RL.json"));		
			}
			
		 public void HeadEndID_DataValidation() throws IOException {
			  ApiKey APIKeyTAG =new ApiKey();
			  GetRequestMethod GetReq =new GetRequestMethod();
			  KeyParameters KParameters =new KeyParameters();
			  Validations valid =new Validations();
			  VODQueries Queries =new VODQueries();
			  ResponseValidation jsonOperations = new ResponseValidation(); 
			  String DynamicURL;
			  JSONArray expectedChannels;
			  JSONObject programminfo;
			 
			  String HeadEndIDS=ExcelUtility.map.get("headendids");
			  APIKeyTAG.getEndpointValidation("HeadEndID_OVDStreaming");
			  String EndpointURL=KParameters.InValidValuePresent(ExcelUtility.map.get("Tag")); 
			  for (String headend : HeadEndIDS.split(",")) {
				String[] NoTag = EndpointURL.split(  "headendids=");
				String[] NoTag1 = NoTag[1].split("&", 2);
				DynamicURL = NoTag[0] + "headendids=" +headend  + "&" + NoTag1[1];
				Response response=GetReq.dynamicgetRequest(DynamicURL); 
				
				JSONObject channels =jsonOperations.getChannels(response.asString(),"programmeslist"); 
				JSONObject expectedChannel = (JSONObject) channels;
				Integer totalcount= (Integer) expectedChannel.get("changeoffset");
				String updateID=totalcount.toString();
				NoTag = DynamicURL.split(  "changeoffset=");
				NoTag1 = NoTag[1].split("&", 2);
				DynamicURL = NoTag[0] +"changeoffset="+totalcount+"&"+NoTag1[1];
				System.out.println(DynamicURL);
				response=GetReq.dynamicgetRequest(DynamicURL); 
				int ActualStatus= response.getStatusCode();
				String ActualCode=Integer.toString(ActualStatus);
				valid.statusCodeValidation(ActualCode,ExcelUtility.map.get("StatusCode"));
				channels =jsonOperations.getChannels(response.asString(),"programmeslist"); 
				expectedChannel = (JSONObject) channels;
				DatabaseConnection Connector= new DatabaseConnection();
				String Query=Queries.HEADENDID_DATA();
				Query=Query.replace("%s",headend);
				Query=Query.replace("%f",updateID);
				Query=Query.replace("%d",ExcelUtility.map.get("UserID"));
				ExtentTestManager.getTest().log(Status.INFO, "VALIDATING HEADENDID: "+headend+" WITH UPDATE ID: "+updateID);
				System.out.println(Query);
				Connector.getResult(Query);
				JSONObject db = Connector.HeadEndID_Data();
				JSONObject actualChannel = (JSONObject) db;
				JSONArray actualChannels = actualChannel.getJSONArray("programmeslist");
				for (String expectedkey : expectedChannel.keySet()) {
					if(expectedkey.contains("results")){
						expectedChannels = expectedChannel.getJSONArray(expectedkey);
						 for (int j = 0; j < actualChannels.length();j++) {
						programminfo=expectedChannels.getJSONObject(j);
						System.out.println(programminfo.get("contenttype"));
						if (programminfo.get("actionflag").equals(actualChannels.getJSONObject(j).getString("UpdateAction"))) 
						{
							if (programminfo.get("contenttype").equals(actualChannels.getJSONObject(j).getString("ContentType"))) 
							{
								System.out.println(programminfo.get("actionflag"));
								if(!programminfo.get("actionflag").equals("DNU")) {
								if (programminfo.get("contenttype").equals("episode")) 
								{
									int count =0;
									if(!programminfo.isNull("platformtitle")||!programminfo.isNull("platformepisodeid")) {
									if (!programminfo.isNull("platformtitle")) {
											if(programminfo.get("platformtitle").equals(actualChannels.getJSONObject(j).getString("EpisodeTitle"))) {
										ExtentTestManager.getTest().log(Status.PASS, "CONTENT:EPISODE, TITLE IS NOT BLANK");
									count++;
											}}
									else{
										if(programminfo.get("platformepisodeid").equals(actualChannels.getJSONObject(j).getString("SourceEpisodeid"))) {
											ExtentTestManager.getTest().log(Status.PASS, "CONTENT:EPISODE, ID IS NOT BLANK");
											count++;
										}	
									}}
									if(count==0) {
										ExtentTestManager.getTest().log(Status.FAIL, "CONTENT:EPISODE, TITLE & ID ARE BLANK");
										
									}
									//{
										if (programminfo.get("platformprogrammeid").equals(actualChannels.getJSONObject(j).getString("ProgrammeId"))) {
											if(programminfo.isNull("platformseasonid") || programminfo.isNull("platformtitle")) {
												ExtentTestManager.getTest().log(Status.WARNING, "CONTENT:EPISODE, SEASON ID AND TITLE ARE BLANK.");
												
											}
											else {
											if (programminfo.get("platformseasonid").equals(actualChannels.getJSONObject(j).getString("SourceSeasonid"))) {
												if (programminfo.get("platformepisodeid").equals(actualChannels.getJSONObject(j).getString("SourceEpisodeid"))) {
										ExtentTestManager.getTest().log(Status.PASS, "CONTENT: EPISODE, TITLE MATCHED ALONG WITH ALL IDS.");
									}
												else {
													ExtentTestManager.getTest().log(Status.FAIL, "CONTENT: EPISODE, TITLE NOT MATCHED ALONG WITH ALL IDS.");
													
												}
												}
										
									}
											}
										//}
								
								}
								else if(programminfo.get("contenttype").equals("tvseries")){
									//if (programminfo.get("platformtitle").equals(actualChannels.getJSONObject(j).getString("SeasonName"))) {
										if (programminfo.get("platformprogrammeid").equals(actualChannels.getJSONObject(j).getString("ProgrammeId"))) {
											if (programminfo.get("platformseasonid").equals(actualChannels.getJSONObject(j).getString("SourceSeasonid"))) {
												ExtentTestManager.getTest().log(Status.PASS, "CONTENT: TVSERIES, TITLE MATCHED ALONG WITH ALL IDS.");
												}
											else {
												ExtentTestManager.getTest().log(Status.FAIL, "CONTENT: TVSERIES, TITLE NOT MATCHED ALONG WITH ALL IDS.");
												
											}
										}
										//}
									}
								else {
									if (programminfo.get("platformtitle").equals(actualChannels.getJSONObject(j).getString("ProgrammeName"))) {
										if (programminfo.get("platformprogrammeid").equals(actualChannels.getJSONObject(j).getString("ProgrammeId"))) {
												
										ExtentTestManager.getTest().log(Status.PASS, "CONTENT: MOVIE, TITLE MATCHED ALONG WITH ALL ID.");
												}
										else {
											ExtentTestManager.getTest().log(Status.FAIL, "CONTENT: MOVIE, TITLE NOT MATCHED ALONG WITH ALL ID.");
											
										}
										}
								}
								if (programminfo.get("platformgenre").equals(actualChannels.getJSONObject(j).getString("ProgrammeGenre"))) 
								{
									ExtentTestManager.getTest().log(Status.PASS, "GENRE IS MATCHED");
									
								}
								}
							}
						
						
						}
						if(programminfo.get("contenttype").equals("episode")||programminfo.get("contenttype").equals("tvseries")) {
							System.out.println(programminfo.isNull("platformprogrammelanguage"));
							if(programminfo.isNull("platformprogrammelanguage")
							   ||programminfo.isNull("platformduration")
							   ||programminfo.isNull("normalizedparentalrating")) {
								ExtentTestManager.getTest().log(Status.PASS, "Since Content type is "+programminfo.get("contenttype")+" ,hence Lang,dur and rating are null");
							}
							else {
								ExtentTestManager.getTest().log(Status.FAIL, "Since Content type is "+programminfo.get("contenttype")+" ,hence Lang,dur and rating are not null");
								
							}
						}
						if(programminfo.get("contenttype").equals("program")||programminfo.get("contenttype").equals("tvseries")) {
							if(programminfo.isNull("platformactors")
							   ||programminfo.isNull("platformdirector")) {
								ExtentTestManager.getTest().log(Status.PASS, "Since Content type is "+programminfo.get("contenttype")+" ,hence actors and directors are null");
							}
							else {
								ExtentTestManager.getTest().log(Status.FAIL, "Since Content type is "+programminfo.get("contenttype")+" ,hence actors and directors are not null");
								
							}
						}
						}
						}
			  }
				}
		 }

}

 
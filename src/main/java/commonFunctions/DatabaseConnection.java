package commonFunctions;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.HashMap;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;

import com.aventstack.extentreports.Status;
import reports.ExtentTestManager;
public class DatabaseConnection {
	public static Logger log = LogManager.getLogger("file");
	public static HashMap<String, Integer> map = new HashMap<String, Integer>();
	public static HashMap<String, String> castDetails = new HashMap<String, String>();
	String DBName;
	InputStream inputStream;
	Properties prop = new Properties();
	String propFileName = "DBCredentials.properties";
	public static Connection connection;
	Statement statement;
	public static ResultSet resultSet;
	public Connection DBConnection(String API_TYPE) throws IOException {
	
		String databaseName	= null;
	readPropertiesFile(propFileName);
	inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
	if (inputStream != null) {
		try {
			prop.load(inputStream);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	} else {
		throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
	}
	String server = prop.getProperty("DBServer");
	String userId = prop.getProperty("DBUser");
	String password = prop.getProperty("DBPassword");
	
	if (API_TYPE.equalsIgnoreCase("Linear")) {
		databaseName= prop.getProperty("LINEAR_DBName");
		 //databaseName = credentials.getProperty("LINEAR_DBName");
	}
	else if(API_TYPE.equalsIgnoreCase("EPGCLOUD")) {
		databaseName= prop.getProperty("OVD_DBName");
		server = prop.getProperty("EPGCLOUD");
		password = prop.getProperty("EPGCLOUDPassword");
		userId = prop.getProperty("DBUser");
		
	}
	else if(API_TYPE.equalsIgnoreCase("OVD_DBName")) {
		databaseName= prop.getProperty("OVD_DBName");
		server = prop.getProperty("DBServerOVD");
		userId = prop.getProperty("DBUser");
		password = prop.getProperty("OVDPassword");
	}
	else{
		databaseName= prop.getProperty("VOD_DBNAME");
	}
	

	String connectionString = "jdbc:sqlserver://" + server +  ";databaseName=" + databaseName;
	
	// Obtaining connection with DB
	try {

		DriverManager.registerDriver(new com.microsoft.sqlserver.jdbc.SQLServerDriver());
		connection = DriverManager.getConnection(connectionString, userId, password);
		System.out.println("Database connection established");
		//ExtentTestManager.getTest().log(Status.PASS, "Database connection established");
		
	} catch (SQLException e) {
		log.error("Database connection could not be established. Please check connection data.");
		ExtentTestManager.getTest().log(Status.FAIL, "Database connection cannot be established");
		e.printStackTrace();
	}
	return connection;
}
	private void readPropertiesFile(String string) {
		// TODO Auto-generated method stub
		
	}
	/**
	 * 
	 * @description fetches data from DB
	 * @param query
	 * @return ResultSet
	 */
	public ResultSet getResult(String query) {
		
		if (connection != null) {
			// creating statement object
			try {
				statement = connection.createStatement();				
			} catch (SQLException e) {
				log.error("Exception Occurred while creating Statement Object", e);
			}
			try {
				log.info("Executing Query :: " + query);
				// query execution start time
				Date startTime = new Date();
				resultSet = statement.executeQuery(query); 
				
				// query execution end time
				Date endTime		= new Date();
				// caluclating time difference
				long	diffTime	= endTime.getTime() - startTime.getTime();
				long	minutes		= (diffTime / 1000) / 60;
				long	seconds		= (diffTime / 1000) % 60;
				log.info(minutes + " : " + seconds + " taken to execute query :: " + query);
				log.info("DB Query Execution Completed.");
			} catch (SQLException e) {
				log.error("Exception Occurred while fetching data from DB", e);
			}

		} else {
			log.error("DB Connection object received as null.");
		}

		return resultSet;
	}
	
	/**
	 * @description closing db connection
	 * @param connection
	 */
	public void closeDBConnection() {
			try {
				connection.close();
				log.info("Database Connection Closed!");
				ExtentTestManager.getTest().log(Status.PASS, "Database connection closed");
			} catch (Exception e) {
				log.error("Connection could not be closed! Message: " + e.getLocalizedMessage());
			}
		}

	/*
	  Function Name-getChannels 
	  Purpose- Returns Resultset in JSONArray for program Details in Celebrity API.
	 */
	public JSONObject getChannels() {
		JSONObject ResultRecord = new JSONObject();
		try { 
			ResultRecord.put("programlist", new JSONArray());
			int count=0;
			while (resultSet.next())
			{
				JSONObject channelN = new JSONObject();
				channelN.put("Programmename", resultSet.getString("Programmename"));
				channelN.put("programmeId", resultSet.getString("programmeId"));
				channelN.put("releasedate", resultSet.getString("releasedate"));    
				channelN.put("RoleName", resultSet.getString("RoleName"));
				channelN.put("Priority", resultSet.getString("Priority"));         
				ResultRecord.getJSONArray("programlist").put(channelN);
				count++;
			}
			map.put("RowCount",count);
			System.out.println(count);
		} catch (Exception e) {
			log.error("Exception Occured Message: " + e.getLocalizedMessage());
			Assert.fail("Exception Occured Message: " + e.getLocalizedMessage());
		}

		return ResultRecord;
	}
	
	public JSONObject getChannelsCatalog() {
		JSONObject ResultRecord = new JSONObject();
		try { 
			ResultRecord.put("programinfo", new JSONArray());
			int count=0;
			while (resultSet.next())
			{
				JSONObject channelN = new JSONObject();
				channelN.put("ProgrammeId", resultSet.getString("ProgrammeId"));
				channelN.put("CMSProgrammeID", resultSet.getString("CMSProgrammeID"));
				channelN.put("ProgrammeName", resultSet.getString("ProgrammeName"));
				channelN.put("ProgrammeLanguage", resultSet.getString("ProgrammeLanguage"));    
				channelN.put("ProgrammeGenre", resultSet.getString("ProgrammeGenre"));
				channelN.put("Synopsis", resultSet.getString("Synopsis"));  
				channelN.put("Cast", resultSet.getString("Cast"));    
				channelN.put("ProductionYear", resultSet.getInt("ProductionYear"));
				channelN.put("StartDate", resultSet.getString("StartDate"));
				channelN.put("EndDate", resultSet.getString("EndDate"));
				ResultRecord.getJSONArray("programinfo").put(channelN);
				count++;
			}
			map.put("RowCount",count);
			System.out.println(count);
		} catch (Exception e) {
			log.error("Exception Occured Message: " + e.getLocalizedMessage());
			Assert.fail("Exception Occured Message: " + e.getLocalizedMessage());
		}

		return ResultRecord;
	}
	public JSONObject HeadEndID_Data() {
		JSONObject ResultRecord = new JSONObject();
		try { 
			ResultRecord.put("programmeslist", new JSONArray());
			while (resultSet.next())
			{
				JSONObject channelN = new JSONObject();
				channelN.put("UpdateAction", resultSet.getString("UpdateAction"));
				channelN.put("ProgrammeName", resultSet.getString("ProgrammeName"));
				channelN.put("SeasonName", resultSet.getString("SeasonName"));
				channelN.put("EpisodeTitle", resultSet.getString("EpisodeTitle"));    
				channelN.put("ContentType", resultSet.getString("ContentType"));
				channelN.put("ProgrammeGenre", resultSet.getString("ProgrammeGenre"));  
				channelN.put("ProductionYear", resultSet.getString("ProductionYear"));    
				channelN.put("ProgrammeLanguage", resultSet.getString("ProgrammeLanguage"));
				channelN.put("ProgrammeId", resultSet.getString("ProgrammeId"));
				channelN.put("SourceEpisodeid", resultSet.getString("SourceEpisodeid"));
				channelN.put("SourceSeasonid", resultSet.getString("SourceSeasonid"));
				ResultRecord.getJSONArray("programmeslist").put(channelN);
				
			}
		} catch (Exception e) {
			log.error("Exception Occured Message: " + e.getLocalizedMessage());
			Assert.fail("Exception Occured Message: " + e.getLocalizedMessage());
		}

		return ResultRecord;
	}
	
	public JSONObject getMetadata() {
		JSONObject ResultRecord = new JSONObject();
		try { 
			ResultRecord.put("programmeslist", new JSONArray());
			int count=0;
			while (resultSet.next())
			{
				JSONObject channelN = new JSONObject();
				channelN.put("MetaDataName", resultSet.getString("MetaDataName"));
				channelN.put("AssignedBucket", resultSet.getString("AssignedBucket"));
				channelN.put("ProductName", resultSet.getString("ProductName"));
				channelN.put("PackageName", resultSet.getString("PackageName")); 
				channelN.put("EnabledHierarchy", resultSet.getString("EnabledHierarchy")); 
				channelN.put("ContentType", resultSet.getString("ContentType")); 
				
				ResultRecord.getJSONArray("programmeslist").put(channelN);
				count++;
			}
			map.put("RowCount",count);
			System.out.println(count);
		} catch (Exception e) {
			log.error("Exception Occured Message: " + e.getLocalizedMessage());
			Assert.fail("Exception Occured Message: " + e.getLocalizedMessage());
		}

		return ResultRecord;
	}
	
	public JSONObject OVDStreamingValue() {
		JSONObject ResultRecord = new JSONObject();
		try { 
			ResultRecord.put("programmeslist", new JSONArray());
			while (resultSet.next())
			{
				JSONObject channelN = new JSONObject();
				channelN.put("ProgrammeId", resultSet.getString("ProgrammeId"));
				channelN.put("ProgrammeName", resultSet.getString("ProgrammeName"));
				channelN.put("ContentType", resultSet.getString("ContentType"));
				channelN.put("ProgramImage", resultSet.getString("ProgramImage")); 
				channelN.put("Synopsis", resultSet.getString("Synopsis")); 
				channelN.put("ProductionYear", resultSet.getString("ProductionYear")); 
				channelN.put("ProgrammeLanguage", resultSet.getString("ProgrammeLanguage")); 
				channelN.put("OperatorId", resultSet.getString("OperatorId")); 
				channelN.put("UpdateAction", resultSet.getString("UpdateAction")); 
				
				ResultRecord.getJSONArray("programmeslist").put(channelN);
				
			}
			
		} catch (Exception e) {
			log.error("Exception Occured Message: " + e.getLocalizedMessage());
			Assert.fail("Exception Occured Message: " + e.getLocalizedMessage());
		}

		return ResultRecord;
	}
	
	public JSONObject IMPORTVALUE() {
		JSONObject ResultRecord = new JSONObject();
		try { 
			ResultRecord.put("IMPORT", new JSONArray());
			while (resultSet.next())
			{
				JSONObject channelN = new JSONObject();
				channelN.put("VODSRID", resultSet.getString("VODSRID"));
				ResultRecord.getJSONArray("IMPORT").put(channelN);
				
			}
			
		} catch (Exception e) {
			log.error("Exception Occured Message: " + e.getLocalizedMessage());
			Assert.fail("Exception Occured Message: " + e.getLocalizedMessage());
		}

		return ResultRecord;
	}
	
	public JSONObject VODPROGRAMME_IMPORT_db() {
		JSONObject ResultRecord = new JSONObject();
		try { 
			ResultRecord.put("VODPROGRAMME_IMPORT_db", new JSONArray());
			while (resultSet.next())
			{
				JSONObject channelN = new JSONObject();
				
				
				channelN.put("ProgrammeName", resultSet.getString("ProgrammeName"));
				channelN.put("ProgrammeId", resultSet.getString("ProgrammeId"));
				channelN.put("ProgrammeLanguage", resultSet.getString("ProgrammeLanguage"));
				channelN.put("ProgrammeGenre", resultSet.getString("ProgrammeGenre"));
				channelN.put("Synopsis", resultSet.getString("Synopsis"));
				channelN.put("ProgramImage", resultSet.getString("ProgramImage"));
				channelN.put("ProgrammeUrl", resultSet.getString("ProgrammeUrl"));
				if(resultSet.getString("Director")==null) {
					channelN.put("Director",JSONObject.NULL );
					}
					else {
						channelN.put("Director", resultSet.getString("Director"));
					}
				//channelN.put("Director", resultSet.getString("Director"));
				//channelN.put("Cast", resultSet.getString("Cast"));
				if(resultSet.getString("Cast")==null) {
					channelN.put("Cast",JSONObject.NULL );
					}
					else {
						channelN.put("Cast", resultSet.getString("Cast"));
					}
				
				channelN.put("ContentType", resultSet.getString("ContentType"));
				channelN.put("ProgramReleaseDate", resultSet.getString("ProgramReleaseDate"));
				if(resultSet.getString("ChannelName")==null) {
				channelN.put("ChannelName",JSONObject.NULL );
				}
				else {
					channelN.put("ChannelName", resultSet.getString("ChannelName"));
				}
				//channelN.put("SourceSeasonid", resultSet.getString("SourceSeasonid").isEmpty());
				channelN.put("SeasonName", resultSet.getString("SeasonName"));
				channelN.put("SeasonImage", resultSet.getString("SeasonImage"));
				channelN.put("SourceEpisodeid", resultSet.getString("SourceEpisodeid"));
				channelN.put("EpisodeNumber", resultSet.getString("EpisodeNumber"));
				channelN.put("EpisodeTitle", resultSet.getString("EpisodeTitle"));
				channelN.put("EpisodeDescription", resultSet.getString("EpisodeDescription"));
				//channelN.put("EpisodeReleasedate", resultSet.getString("EpisodeReleasedate"));
				channelN.put("EpisodeImage", resultSet.getString("EpisodeImage"));
				//channelN.put("EpisodeVideoUrl", resultSet.getString("EpisodeVideoUrl"));
				channelN.put("CommercialModel", resultSet.getString("CommercialModel"));
				channelN.put("StartDate", resultSet.getString("StartDate"));
				channelN.put("EndDate", resultSet.getString("EndDate"));
				if(resultSet.getString("SourceSeasonid")==null) {
					channelN.put("SourceSeasonid",JSONObject.NULL );
					}
					else {
						channelN.put("SourceSeasonid", resultSet.getString("SourceSeasonid"));
					}
				
				if(resultSet.getString("EpisodeVideoUrl")==null) {
					channelN.put("EpisodeVideoUrl",JSONObject.NULL );
					}
					else {
						channelN.put("EpisodeVideoUrl", resultSet.getString("EpisodeVideoUrl"));
					}
				if(resultSet.getString("EpisodeReleasedate")==null) {
					channelN.put("EpisodeReleasedate",JSONObject.NULL );
					}
					else {
						channelN.put("EpisodeReleasedate", resultSet.getString("EpisodeReleasedate"));
					}
				
				if(resultSet.getString("SocialType")==null) {
					channelN.put("SocialType",JSONObject.NULL );
					}
					else {
						channelN.put("SocialType", resultSet.getString("SocialType"));
					}
				//channelN.put("SocialType", resultSet.getString("SocialType"));
				//channelN.put("SocialCount", resultSet.getString("SocialCount"));
				if(resultSet.getString("SocialCount")==null) {
					channelN.put("SocialCount",JSONObject.NULL );
					}
					else {
						channelN.put("SocialCount", resultSet.getString("SocialCount"));
					}
				if(resultSet.getString("ParentalRating")==null) {
					channelN.put("ParentalRating",JSONObject.NULL );
					}
					else {
						channelN.put("ParentalRating", resultSet.getString("ParentalRating"));
					}
				//channelN.put("ParentalRating", resultSet.getString("ParentalRating"));
				//channelN.put("SeasonNumber", resultSet.getString("SeasonNumber"));
				if(resultSet.getString("SeasonNumber")==null) {
					channelN.put("SeasonNumber",JSONObject.NULL );
					}
					else {
						channelN.put("SeasonNumber", resultSet.getString("SeasonNumber"));
					}
				if(resultSet.getString("Duration")==null) {
					channelN.put("Duration",JSONObject.NULL );
					}
					else {
						channelN.put("Duration", resultSet.getString("Duration"));
					}
				//channelN.put("Duration", resultSet.getString("Duration"));
			//	channelN.put("ProductionYear", resultSet.getString("ProductionYear"));
				if(resultSet.getString("ProductionYear")==null) {
					channelN.put("ProductionYear",JSONObject.NULL );
					}
					else {
						channelN.put("ProductionYear", resultSet.getString("ProductionYear"));
					}
				
				//channelN.put("Commercials", resultSet.getString("Commercials"));
				if(resultSet.getString("Commercials")==null) {
					channelN.put("Commercials",JSONObject.NULL );
					}
					else {
						channelN.put("Commercials", resultSet.getString("Commercials"));
					}
				
				//channelN.put("EpisodeAndroidDeeplink", resultSet.getString("EpisodeAndroidDeeplink"));
				if(resultSet.getString("EpisodeAndroidDeeplink")==null) {
					channelN.put("EpisodeAndroidDeeplink",JSONObject.NULL );
					}
					else {
						channelN.put("EpisodeAndroidDeeplink", resultSet.getString("EpisodeAndroidDeeplink"));
					}
				//channelN.put("SeasonAndroidDeeplink", resultSet.getString("SeasonAndroidDeeplink"));
				if(resultSet.getString("SeasonAndroidDeeplink")==null) {
					channelN.put("SeasonAndroidDeeplink",JSONObject.NULL );
					}
					else {
						channelN.put("SeasonAndroidDeeplink", resultSet.getString("SeasonAndroidDeeplink"));
					}
				if(resultSet.getString("UDISNoofEpisodes")==null) {
					channelN.put("ProgramAndroidDeeplink",JSONObject.NULL );
					}
					else {
						channelN.put("ProgramAndroidDeeplink", resultSet.getString("ProgramAndroidDeeplink"));
					}
				//channelN.put("ProgramAndroidDeeplink", resultSet.getString("ProgramAndroidDeeplink"));
				//channelN.put("UDISNoofEpisodes", resultSet.getString("UDISNoofEpisodes"));	
				if(resultSet.getString("UDISNoofEpisodes")==null) {
					channelN.put("UDISNoofEpisodes",JSONObject.NULL );
					}
					else {
						channelN.put("UDISNoofEpisodes", resultSet.getString("UDISNoofEpisodes"));
					}
				ResultRecord.getJSONArray("VODPROGRAMME_IMPORT_db").put(channelN);
				
			}
			
		} catch (Exception e) {
			log.error("Exception Occured Message: " + e.getLocalizedMessage());
			Assert.fail("Exception Occured Message: " + e.getLocalizedMessage());
		}

		return ResultRecord;
	}
	public JSONObject IMPORTPROCESS_VALIDATION_db() {
		JSONObject ResultRecord = new JSONObject();
		try { 
			ResultRecord.put("IMPORTPROCESS_VALIDATION_DB", new JSONArray());
			while (resultSet.next())
			{
				JSONObject channelN = new JSONObject();
				channelN.put("AttributeValue", resultSet.getString("AttributeValue"));
				channelN.put("AttributeID", resultSet.getString("AttributeID"));
				channelN.put("AttributeName", resultSet.getString("AttributeName"));
				ResultRecord.getJSONArray("IMPORTPROCESS_VALIDATION_DB").put(channelN);
				
			}
			
		} catch (Exception e) {
			log.error("Exception Occured Message: " + e.getLocalizedMessage());
			Assert.fail("Exception Occured Message: " + e.getLocalizedMessage());
		}

		return ResultRecord;
	}
	
	public void getUSERID() {
		try { 
			while (resultSet.next())
			{
				String UserID = resultSet.getString("UserID");
				castDetails.put("UserID", UserID);
				}
		} catch (Exception e) {
			log.error("Exception Occured Message: " + e.getLocalizedMessage());
			Assert.fail("Exception Occured Message: " + e.getLocalizedMessage());
		}}
	
	public JSONObject getTotalRecords() {
		JSONObject ResultRecord = new JSONObject();
		try { 
			ResultRecord.put("programinfo", new JSONArray());
			int count=0;
			int MovieSum=0;
			int TVShowSum=0;
			while (resultSet.next())
			{
				JSONObject channelN = new JSONObject();
				String Temp=resultSet.getString("ContentType");
				System.out.println(Temp);
				if(resultSet.getString("ContentType").equalsIgnoreCase("Movie")){
					channelN.put("Count", resultSet.getInt("Count"));
					MovieSum=MovieSum+resultSet.getInt("Count");
					System.out.println(MovieSum);
				}
				else {
				channelN.put("Count", resultSet.getInt("Count"));
				TVShowSum=TVShowSum+resultSet.getInt("Count");
				System.out.println(TVShowSum);
				count++;
				}
			}
			map.put("MovieCount",MovieSum);
			map.put("TVShowCount",TVShowSum);
			map.put("RowCount",count);
			System.out.println(count);
		} catch (Exception e) {
			log.error("Exception Occured Message: " + e.getLocalizedMessage());
			Assert.fail("Exception Occured Message: " + e.getLocalizedMessage());
		}

		return ResultRecord;
	}
	
	/*
	  Function Name-getCastDetails 
	  Purpose- Returns Resultset  for Cast Details in Celebrity API and store in hashmap.
	 */
			public void getCastDetails() {
				try { 
					
					int count=0;
					while (resultSet.next())
					{
						String CastName = resultSet.getString("castname");
						castDetails.put("castname", CastName);
						String Castid = resultSet.getString("castid");
						castDetails.put("castid", Castid);
						String height = resultSet.getString("height");
						castDetails.put("height", height);
						String dob = resultSet.getString("dob");
						castDetails.put("dob", dob);
						String castimage = resultSet.getString("castImage");
						if(castimage!=null) {
						String[] castimageurl = castimage.split("/");
						String sizearray = castimageurl[castimageurl.length - 1];	
						System.out.println(sizearray);
						System.out.println(castimageurl[1]);
						castDetails.put("castimageurl", sizearray);
						}
						else {
							castDetails.put("castimageurl", null);
						}
						count++;
					}
					map.put("RowCount",count);
					System.out.println(count);
				} catch (Exception e) {
					log.error("Exception Occured Message: " + e.getLocalizedMessage());
					Assert.fail("Exception Occured Message: " + e.getLocalizedMessage());
				}
				
		
			}
			/*
			  Function Name-getCastDetails 
			  Purpose- Returns Resultset  for Cast Details in Celebrity API and store in hashmap.
			 */
			public JSONObject getEpisodeDetails() {
				JSONObject ResultRecord = new JSONObject();
				try { 
					ResultRecord.put("programinfo", new JSONArray());
					int count=0;
					while (resultSet.next())
					{
						JSONObject channelN = new JSONObject();
						channelN.put("EpisodeTitle", resultSet.getString("EpisodeTitle"));
						channelN.put("EpisodeNumber", resultSet.getString("EpisodeNumber"));
						channelN.put("ContentType", resultSet.getString("ContentType"));
						channelN.put("EpisodeDescription", resultSet.getString("EpisodeDescription"));    
						channelN.put("EpisodeImage", resultSet.getString("EpisodeImage"));
						channelN.put("EpisodeReleasedate", resultSet.getString("EpisodeReleasedate"));  
						ResultRecord.getJSONArray("programinfo").put(channelN);
						count++;
					}
					map.put("RowCount",count);
					System.out.println(count);
				} catch (Exception e) {
					log.error("Exception Occured Message: " + e.getLocalizedMessage());
					Assert.fail("Exception Occured Message: " + e.getLocalizedMessage());
				}

				return ResultRecord;
			}
			
			public JSONObject getLanguageAssigned() {
				JSONObject ResultRecord = new JSONObject();
				try { 
					ResultRecord.put("programmedetails", new JSONArray());
					
					while (resultSet.next())
					{
						JSONObject channelN = new JSONObject();
						channelN.put("APIKey", resultSet.getString("APIKey"));
						channelN.put("LanguageAssigned", resultSet.getString("LanguageAssigned"));
						channelN.put("PackageName", resultSet.getString("PackageName"));
						channelN.put("UserID", resultSet.getString("UserID"));  
						ResultRecord.getJSONArray("programmedetails").put(channelN);
					}
				} catch (Exception e) {
					log.error("Exception Occured Message: " + e.getLocalizedMessage());
					Assert.fail("Exception Occured Message: " + e.getLocalizedMessage());
				}

				return ResultRecord;
			}
			
			
			public JSONObject getProgrammeID() {
				JSONObject ResultRecord = new JSONObject();
				try { 
					ResultRecord.put("programmedetails", new JSONArray());
					
					while (resultSet.next())
					{
						JSONObject channelN = new JSONObject();
						channelN.put("ProgrammeID", resultSet.getString("ProgrammeID"));
						ResultRecord.getJSONArray("programmedetails").put(channelN);
					}
				} catch (Exception e) {
					log.error("Exception Occured Message: " + e.getLocalizedMessage());
					Assert.fail("Exception Occured Message: " + e.getLocalizedMessage());
				}

				return ResultRecord;
			}
			public JSONObject getMandateProgDetailsFields() {
				JSONObject ResultRecord = new JSONObject();
				try { 
					ResultRecord.put("programmedetails", new JSONArray());
					
					while (resultSet.next())
					{
						JSONObject channelN = new JSONObject();
						channelN.put("CMSProgrammeID", resultSet.getString("CMSProgrammeID"));
						channelN.put("ProgrammeName", resultSet.getString("ProgrammeName"));
						channelN.put("M2eCategoryName", resultSet.getString("M2eCategoryName"));
						channelN.put("M2eSubCategoryName", resultSet.getString("M2eSubCategoryName"));
						channelN.put("LanguageName", resultSet.getString("LanguageName"));
						ResultRecord.getJSONArray("programmedetails").put(channelN);
					}
				} catch (Exception e) {
					log.error("Exception Occured Message: " + e.getLocalizedMessage());
					Assert.fail("Exception Occured Message: " + e.getLocalizedMessage());
				}

				return ResultRecord;
			}
			
			public JSONObject getOptionalProgDetailsFields() {
				JSONObject ResultRecord = new JSONObject();
				try { 
					ResultRecord.put("programmedetails", new JSONArray());
					
					while (resultSet.next())
					{
						JSONObject channelN = new JSONObject();
						channelN.put("CMSProgrammeID", resultSet.getString("CMSProgrammeID"));
						if(resultSet.getString("ShortDescription").isBlank()) {
							channelN.put("ShortDescription","null" );
							}
							else {
								channelN.put("ShortDescription", resultSet.getString("ShortDescription"));
							}
						if(resultSet.getString("WapDescription").isBlank()) {
							channelN.put("WapDescription","null" );}
							else {channelN.put("WapDescription", resultSet.getString("WapDescription"));}
						if(resultSet.getString("Certificate").isBlank()) {
							channelN.put("Certificate","null" );}
							else {
								channelN.put("Certificate", resultSet.getString("Certificate"));}
						//System.out.println(channelN.get("Certificate"));
						if(resultSet.getString("ProductionYear").isBlank()) {
							channelN.put("ProductionYear","null" );}
							else {channelN.put("ProductionYear", resultSet.getInt("ProductionYear"));}
						
						ResultRecord.getJSONArray("programmedetails").put(channelN);
					}
				} catch (Exception e) {
					log.error("Exception Occured Message: " + e.getLocalizedMessage());
					Assert.fail("Exception Occured Message: " + e.getLocalizedMessage());
				}

				return ResultRecord;
			}
			public int getProgrammeIDCount() {
				int ResultRecord=0;
				try { 
					
					while (resultSet.next())
					{
						ResultRecord++;
					}
				} catch (Exception e) {
					log.error("Exception Occured Message: " + e.getLocalizedMessage());
					Assert.fail("Exception Occured Message: " + e.getLocalizedMessage());
				}

				return ResultRecord;
			}
			
			
			
			
			/*
			  Function Name-getChannels 
			  Purpose- Returns Result for Cast Image Dimensions in Celebrity API and store in hashmap.
			 */
		public void getCastDimensions() {
			try {
				EPGQueries query =new EPGQueries();
				query.CastDetails();
				String Query=EPGQueries.map.get("CastDetails");
				Query=Query.replace("%s",ExcelUtility.map.get("CastID"));
				System.out.println(Query);
				getResult(Query);
				while (resultSet.next())
				{
					String castimage = resultSet.getString("castImage");
					if(castimage!=null) {
					String[] castimageurl = castimage.split("/");
					String[] sizearray = castimageurl[castimageurl.length - 2].split("x");	
					System.out.println(sizearray[0]);
					System.out.println(sizearray[1]);
					castDetails.put("DBWidth", sizearray[0]);
					castDetails.put("DBHeight", sizearray[1]);
					}
					else {
						castDetails.put("DBWidth", null);
					}
				}
			} catch (Exception e) {
				log.error("Exception Occured Message: " + e.getLocalizedMessage());
				Assert.fail("Exception Occured Message: " + e.getLocalizedMessage());
			}
			
		}

}

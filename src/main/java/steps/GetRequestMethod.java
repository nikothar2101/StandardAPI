package steps;

import static io.restassured.RestAssured.*;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import com.aventstack.extentreports.Status;

import io.restassured.http.ContentType;
import io.restassured.response.Response;
import reports.ExtentTestManager;

public class GetRequestMethod {
	String URL;
	//String EndpointAPI="AdvanceSearch";
	InputStream inputStream;
	 //Properties prop=new Properties();
	Properties prop = new Properties();
	String propFileName = "config.properties";
	
	/*Function Name-getEndpoint
	  Purpose- For getting endpoint from config.properties file and based on testcase it will pick the endpoint*/
	public void getEndpoint(String EndpointAPI) throws IOException {
		readPropertiesFile(propFileName);
		inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
		if (inputStream != null) {
			try {
				prop.load(inputStream);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
		}
		// Properties prop = 
		if(EndpointAPI.equals("AdvanceSearch")) {
			URL = prop.getProperty("AdvanceSearchAPI");
		}
		else if(EndpointAPI.equals("AdvanceSearchAPINoResponseLanguage")) {
			
			URL = prop.getProperty("AdvanceSearchAPINoResponseLanguage");
		}
		else if(EndpointAPI.equals("ScheduleGrid_UAT")) {
			
			URL = prop.getProperty("ScheduleGrid_UAT");
		}
		else if(EndpointAPI.equals("CelebrityAPI")) {
			
			URL = prop.getProperty("CelebrityAPI");
		}
		else if(EndpointAPI.equals("ScheduleGrid_LIVE")) {
	
	URL = prop.getProperty("ScheduleGrid_LIVE");
		}
		else if(EndpointAPI.isEmpty()) {
			ExtentTestManager.getTest().log(Status.FAIL, "Endpoint API IS BLANK/EMPTY");
		}
		else {
			ExtentTestManager.getTest().log(Status.FAIL, "Endpoint API IS NOT PRESENT IN PROPERTIES FILE");
		}
		ExtentTestManager.getTest().log(Status.PASS, "Endpoint is:"+URL);
	}
	private void readPropertiesFile(String string) {
		// TODO Auto-generated method stub
		
	}
	/*Function Name-getRequest
	  Purpose- For Get request using rest assured
	  */
	
	public Response getRequest() throws IOException {
		return given()
                .contentType(ContentType.JSON)
                .when()
                .get(URL)
                .then()
                .extract().response();
	}

	/*Function Name-dynamicgetRequest
	  Purpose- Getting dynamic request using rest assured Dynamic Endpoint/URL
	  */ 
	public Response dynamicgetRequest(String URL) throws IOException {
		return given()
                .contentType(ContentType.JSON)
                .when()
                .get(URL)
                .then()
                .extract().response();
	}
	/*Function Name-dynamicgetRequest
	  Purpose- Getting dynamic request using rest assured Dynamic Endpoint/URL
	  */ 
	public Response DynamicgetRequestError(String URL) throws IOException {
		return given()
              .contentType("application/octet-stream")
              .when()
              .get(URL)
              .then()
              .extract().response();
	}
}
